#include "..\BattleTank3d\framework\renderer\SunRenderer.h"
#include "..\\BattleTank3d\\framework\\base\\Material.h"
#include "..\\BattleTank3d\\framework\geometry\Sun.h"

USING_NS_TINY;

SunRenderer::SunRenderer()
{
}

SunRenderer::~SunRenderer()
{
	_texture->Release();
}

bool SunRenderer::init()
{
	Renderer::init();

	_texture = nullptr;
	auto mat = Material("sunMat", Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f));
	this->setMaterial(mat);

	return true;
}

void SunRenderer::draw(LPDIRECT3DVERTEXBUFFER9 vertex, LPDIRECT3DINDEXBUFFER9 index)
{
	Renderer::draw();

	// select which vertex we're using
	_device->SetFVF(Sun::SUN_FVF);

	// select vertex buffer to display
	_device->SetStreamSource(0, vertex, 0, sizeof(Sun::SunVertex));
	_device->SetIndices(index);
	//
	//alpha blend
	_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	_device->SetMaterial(&_material);
	_device->SetTexture(0, _texture);

	// set blending factors so that alpha component determines transparency
	_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	// draw cube
	_device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 6);

	_device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
}

void SunRenderer::setTexture(LPDIRECT3DTEXTURE9 texture)
{
	_texture = texture;
}

void SunRenderer::setMaterial(const Material& material)
{
	_material.Ambient = Color4F(material.ambient.x, material.ambient.y, material.ambient.z, material.alpha);
	_material.Diffuse = Color4F(material.diffuse.x, material.diffuse.y, material.diffuse.z, material.alpha);
	_material.Specular = Color4F(material.specular.x, material.specular.y, material.specular.z, material.alpha);
	_material.Emissive = Color4F(material.emissive.x, material.emissive.y, material.emissive.z, material.alpha);
	_material.Power = material.shininess;
}
