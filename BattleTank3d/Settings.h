#pragma once

#include "framework\base\Size.h"
#include "framework\TinyDefinitions.h"

NS_TINY_BEGIN

class Settings
{
public:
	enum AntiAliasingType
	{
		NONE = 0,
		AA2X = 2,
		AA4X = 4
	};

	static Settings* getInstance();

	~Settings();

	bool init();
	void release();

	bool isFullScreen();
	void setFullScreen(bool value);

	bool isEnabledLight();
	void enableLight(bool value);

	const Size& getWindowSize();
	void setWindowSize(const Size& size);

	float getFrameRate();
	void setFrameRate(float frameRate);

	Vec2 getMouseStartPosition();
	void setMouseStartPosition(const Vec2& position);

	const Vec2& getWindowStartPosition();
	void setWindowStartPosition(const Vec2& position);

	void setAntiAliasing(int value);
	void setAntiAliasing(AntiAliasingType value);
	int getAntiAliasing();

	const Color4F& getAmbientColor();
	void setAmbientColor(const Color4F& ambientRGB);

	void showCursor(bool enable);

	inline bool isShownCursor() { return _isShownCursor; }

private:
	static Settings* _instance;

	bool _isFullScreen;
	bool _lightEnabled;
	Size _windowSize;

	float _frameRate;

	Vec2 _mouseStartPosition;
	Vec2 _windowStartPosition;

	int _multiSampleType;

	Color4F _ambientColor;
	bool _isShownCursor;
};

NS_TINY_END