#pragma once

#include "framework\TinyDefinitions.h"

#include "framework\base\Shader.h"
#include "framework\base\Object.h"
#include "framework\base\Scene.h"
#include "framework\base\Size.h"
#include "framework\base\Time.h"
#include "framework\base\Component.h"
#include "framework\base\Renderer.h"
#include "framework\base\Event.h"
#include "framework\base\EventDispatcher.h"
#include "framework\base\EventKeyboard.h"
#include "framework\base\EventKeyboardListener.h"
#include "framework\base\EventMouse.h"
#include "framework\base\EventMouseListener.h"
#include "framework\base\EventContact.h"
#include "framework\base\EventContactListener.h"
#include "framework\base\EventListener.h"
#include "framework\base\OBJFileReader.h"
#include "framework\base\Material.h"
#include "framework\base\Model.h"

#include "framework\component\Transform.h"
#include "framework\component\Mesh.h"
#include "framework\component\BoundingBox.h"
#include "framework\component\SphereCollider.h"

#include "framework\geometry\Cube.h"
#include "framework\geometry\Plane.h"
#include "framework\geometry\Sun.h"
#include "framework\geometry\Terrain.h"
#include "framework\geometry\Sphere.h"
#include "framework\geometry\Ray.h"
#include "framework\geometry\PointLightCube.h"

#include "framework\effect\PhongEffect.h"

#include "framework\Utils.h"
#include "framework\Game.h"
#include "framework\WindowsHelper.h"
#include "framework\Direct3DDevice.h"
#include "framework\Camera.h"
#include "framework\SceneManager.h"
//testing
#include "framework\\base\\\Light.h"

#include "framework\physics\PhysicsWorld.h"

#include "framework\debug\Debug.h"
