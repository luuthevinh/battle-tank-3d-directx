#pragma once

#define DIRECTINPUT_VERSION 0x0800

#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>		// dinput8.lib, dxguid.lib
#include <memory>

#define NS_TINY_BEGIN namespace tiny {
#define NS_TINY_END }

#define USING_NS_TINY using namespace tiny

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) \
if(p) \
{\
	delete (p); \
	p = nullptr; \
}
#endif // !SAFE_DELETE

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) \
if (p) \
{\
	p->release(); \
	p = nullptr; \
}
#endif // !SAFE_RELEASE

typedef D3DXVECTOR3 Vec3;
typedef D3DXVECTOR2 Vec2;
typedef D3DXCOLOR Color4F;
typedef D3DXVECTOR4 Vec4;

#define Color3B(r, g, b) D3DCOLOR_XRGB(r, g, b)

#define CALLBACK_01(function, target) std::bind(&function, target, std::placeholders::_1)
#define CALLBACK_02(function, target) std::bind(&function, target, std::placeholders::_1, std::placeholders::_2)

#define Log( message ) \
do \
{ \
	OutputDebugString( message ); \
} while (0)

#define DEF_SHARED_PTR(type) typedef std::shared_ptr<type> Ptr

enum FillMode
{
	MODE_POINT = 1,
	WIREFRAME = 2,
	SOLID = 3,
	FORCE = 0x7fffffff
};

struct Direction
{
	float up;
	float down;
	float left;
	float right;
	float front;
	float behind;

	Direction()
	{
		up = down = left = right = front = behind = 0.0f;
	}
};