#pragma once

#include <d3dx9.h>
#include <vector>
#include "TinyDefinitions.h"

NS_TINY_BEGIN

class Utils
{
public:
	static D3DMATERIAL9 WHITE_MATERIAL;

	static const std::vector<std::string> splitString(const char* source, char delim);
	
	static std::string toString(const wchar_t* source, unsigned int codePage = CP_ACP);

	static float Utils::lerp(float a, float b, float t);

	static Vec3 rgbToVec3(byte red, byte green, byte blue);
private:
	Utils();
};

NS_TINY_END