#pragma once

#include <vector>
#include <map>
#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Collider;
class EventDispatcher;
class Object;

class PhysicsWorld
{
public:
	DEF_SHARED_PTR(PhysicsWorld);

	static Ptr create()
	{
		auto world = Ptr(new PhysicsWorld());
		if(world->init())
			return world;

		return nullptr;
	}

	~PhysicsWorld();

	bool init();
	void update(float dt);

	void addObjectToWorld(const std::shared_ptr<Object>& object);
	
private:
	PhysicsWorld();

	std::vector<std::shared_ptr<Collider>> _colliders;
	std::shared_ptr<EventDispatcher> _dispatcher;

	std::map<std::shared_ptr<Collider>, std::shared_ptr<Collider>> _collidingObjects;

	bool isColliding(const std::shared_ptr<Collider>& colliderA, const std::shared_ptr<Collider>& colliderB);
};

NS_TINY_END