#include "PhysicsWorld.h"
#include "..\component\Collider.h"
#include "..\SceneManager.h"
#include "..\base\EventDispatcher.h"

#include "..\base\EventContact.h"

USING_NS_TINY;

PhysicsWorld::PhysicsWorld()
{
}

PhysicsWorld::~PhysicsWorld()
{
	_colliders.clear();
	_collidingObjects.clear();
}

bool PhysicsWorld::init()
{
	_dispatcher = SceneManager::getInstance()->getEventDispatcher();

	return true;
}

void PhysicsWorld::update(float dt)
{
	// fix me: find new way? linear check is too slow
	auto num = _colliders.size();
	for (size_t i = 0; i < num; i++)
	{
		for (size_t j = 0; j < num; j++)
		{
			if (_colliders[i] != _colliders[j])
			{
				if (isColliding(_colliders[i], _colliders[j]))
				{
					continue;
				}

				if (_colliders[i]->checkCollisionWith(_colliders[j]))
				{
					auto e = EventContact::createBeginEvent(_colliders[i], _colliders[j]);
					_dispatcher->notify(e);

					// cached
					_collidingObjects[_colliders[i]] = _colliders[j];
				}
			}
		}
	}

	// check colliding object
	std::vector<Collider::Ptr> deleting;
	for (auto pairColliding : _collidingObjects)
	{
		if (!pairColliding.first->checkCollisionWith(pairColliding.second))
		{
			auto e = EventContact::createEndEvent(pairColliding.first, pairColliding.second);
			_dispatcher->notify(e);

			// to remove from list
			deleting.push_back(pairColliding.first);
		}
	}

	// remove from list
	for (auto i : deleting)
	{
		_collidingObjects.erase(i);
	}
	
	deleting.clear();
}

void PhysicsWorld::addObjectToWorld(const std::shared_ptr<Object>& object)
{
	auto collider = object->getComponent<Collider>(Component::COLLIDER);
	if (collider == nullptr)
		return;

	_colliders.push_back(collider);
}

bool PhysicsWorld::isColliding(const std::shared_ptr<Collider>& colliderA, const std::shared_ptr<Collider>& colliderB)
{
	auto result = _collidingObjects.find(colliderA);
	if (result == _collidingObjects.end())
		result = _collidingObjects.find(colliderB);

	if(result == _collidingObjects.end())
		return false;

	if ((result->first == colliderA && result->second == colliderB) ||
		(result->first == colliderB && result->second == colliderA))
	{
		return true;
	}

	return false;
}