#pragma once

#include <map>
#include "TinyDefinitions.h"

NS_TINY_BEGIN

class Texture;
class Material;
class Mesh;
class Effect;

class ResourcesManager
{
public:
	static ResourcesManager* getInstance()
	{
		if (_instance == nullptr)
		{
			_instance = new ResourcesManager();
			_instance->init();
		}

		return _instance;
	}

	bool init();
	void release();

	void addMaterial(const char* name, const std::shared_ptr<Material>& material);
	std::shared_ptr<Material> getMaterial(const char* name);

	std::shared_ptr<Texture> loadTexture(const char* name);
	std::shared_ptr<Texture> getTexture(const char* name);

	void addMesh(const char* name, const std::shared_ptr<Mesh>& mesh);
	std::shared_ptr<Mesh> getMesh(const char* name);

	void addEffect(const char* name, const std::shared_ptr<Effect>& effect);
	const std::shared_ptr<Effect>& getEffect(const char* name);

	~ResourcesManager();

private:
	ResourcesManager();
	static ResourcesManager* _instance;

	std::map<std::string, std::shared_ptr<Material>> _materials;
	std::map<std::string, std::shared_ptr<Mesh>> _meshes;
	std::map<std::string, std::shared_ptr<Texture>> _textures;
	std::map<std::string, std::shared_ptr<Effect>> _effects;
};

NS_TINY_END