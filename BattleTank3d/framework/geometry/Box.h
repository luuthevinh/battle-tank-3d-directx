#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Box
{
public:
	DEF_SHARED_PTR(Box);

	static Ptr create(const Vec3 min, const Vec3& max)
	{
		auto box = Ptr(new Box());
		box->maxPosition = max;
		box->minPosition = min;

		return box;
	}

	~Box();

	Vec3 minPosition;
	Vec3 maxPosition;

protected:
	Box();
};

NS_TINY_END