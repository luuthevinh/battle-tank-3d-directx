#include "..\geometry\\Sun.h"
#include "..\SceneManager.h"
#include "..\renderer\SunRenderer.h"
USING_NS_TINY;

DWORD Sun::SUN_FVF = (D3DFVF_XYZ | D3DFVF_NORMAL| D3DFVF_TEX1);

Sun::Sun(bool  debugFlag)
{
	
	this->_debugFlag = debugFlag;
}

Sun::~Sun()
{
	if(_vertexBuffer)
		_vertexBuffer->Release();
	if(_indicesBuffer)
		_indicesBuffer->Release();
}
std::shared_ptr<Light>  Sun::getLight() {
	return _light;
}
bool Sun::init()
{
	Object::init();

	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	_device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
	
	_mesh = Mesh::create();
	this->addComponent(Component::MESH, _mesh);

	_renderer = MeshRenderer::create();
	this->addComponent(Component::RENDERDER, _renderer);

	if (!initVerticesAndIndices())
	{
		return false;
	}
	//load texture
	Material* sunMat = new Material(Material::WHITE_MATERIAL);
	sunMat->texturePaths.insert(std::pair<std::string, std::string>("diffuse", "resources\\sun.png"));
	_mesh->materials.push_back(Material::Ptr(sunMat));

	if (!_mesh->buildMesh()) {
		return false;
	}

	//
	_renderer->setMesh(_mesh);

	//********deprecated************ use Mesh + MeshRenderer instead
		/*auto renderer = new SunRenderer();
		renderer->init();
		renderer->setTexture(texture);
		this->addComponent(Component::RENDERDER, std::shared_ptr<SunRenderer>(renderer));
		*/
	//*******************************

	_light = Light::createDirectionalLight(Vec3(1, -3, 1), D3DXCOLOR(1.0F, 1.0F, 1.0F, 1.0F));
	this->addComponent(Component::LIGHT, _light);
	
	return true;
}
void Sun::update(float dt) {
	auto light = this->getComponent(Component::LIGHT);
	light->update(dt);
}
void Sun::draw()
{
	if (_debugFlag)
	{
		//********deprecated************ use Mesh + MeshRenderer instead
			/*auto renderer = (SunRenderer*)this->getComponent(Component::RENDERDER).get();
			renderer->draw(_vertexBuffer, _indicesBuffer);*/
		//******************************
		// set blending factors so that alpha component determines transparency

		_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		_renderer->draw();

		_device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	}
}

bool Sun::initVerticesAndIndices() {
	
	if (!_mesh)
		return false;
	int row=0, col = 0;

	//quad
	Mesh::Vertex	v[] =
	{
		{ Vec3( -1,-1, 0),Vec3( 0, 0, 0),Vec2( 0, 1) },
		{ Vec3(-1, 1, 0),Vec3(0, 0, 0),Vec2(0, 0) },
		{ Vec3(1, 1, 0),Vec3(0, 0, 0),Vec2(1, 0) },
		{ Vec3(1,-1, 0),Vec3(0, 0, 0),Vec2(1, 1) }
	};

	//_mesh->vertices.push_back(v);
	for(int i=0;i<4;i++)
		_mesh->vertices.push_back(v[i]);

	_mesh->indices.resize(6);
	// add 6 index per quad
	_mesh->indices.push_back(0 );
	_mesh->indices.push_back(1 );
	_mesh->indices.push_back(2 );

	_mesh->indices.push_back(0);
	_mesh->indices.push_back(2);
	_mesh->indices.push_back(3);
			
	return true;
}

bool Sun::initVertexBuffer()
{
	//quad
	Sun::SunVertex	v[] =
	{
		{ -1,	-1, 0, 0, 0, 0, 0, 1 },
		{ -1,	 1, 0, 0, 0, 0, 0, 0 },
		{  1,	 1, 0, 0, 0, 0, 1, 0 },
		{  1,	-1, 0, 0, 0, 0, 1, 1 }
	};
	//create vertex buffer
	_device->CreateVertexBuffer(4 * sizeof(Sun::SunVertex),
		0,
		Sun::SUN_FVF,
		D3DPOOL_MANAGED,
		&_vertexBuffer,
		NULL
	);
	
	//// lock vertexbuffer and load vetices into it
	VOID* pVoid;
	_vertexBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, v, sizeof(v));
	_vertexBuffer->Unlock();

	//_device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	return true;
}

bool Sun::initIndicesBuffer()
{
	//// indices
	short i[] =
	{
		0,1,2,
		0,2,3
	};
	

	//// create an index buffer interface called i_buffer
	_device->CreateIndexBuffer(6 * sizeof(short),
		0,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&_indicesBuffer,
		NULL);

	//// lock indicesBuffer and load the indices into it
	VOID* pVoid;
	_indicesBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, i, sizeof(i));
	_indicesBuffer->Unlock();

	return true;
}
