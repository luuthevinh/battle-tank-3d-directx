
#include "..\base\Material.h"
#include "..\component\Mesh.h"
#include "..\renderer\MeshRenderer.h"
#include "..\SceneManager.h"
#include "Terrain.h"
#include <fstream>
#include "..\ResourcesManager.h"
#include "..\Utils.h"
#include "..\effect\PhongEffect.h"
#include "..\component\BoundingBox.h"
USING_NS_TINY;

DWORD Terrain::TERRAIN_FVF = (D3DFVF_XYZ | D3DFVF_TEX1);

Terrain::Terrain() {

}



bool Terrain::initWithData(std::string heightMapFileName,
	int numVertsPerRow,
	int numVertsPerCol,
	int cellSpacing,
	float heightScale)
{
	Object::init();

	// assign values
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();



	_numVertsPerRow = numVertsPerRow;
	_numVertsPerCol = numVertsPerCol;
	_heightScale = heightScale;
	_cellSpacing = cellSpacing;
	_heightMapFileName = heightMapFileName;
	// 1 cell mean 1 quad 
	_numCellsPerRow = _numVertsPerRow - 1;
	_numCellsPerCol = _numVertsPerCol - 1;

	// total vertices
	_numVertices = _numVertsPerCol * _numVertsPerRow;
	// total quads(cells) * 2 = total tris
	_numTriangles = _numCellsPerRow * _numCellsPerCol * 2;

	_width = _numCellsPerRow * _cellSpacing;
	_depth = _numCellsPerCol * _cellSpacing;
	//read height map from file
	if (!readRawFile(_heightMapFileName))
	{
		MessageBox(0, "readRawFile - failed", 0, 0);
		return false;
	}
	// scale heights
	for (int i = 0; i < _heightmap.size(); i++)
		_heightmap[i] *= _heightScale;

	// add components

	_mesh = Mesh::create();
	this->addComponent(Component::MESH, _mesh);

	_renderer = MeshRenderer::create();
	this->addComponent(Component::RENDERDER, _renderer);

	
	//_renderer->setEffect(Effect::get("phong"));

	// calculate vertices and indices
	if (!computeVertices() || !computeIndices())
	{
		return false;
	}

	
	//should call loadTexture after init

	return true;
}

bool Terrain::loadTexture(const char* fileName,Vec3* direction) {
	//load texture
	_tex = ResourcesManager::getInstance()->getTexture(fileName);

	Material* terrainMat = new Material(Material::WHITE_MATERIAL);
	//terrainMat->texturePaths.insert(std::pair<std::string,std::string>("diffuse", "resources\\grass.png"));
	terrainMat->textures.insert(std::pair<std::string, std::shared_ptr<Texture>>("diffuse", _tex));

	Vec3 testLightDirection(0.0f, 1.0f, 0.0f);
	D3DXVec3Normalize(&testLightDirection, direction);
	if (!lightTerrain(&testLightDirection))
		return false;

	_mesh->materials.push_back(std::shared_ptr<Material>(terrainMat));
	if (!_mesh->buildMesh()) {
		return false;
	}

	_renderer->setMesh(_mesh);

	return true;
}

bool Terrain::lightTerrain(Vec3* direction) {
	

	D3DSURFACE_DESC textureDesc;
	auto _lpd3dtex = _tex->getD3DTexture();
	_lpd3dtex->GetLevelDesc(0, &textureDesc);
	
	// make sure we got the requested format because our code that fills the
	// texture is hard coded to a 32 bit pixel depth.
	if (textureDesc.Format != D3DFMT_X8R8G8B8 && textureDesc.Format != D3DFMT_A8R8G8B8)
		return false;
	D3DLOCKED_RECT lockedRect;
	_lpd3dtex->LockRect(
		0,
		&lockedRect,
		0,
		0
	);

	DWORD* imgData = (DWORD*)lockedRect.pBits;
	for (int i = 0; i < textureDesc.Width; i++)
	{
		for (int j = 0; j < textureDesc.Height; j++)
		{
			//using pitch and divide by 4 as pitch is given in bytes and 4 bytes/ DWORD
			int index = i * lockedRect.Pitch/4  + j;

			D3DXCOLOR c(imgData[index ]);
			//get current color of quad
			//D3DCOLOR c(imgData[index]);

			//shade curr quad
			c *= computeShade(i, j, direction);

			imgData[index] = (DWORD)c;
		}

	}
	_lpd3dtex->UnlockRect(0);
	return true;
}
float Terrain::computeShade(int cellRow, int cellCol, Vec3* direction) {
	if (cellRow >= _numCellsPerRow || cellCol >= _numCellsPerCol)
	{
		_normalSurfaceMap.push_back(Vec3(0,1,0));
		return 1.0f;
	}
	float	heightA = getHeightmapEntry(cellRow, cellCol);
	float	heightB = getHeightmapEntry(cellRow, cellCol+1);
	float	heightC = getHeightmapEntry(cellRow+1, cellCol );

	Vec3 u(_cellSpacing, heightB - heightA, 0.0f);
	Vec3 v(0.0f, heightC - heightA, -_cellSpacing);

	Vec3 n;
	// cross product
	D3DXVec3Cross(&n, &u, &v);
	// get normalize
	D3DXVec3Normalize(&n, &n);

	_normalSurfaceMap.push_back(n);
	// dot product find cos alpha bettwen 2 vectors
	float cosine = D3DXVec3Dot(&n, direction);

	if (cosine < 0.0f)
		cosine = 0.0f;
	return cosine;
}
bool Terrain::init() {
	//default terrain
	Object::init();
	return initWithData("resources\\coastMountain64.raw", 64, 64, 10, 0.5f);
}

void Terrain::update(float dt) {
	for (int i = 0; i < _ontoGameObjectList.size(); i++)
	{
		auto tf = _ontoGameObjectList.at(i)->getTransform();
		auto position = tf->position;
		auto rotation = tf->rotate;

		auto mesh =  _ontoGameObjectList.at(i)->getComponent<BoundingBox>(Component::COLLIDER);
		
		if (mesh != nullptr)
		{
			auto normal = tf->normal;
			auto minPos = mesh->minPosition;
			auto maxPos = mesh->maxPosition;
			auto width = abs(maxPos.x - minPos.x);
			auto depth = abs(maxPos.z - minPos.z);

			Vec3 frontCenterPos, frontLeftPos, frontRightPos, backCenterPos;

			frontCenterPos = backCenterPos = position;
			frontCenterPos.z += depth / 4;
			frontLeftPos = frontRightPos = frontCenterPos;

			frontLeftPos.x -= width / 2;
			frontRightPos.x += width / 2;

			backCenterPos.z -= depth / 4;


			float frontHeight = this->getHeight(frontCenterPos.x, frontCenterPos.z);
			float backHeight = this->getHeight(backCenterPos.x, backCenterPos.z);

			float opp = abs(frontHeight - backHeight);
			float distance = D3DXVec3Length(&(frontCenterPos - backCenterPos));

			float arcsinRoll = opp / distance;
			if (arcsinRoll > 1.0f)
				arcsinRoll = 1.0f;
			else if (arcsinRoll < -1.0f)
				arcsinRoll = -1.0f;

			float roll = asin(arcsinRoll);

			rotation.z = D3DXToDegree(roll);
		}

		float height = this->getHeight(position.x, position.z);
		position.y = height + 0.0f;

		//
		//D3DXMATRIX rotateMat;
		//D3DXQUATERNION quaternion;
		//D3DXQuaternionRotationYawPitchRoll(&quaternion, D3DXToRadian(rotation.y), D3DXToRadian(rotation.x), D3DXToRadian(rotation.z));
		//D3DXMatrixRotationQuaternion(&rotateMat, &quaternion);
		//D3DXVec3Transform(&(Vec4)normal, &normal, &rotateMat);

		//// adjust translation
		//float height = getHeight(position.x, position.z);
		//// adjust rotation
		////Vec3 myNormal = this->getTransform()->getNormal();

		//position.y = height + 1.0f;

		//Vec3 normalSurface = getNormalSurface(position.x, position.z);
		//float alpha = 0.0f; //rad
		//float dot = D3DXVec3Dot(&normal, &normalSurface);
		//

		//Vec3 cross;
		//D3DXVec3Cross(&cross,  &normal,&normalSurface );
		//float crossValue = D3DXVec3Length(&cross); // normalSurface.x * _up.y - normalSurface.y * _up.x;
		////alpha = atan2(crossValue, dot) + D3DX_PI;

		////alpha = acos(D3DXVec3Dot(&normalSurface, &myNormal) / (D3DXVec3Length(&normalSurface) * D3DXVec3Length(&myNormal)));
		//float lengthNormalSurface = D3DXVec3Length(&normalSurface);
		//float lengthUp = D3DXVec3Length(&normal);
		//float cosAlpha = (dot / (lengthNormalSurface * lengthUp));
		//
		//if (cosAlpha > 1.0f)
		//	cosAlpha = 1.0f;
		//else if (cosAlpha < -1.0f)
		//	cosAlpha = -1.0f;

		//alpha = acos(cosAlpha);
		//if (crossValue < 0)
		//	alpha -= 180;
		//if (alpha != 0 )
		//{
		//	
		//	rotation.x = D3DXToDegree(alpha);// *dt
		//	tf->setNormal(normalSurface);
		//}
		//
		
		tf->setPosition(position);
		tf->setRotate(rotation);
		
	}
}
void Terrain::draw() {
	
	_renderer->draw();
}

bool Terrain::computeVertices() {
	
	if (!_mesh)
	{
		//Require mesh component
		return false;
	}
	// coordinates to start generating vertices at
	int startX = -_width / 2;
	int startZ = _depth / 2;

	// coordinates to end generating vertices at
	int endX = _width / 2;
	int endZ = -_depth / 2;

	// compute the increment size of the texture coordinates
	// from one vertex to the next.
	float uCoordIncrementSize = 1.0f / (float)_numCellsPerRow;
	float vCoordIncrementSize = 1.0f / (float)_numCellsPerCol;


	_mesh->vertices.resize(_numVertices);

	//Mesh::Vertex* v = new Mesh::Vertex[_numVertices];
	int index = 0;
	int i = 0;
	for (int z = startZ; z >= endZ; z -= _cellSpacing)
	{
		int j = 0;
		for (int x = startX; x <= endX; x += _cellSpacing)
		{
			// compute the correct index into the vertex buffer and heightmap
			// based on where we are in the nested loop.
			index = i * _numVertsPerRow + j;
		
			_mesh->vertices[index] = Mesh::Vertex(
				Vec3(	// position
					(float)x,
					(float)_heightmap[index],
					(float)z
				),
				Vec2(	// uv coord
					(float)j * uCoordIncrementSize,
					(float)i * vCoordIncrementSize
				));

			j++; // next column
		}
		i++; // next row
	}
	
	////copy vertex to mesh vertices
	//for (int i = 0; i < index; i++) {
	//	_mesh->vertices.push_back(v[i]);
	//}
	
	//delete[] v;
	//v = nullptr;
	return true;
}
bool Terrain::computeIndices() {
	if (!_mesh)
	{
		//Require mesh component
		return false;
	}
	//WORD* indices = 0;

	// index to start of a group of 6 indices that describe the
	// two triangles that make up a quad
	//int baseIndex = 0;

	// loop through and compute the triangles of each quad
	for (int i = 0; i < _numCellsPerCol; i++)
	{
		for (int j = 0; j < _numCellsPerRow; j++)
		{
			_mesh->indices.push_back(i   * _numVertsPerRow + j);
			_mesh->indices.push_back(i   * _numVertsPerRow + j + 1);
			_mesh->indices.push_back((i + 1) * _numVertsPerRow + j);
			_mesh->indices.push_back((i + 1) * _numVertsPerRow + j);
			_mesh->indices.push_back(i   * _numVertsPerRow + j + 1);
			_mesh->indices.push_back((i + 1) * _numVertsPerRow + j + 1);
			//*************************************************
			/*indices[baseIndex] = i   * _numVertsPerRow + j;
			indices[baseIndex + 1] = i   * _numVertsPerRow + j + 1;
			indices[baseIndex + 2] = (i + 1) * _numVertsPerRow + j;

			indices[baseIndex + 3] = (i + 1) * _numVertsPerRow + j;
			indices[baseIndex + 4] = i   * _numVertsPerRow + j + 1;
			indices[baseIndex + 5] = (i + 1) * _numVertsPerRow + j + 1;*/

			// next quad
			//baseIndex += 6;
		}
	}


	return true;
}
bool Terrain::readRawFile(std::string fileName) {
	std::vector<BYTE> in(_numVertices);

	std::ifstream inFile(fileName.c_str(), std::ios_base::binary);

	inFile.read(
		(char*)&in[0],
		in.size()
	);

	inFile.close();

	_heightmap.resize(_numVertices);

	for (int i = 0; i < in.size(); i++) {
		_heightmap[i] = in[i];
	}

	//vector in is released automatically
	return true;
}

Terrain::~Terrain() {

	if (!_heightmap.empty())
		_heightmap.clear();
	if (!_normalSurfaceMap.empty())
		_normalSurfaceMap.clear();
	if (!_ontoGameObjectList.empty())
		_ontoGameObjectList.clear();
}

Vec3 Terrain::getNormalSurfaceMapEntry(int row, int col) {
	return _normalSurfaceMap[row * _numVertsPerRow + col];
}
// get height map index by row,col
int Terrain::getHeightmapEntry(int row, int col) {
	return _heightmap[row * _numVertsPerRow + col];
}
// set height map index by row,col
void Terrain::setHeightmapEntry(int row, int col, int value) {
	_heightmap[row * _numVertsPerRow + col] = value;
}


//get normal vector by coorx,coorz
Vec3 Terrain::getNormalSurface(float x, float z){

	// translate from top-left coor (0,0) to origin coor (w/2,h/2)
	x = ((float)_width / 2.0f) + x;
	z = ((float)_depth / 2.0f) - z; // inverse z coor (+z is down)

									//scale down by divide to _cellSacing
									// 1 unit =		_cellspacing	==scale_down===>	1 unit = 1.0f
									// x unit = x * _cellspacing	===============>	x unit = x * 1.0f
	x /= _cellSpacing;
	z /= _cellSpacing;

	// define row,col using x,z which are in origin coor
	// using to query heightmap easily 
	float curCol = floorf(x); // round up 
	float curRow = floorf(z); // round up
	if (curRow > _numCellsPerRow || curCol > _numCellsPerCol
		|| curRow < 0.0f || curCol < 0.0f) {
		// deal simple get A (although it still appear case which is B or C  has higher height than A)
		return Vec3(0,1,0);
	}

	return getNormalSurfaceMapEntry(curRow,curCol);
}


// get currCol and currRow at Vec2(x,z)
// Return Vec2(row,col)
Vec2 Terrain::getApproximatedEntry(float tempx, float tempz) {
	float x = tempx;
	float z = tempz;
	// translate from top-left coor (0,0) to origin coor (w/2,h/2)
	x = ((float)_width / 2.0f) + x;
	z = ((float)_depth / 2.0f) - z; // inverse z coor (+z is down)

									//scale down by divide to _cellSacing
									// 1 unit =		_cellspacing	==scale_down===>	1 unit = 1.0f
									// x unit = x * _cellspacing	===============>	x unit = x * 1.0f
	x /= _cellSpacing;
	z /= _cellSpacing;

	// define row,col using x,z which are in origin coor
	// using to query heightmap easily 
	float curCol = floorf(x); // round up 
	float curRow = floorf(z); // round up

	if (curRow >= _numCellsPerRow || curCol >= _numCellsPerCol
		|| curRow < 0.0f || curCol < 0.0f) {
		// deal simple get A (although it still appear case which is B or C  has higher height than A)
		return Vec2(-1.0f,-1.0f);
	}

	return Vec2(curRow, curCol);
}

// get height (y_pos) position by x_pos, z_pos
float Terrain::getHeight(float x, float z) {
	
	// translate from top-left coor (0,0) to origin coor (w/2,h/2)
	x = ((float)_width / 2.0f) + x;
	z = ((float)_depth / 2.0f) - z; // inverse z coor (+z is down)

									//scale down by divide to _cellSacing
									// 1 unit =		_cellspacing	==scale_down===>	1 unit = 1.0f
									// x unit = x * _cellspacing	===============>	x unit = x * 1.0f
	x /= _cellSpacing;
	z /= _cellSpacing;

	// define row,col using x,z which are in origin coor
	// using to query heightmap easily 
	float curCol = floorf(x); // round up 
	float curRow = floorf(z); // round up
	if (curRow >= _numCellsPerRow || curCol >= _numCellsPerCol
		||curRow < 0.0f || curCol < 0.0f) {
		// deal simple get A (although it still appear case which is B or C  has higher height than A)
		return 0.0f;
	}

							  // re-coor from origin(w/2,h/2) to TOP-LEFT-CELL coor(row,col)
	float dx = x - curCol;
	float dz = z - curRow;

	/* v1 at this time is TOP-LEFT-CELL(row,col)
	A---B
	| / |
	C---D
	*/
	float A = getHeightmapEntry(curRow, curCol);
	float B = getHeightmapEntry(curRow, curCol + 1);
	float C = getHeightmapEntry(curRow + 1, curCol);
	float D = getHeightmapEntry(curRow + 1, curCol + 1);

	float height = 0.0f;
	if (dz < 1.0f - dx)// ABC
	{
		float uy = B - A;
		float vy = C - A;

		height = A + Utils::lerp(0.0f, uy, dx) + Utils::lerp(0.0f, vy, dz);
	}
	else// CBD
	{
		float uy = C - D;
		float vy = B - D;
		height = D + Utils::lerp(0.0f, uy, 1.0f - dx) + Utils::lerp(0.0f, vy, 1.0f - dz);
	}

	return height;

}


bool  Terrain::walkOnTerrain(Transform::Ptr tf,const float bonusApproximateHeight) {
	// adjust height
	
	Vec3 pos = tf->getPosition();
	Vec3 rotate = tf->getRotate();
	float height = getHeight(pos.x, pos.z);
	if (height == 0.0f) { // out of terrain
		return false;
	}

	pos.y = height + bonusApproximateHeight;
	// adjust rotation
	Vec3 normalSurface = getNormalSurface(pos.x, pos.z);
	Vec3 up = Vec3(0,1, 0);

	float alpha = acos( D3DXVec3Dot(&normalSurface, &up) / (D3DXVec3Length(&normalSurface) * D3DXVec3Length(&up)) );

	rotate.z = alpha * 180 / 3.14f;
	if (rotate.y < 0)
		rotate.z = -rotate.z;
	tf->setPosition(pos);
	tf->setRotate(rotate);
	return true;
}


void Terrain::addTransformToTerrain(const std::shared_ptr<Object> transform)
{
	_ontoGameObjectList.push_back(transform);

}