#pragma once

#include "..\\base\Object.h"
#include "..\\base\Light.h"
#include "..\component\Mesh.h"
#include "..\renderer\MeshRenderer.h"
NS_TINY_BEGIN

class Sun : public Object
{
public:
	~Sun();

	typedef std::shared_ptr<Sun> Ptr;

	static Ptr create(bool debugFlag = false)
	{
		auto sun = std::shared_ptr<Sun>(new Sun(debugFlag));
		if(sun->init())
			return sun;
		return nullptr;
	}

	bool init();
	void update(float dt);
	void draw();
	
	std::shared_ptr<Light> getLight();

	struct SunVertex
	{
		FLOAT x, y, z;
		FLOAT nx, ny, nz;
		FLOAT u, v;
	};

	static DWORD SUN_FVF;

protected:
	Sun(bool debugFlag);
private:
	LPDIRECT3DDEVICE9 _device;
	
	bool _debugFlag;
	std::shared_ptr<Mesh> _mesh;
	std::shared_ptr<MeshRenderer> _renderer;
	std::shared_ptr<Light> _light;
	bool initVerticesAndIndices();

	//deprecated
	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;
	LPDIRECT3DINDEXBUFFER9 _indicesBuffer;
	bool initVertexBuffer();
	bool initIndicesBuffer();
};

NS_TINY_END