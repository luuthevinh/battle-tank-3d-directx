#pragma once

#include "..\TinyDefinitions.h"
#include "..\base\Object.h"
#include "..\component\Transform.h"

NS_TINY_BEGIN

class PointLightCube : public Object
{
public:
	typedef std::shared_ptr<PointLightCube> Ptr;

	static Ptr create(const Vec3& position)
	{
		auto light = Ptr(new PointLightCube());
		if (light->init())
		{
			light->getTransform()->setPosition(position);
			return light;
		}

		return nullptr;
	}

	~PointLightCube();

	virtual bool init() override;

private:
	PointLightCube();
};

NS_TINY_END