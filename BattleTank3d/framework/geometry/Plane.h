#pragma once

#include "..\base\Object.h"
#include "..\TinyDefinitions.h"
#include "..\component\Transform.h"

NS_TINY_BEGIN

class MeshRenderer;
class Mesh;

class Plane : public Object
{
public:
	typedef std::shared_ptr<Plane> Ptr;

	static Ptr create(int rows, int cols)
	{
		auto plane = std::shared_ptr<Plane>(new Plane(rows, cols));
		if(plane->init())
			return plane;

		return nullptr;
	}

	~Plane();

	virtual bool init() override;
	virtual void draw() override;

private:
	Plane(int rows, int cols);

	int _rows;
	int _cols;
	int _width;

	std::shared_ptr<MeshRenderer> _renderer;
	std::shared_ptr<Mesh> _mesh;
};

NS_TINY_END