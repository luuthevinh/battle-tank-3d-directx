#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Line
{
public:
	DEF_SHARED_PTR(Line);

	static Ptr create(const Vec3& start, const Vec3& end)
	{
		auto line = Ptr(new Line());
		line->startPosition = start;
		line->endPosition = end;

		return line;
	}

	~Line() { }

	Vec3 startPosition;
	Vec3 endPosition;

private:
	Line() { }
};

NS_TINY_END