#include "Ray.h"
#include "Sphere.h"
#include "..\component\SphereCollider.h"

USING_NS_TINY;

bool Ray::hitTestWithSphere(float radius, const Vec3 & position)
{
	D3DXVECTOR3 v = this->origin - position;

	float b = 2.0f * D3DXVec3Dot(&this->direction, &v);
	float c = D3DXVec3Dot(&v, &v) - (radius * radius);

	// find the discriminant
	float discriminant = (b * b) - (4.0f * c);

	// test for imaginary number
	if (discriminant < 0.0f)
		return false;

	discriminant = sqrtf(discriminant);

	float s0 = (-b + discriminant) / 2.0f;
	float s1 = (-b - discriminant) / 2.0f;

	// if a solution is >= 0, then we intersected the sphere
	if (s0 >= 0.0f || s1 >= 0.0f)
		return true;

	return false;
}

bool Ray::hitTestWithSphere(const std::shared_ptr<Sphere>& sphere)
{
	return this->hitTestWithSphere(sphere->radius, sphere->center);
}

bool Ray::hitTestWithSphereCollider(const std::shared_ptr<SphereCollider>& collider)
{
	return this->hitTestWithSphere(collider->getRadiusInWorld(), collider->getCenterInWorld());
}
