#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class SphereCollider;
class Sphere;

class Ray
{
public:
	DEF_SHARED_PTR(Ray);

	~Ray() { }

	static Ptr create(const Vec3& origin, const Vec3& direction)
	{
		auto ray = Ptr(new Ray());
		ray->origin = origin;
		ray->direction = direction;

		return ray;
	}

	Vec3 origin;
	Vec3 direction;
	
	bool hitTestWithSphere(const std::shared_ptr<Sphere>& sphere);
	bool hitTestWithSphere(float radius, const Vec3& origin);
	bool hitTestWithSphereCollider(const std::shared_ptr<SphereCollider>& collider);

private:
	Ray() { }
};

NS_TINY_END