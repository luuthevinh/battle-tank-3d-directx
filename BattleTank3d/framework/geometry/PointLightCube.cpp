#include "PointLightCube.h"
#include "Cube.h"
#include "..\base\Light.h"

USING_NS_TINY;

PointLightCube::PointLightCube()
{
}

PointLightCube::~PointLightCube()
{
}

bool PointLightCube::init()
{
	Object::init();

	auto cube = Cube::create();
	cube->getTransform()->setScale(0.5f);
	this->addChild("cube", cube);

	auto light = Light::createPointLight(10.0f, Color4F(1.0f, 1.0f, 0.0f, 1.0f));
	this->addComponent(Component::LIGHT, std::shared_ptr<Light>(light));

	return true;
}
