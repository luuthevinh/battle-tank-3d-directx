#include "Cube.h"
#include "..\SceneManager.h"
#include "..\base\Material.h"
#include "..\component\Mesh.h"
#include "..\renderer\MeshRenderer.h"
#include "..\renderer\CubeRenderer.h"

USING_NS_TINY;

DWORD Cube::CUBE_FVF = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

Cube::Cube()
{
}

Cube::~Cube()
{
	if(_vertexBuffer != nullptr)
		_vertexBuffer->Release();

	if(_indicesBuffer != nullptr)
		_indicesBuffer->Release();
}

bool Cube::init()
{
	Object::init();

	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	
	auto mesh = Mesh::create();
	this->addComponent(Component::MESH, mesh);

	auto renderer = MeshRenderer::create();
	this->addComponent(Component::RENDERDER, renderer);

	// cube
	Mesh::Vertex vertices[] =
	{
		Mesh::Vertex(Vec3(-1.0f, -1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec2(0.0f, 1.0f) ),   // side 1
		Mesh::Vertex(Vec3(1.0f, -1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec2(1.0f, 1.0f)),    // side 1
		Mesh::Vertex(Vec3(-1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec2(0.0f, 0.0f)),    // side 1
		Mesh::Vertex(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec2(1.0f, 0.0f)),    // side 1

		Mesh::Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f), Vec2(0.0f, 1.0f)),    // side 2
		Mesh::Vertex(Vec3(-1.0f, 1.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f), Vec2(0.0f, 0.0f)),    // side 2
		Mesh::Vertex(Vec3(1.0f, -1.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f), Vec2(1.0f, 1.0f)),    // side 2
		Mesh::Vertex(Vec3(1.0f, 1.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f), Vec2(1.0f, 0.0f)),    // side 2

		Mesh::Vertex(Vec3(-1.0f, 1.0f, -1.0f), Vec3(0.0f, 1.0f, 0.0f), Vec2(0.0f, 1.0f)),    // side 3
		Mesh::Vertex(Vec3(-1.0f, 1.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f), Vec2(0.0f, 0.0f)),    // side 3
		Mesh::Vertex(Vec3(1.0f, 1.0f, -1.0f), Vec3(0.0f, 1.0f, 0.0f), Vec2(1.0f, 1.0f)),    // side 3
		Mesh::Vertex(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f), Vec2(1.0f, 0.0f)),    // side 3

		Mesh::Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec3(0.0f, -1.0f, 0.0f), Vec2(0.0f, 1.0f)),    // side 4
		Mesh::Vertex(Vec3(1.0f, -1.0f, -1.0f), Vec3(0.0f, -1.0f, 0.0f), Vec2(1.0f, 1.0f)),    // side 4
		Mesh::Vertex(Vec3(-1.0f, -1.0f, 1.0f), Vec3(0.0f, -1.0f, 0.0f), Vec2(0.0f, 0.0f)),    // side 4
		Mesh::Vertex(Vec3(1.0f, -1.0f, 1.0f), Vec3(0.0f, -1.0f, 0.0f), Vec2(1.0f, 0.0f)),    // side 4

		Mesh::Vertex(Vec3(1.0f, -1.0f, -1.0f), Vec3(1.0f, 0.0f, 0.0f), Vec2(0.0f, 1.0f)),    // side 5 16
		Mesh::Vertex(Vec3(1.0f, 1.0f, -1.0f), Vec3(1.0f, 0.0f, 0.0f), Vec2(1.0f, 1.0f)),    // side 5 17
		Mesh::Vertex(Vec3(1.0f, -1.0f, 1.0f), Vec3(1.0f, 0.0f, 0.0f), Vec2(0.0f, 0.0f)),    // side 5 18
		Mesh::Vertex(Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 0.0f, 0.0f), Vec2(1.0f, 0.0f)),    // side 5 19

		Mesh::Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec3(-1.0f, 0.0f, 0.0f), Vec2(0.0f, 1.0f)),    // side 6
		Mesh::Vertex(Vec3(-1.0f, -1.0f, 1.0f), Vec3(-1.0f, 0.0f, 0.0f), Vec2(0.0f, 0.0f)),    // side 6
		Mesh::Vertex(Vec3(-1.0f, 1.0f, -1.0f), Vec3(-1.0f, 0.0f, 0.0f), Vec2(1.0f, 1.0f)),    // side 6
		Mesh::Vertex(Vec3(-1.0f, 1.0f, 1.0f), Vec3(-1.0f, 0.0f, 0.0f), Vec2(1.0f, 0.0f)),    // side 6
	};

	for (size_t i = 0; i < 24; i++)
	{
		mesh->vertices.push_back(vertices[i]);
	}

	short indices[] =
	{
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 5, 6,    // side 2
		6, 5, 7,
		8, 9, 10,    // side 3
		10, 9, 11,
		12, 13, 14,    // side 4
		14, 13, 15,
		16, 17, 18,    // side 5
		18, 17, 19,
		20, 21, 22,    // side 6
		22, 21, 23,
	};

	for (size_t i = 0; i < 36; i++)
	{
		mesh->indices.push_back(indices[i]);
	}

	mesh->materials.push_back(Material::get("white"));
	mesh->buildMesh();

	renderer->setMesh(mesh);

	return true;
}

bool Cube::init(const char* filePath)
{
	Object::init();

	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	if (!(this->initVertexBuffer() && this->initIndicesBuffer()))
	{
		return false;
	}

	LPDIRECT3DTEXTURE9 texture;
	D3DXCreateTextureFromFile(_device, filePath, &texture);

	_device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

	auto renderer = new CubeRenderer();
	renderer->init();
	renderer->setTexture(texture);
	renderer->setMaterial(Material::WHITE_MATERIAL);

	this->addComponent(Component::RENDERDER, std::shared_ptr<CubeRenderer>(renderer));

	return true;
}

void Cube::draw()
{
	Object::draw();

	auto renderer = this->getComponent<MeshRenderer>(Component::RENDERDER);
	renderer->draw();
}

bool Cube::initVertexBuffer()
{
	// cube
	Cube::CubeVertex vertices[] =
	{
		{ -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },    // side 1
		{ 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
		{ -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f },

		{ -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f },    // side 2
		{ -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f },
		{ 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f },
		{ 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f },

		{ -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f },    // side 3
		{ -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f },

		{ -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f },    // side 4
		{ 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f },
		{ -1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f },
		{ 1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f },

		{ 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f },    // side 5
		{ 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f },

		{ -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f },    // side 6
		{ -1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f },
		{ -1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f },
	};

	// create a vertex buffer interface called v_buffer
	_device->CreateVertexBuffer(24 * sizeof(Cube::CubeVertex),
		0,
		Cube::CUBE_FVF,
		D3DPOOL_MANAGED,
		&_vertexBuffer,
		NULL);

	// lock vertexbuffer and load vetices into it
	VOID* pVoid;
	_vertexBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	_vertexBuffer->Unlock();

	return true;
}

bool Cube::initIndicesBuffer()
{
	// indices
	short indices[] =
	{
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 5, 6,    // side 2
		6, 5, 7,
		8, 9, 10,    // side 3
		10, 9, 11,
		12, 13, 14,    // side 4
		14, 13, 15,
		16, 17, 18,    // side 5
		18, 17, 19,
		20, 21, 22,    // side 6
		22, 21, 23,
	};

	// create an index buffer interface called i_buffer
	_device->CreateIndexBuffer(36 * sizeof(short),
		0,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&_indicesBuffer,
		NULL);

	// lock indicesBuffer and load the indices into it
	VOID* pVoid;
	_indicesBuffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indices, sizeof(indices));
	_indicesBuffer->Unlock();

	return true;
}
