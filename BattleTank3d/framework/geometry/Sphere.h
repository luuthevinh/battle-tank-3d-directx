#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Sphere
{
public:
	DEF_SHARED_PTR(Sphere);

	virtual ~Sphere() { }

	static Ptr create(const Vec3& position, float radius)
	{
		auto sphere = Ptr(new Sphere());
		sphere->center = position;
		sphere->radius = radius;

		return sphere;
	}

	Vec3 center;
	float radius;

protected:
	Sphere() { }
};

NS_TINY_END