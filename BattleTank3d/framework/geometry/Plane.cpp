#include "Plane.h"
#include "..\renderer\MeshRenderer.h"
#include "..\component\Mesh.h"
#include "..\base\Material.h"

#include <vector>

USING_NS_TINY;

Plane::Plane(int rows, int cols) :
	_rows(rows),
	_cols(cols)
{
}

Plane::~Plane()
{
}

bool Plane::init()
{
	if (!Object::init())
	{
		return false;
	}

	// mesh component
	_mesh = Mesh::create();
	this->addComponent(Component::MESH, _mesh);

	// renderer component
	_renderer = MeshRenderer::create();
	
	this->addComponent(Component::RENDERDER, _renderer);

	// create vertex
	for (size_t row = 0; row < _rows; row++)
	{
		for (size_t col = 0; col < _cols; col++)
		{
			auto v = Mesh::Vertex(Vec3(row, 0.0f, col), Vec3(0.0f, 1.0f, 0.0f), Vec2(0, 0));
			_mesh->vertices.push_back(v);

			if (row == _rows - 1 || col == _cols - 1) continue;

			// add 6 index per quad
			_mesh->indices.push_back(_cols * (row + 1) + col);
			_mesh->indices.push_back(_cols * (row) + col);
			_mesh->indices.push_back(_cols * (row) + (col + 1));

			_mesh->indices.push_back(_cols * (row + 1) + col);
			_mesh->indices.push_back(_cols * (row) + (col + 1));
			_mesh->indices.push_back(_cols * (row + 1) + (col + 1));
		}
	}

	_mesh->materials.push_back(Material::get("white"));
	_mesh->setFillMode(FillMode::SOLID);
	_mesh->buildMesh();

	_renderer->setMesh(_mesh);

	return true;
}

void Plane::draw()
{
	Object::draw();
	_renderer->draw();
}
