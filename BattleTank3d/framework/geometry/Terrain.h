#pragma once

#include "..\\base\Object.h"
#include "..\base\Texture.h"
#include "..\renderer\MeshRenderer.h"
#include "..\Camera.h"
NS_TINY_BEGIN

class Terrain : public Object
{
public:
	
	typedef std::shared_ptr<Terrain> Ptr;

	static Ptr create(std::string heightMapFileName,
		int numVertsPerRow,
		int numVertsPerCol,
		int cellSpacing,
		float heightScale)
	{
		auto terrain = std::shared_ptr<Terrain>(new Terrain());
		if (terrain->initWithData(heightMapFileName,
			numVertsPerRow,
			numVertsPerCol,
			cellSpacing,
			heightScale))
		{
			return terrain;
		}

		return nullptr;
	}
	~Terrain();


	void addTransformToTerrain(const std::shared_ptr<Object>  transform);

	bool init();
	bool initWithData(std::string heightMapFileName,
		int numVertsPerRow,
		int numVertsPerCol,
		int cellSpacing,
		float heightScale);
	//add texture and process lighting onto terrain
	bool loadTexture(const char* fileName, Vec3* direction);
	void update(float dt);
	void draw();

	// get height map index by row,col
	int getHeightmapEntry(int row, int col);
	// set height map index by row,col
	void setHeightmapEntry(int row, int col, int value);

	// get normal vector by row,col
	Vec3 getNormalSurfaceMapEntry(int row, int col);
	// get height (y_pos) position by x_pos, z_pos
	float getHeight(float x, float z);

	Vec2 getApproximatedEntry(float x, float z);

	Vec3 getNormalSurface(float x, float z);

	bool walkOnTerrain(Transform::Ptr tf , const float bonusApproximateHeight);


	struct TerrainVertex
	{
		FLOAT _x, _y, _z;
		FLOAT _u, _v;
		TerrainVertex(float x, float y, float z, float u, float v) {
			_x = x;
			_y = y;
			_z = z;
			_u = u;
			_v = v;
		}
	};
	static DWORD TERRAIN_FVF;
protected:
	// we should specify exactly number of vertices per row and col
	// normally is (width x height) of the picture
	Terrain();

private:


	LPDIRECT3DDEVICE9 _device;
	std::shared_ptr<tiny::Texture> _tex;
	std::shared_ptr<Mesh> _mesh;
	std::shared_ptr<MeshRenderer > _renderer;

	std::string _heightMapFileName;
	// number of vertices per row
	int _numVertsPerRow; 
	// number of  vertices per column
	int _numVertsPerCol;
	// cell spacing
	int _cellSpacing;

	// number of cells per row  
	int _numCellsPerRow;
	// number of cells per col 
	int _numCellsPerCol;
	// width of terrain
	int _width;
	// depth of terrain
	int _depth;
	// total number of vertices
	int _numVertices;
	// total number of triangles 
	int _numTriangles;
	// height scale
	float _heightScale; 

	//height map onto file
	std::vector<int> _heightmap;
	std::vector<Vec3> _normalSurfaceMap;

	// helper methods
	bool readRawFile(std::string fileName);
	bool computeVertices();
	bool computeIndices();
	bool lightTerrain(Vec3* direction);
	float computeShade(int cellRow, int cellCol, Vec3* direction);

	// handle transform onto terrain
	std::vector<std::shared_ptr<Object>> _ontoGameObjectList;
};

NS_TINY_END