#pragma once

#include "..\TinyDefinitions.h"
#include "..\base\Object.h"
#include "..\component\Transform.h"

NS_TINY_BEGIN

class Cube : public Object
{
public:
	typedef std::shared_ptr<Cube> Ptr;

	static Ptr create()
	{
		auto cube = std::shared_ptr<Cube>(new Cube());
		if (cube->init())
		{
			return cube;
		}

		return nullptr;
	}

	~Cube();

	bool init();
	bool init(const char* filePath);
	void draw();

	struct CubeVertex
	{
		FLOAT x, y, z;
		FLOAT nx, ny, nz;
		FLOAT u, v;
	};

	static DWORD CUBE_FVF;

protected:
	Cube();

private:
	LPDIRECT3DDEVICE9 _device;
	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;
	LPDIRECT3DINDEXBUFFER9 _indicesBuffer;

	bool initVertexBuffer();
	bool initIndicesBuffer();
};

NS_TINY_END