#pragma once

#include <vector>
#include "base\Scene.h"
#include "TinyDefinitions.h"
#include "Direct3DDevice.h"

NS_TINY_BEGIN

class EventDispatcher;
class Camera;

class SceneManager
{
public:
	static SceneManager* getInstance();
	~SceneManager();

	void addScene(const Scene::Ptr& scene);
	void removeScene();
	void replaceScene(const Scene::Ptr& scene);
	void clearScenes();

	void update(float dt);
	void draw();
	void release();

	Scene::Ptr getCurrentScene();

	void setDevice(Direct3DDevice* device);
	Direct3DDevice* getDevice();

	void setDispatcher(std::shared_ptr<EventDispatcher> dispatcher);
	std::shared_ptr<EventDispatcher> getEventDispatcher();

	std::shared_ptr<Camera> getMainCamera();
	void setMainCamera(std::shared_ptr<Camera> camera);

	void mainCameraUpdate();
	void createDefaultCamera();

	inline bool isDebug()
	{
		return _isDebug;
	}

	inline void setDebug(bool debug)
	{
		_isDebug = debug;
	}

private:
	SceneManager();
	static SceneManager* _instance;

	std::vector<Scene::Ptr> _scenes;
	Direct3DDevice* _device;
	std::shared_ptr<EventDispatcher> _dispatcher;

	std::shared_ptr<Camera> _mainCamera;

	bool _isDebug;
};

NS_TINY_END
