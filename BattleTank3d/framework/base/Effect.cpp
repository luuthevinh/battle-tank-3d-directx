#include "Effect.h"
#include "..\SceneManager.h"
#include "Renderer.h"
#include "..\Camera.h"

USING_NS_TINY;

Effect::Effect()
{
}

Effect::~Effect()
{
	if(_effect != nullptr)
		_effect->Release();
}

unsigned int Effect::begin(const char* techniqueName)
{
	if (_effect == nullptr)
		return 0;

	if (_currentTechniqueName.compare(techniqueName) != 0)
	{
		this->useTechnique(techniqueName);
	}

	UINT numPasses = 0;
	_effect->Begin(&numPasses, 0);

	D3DXMATRIX project;
	D3DXMATRIX view;
	D3DXMATRIX world;

	auto camera = SceneManager::getInstance()->getMainCamera();
	view = camera->getViewMatrix();
	project = camera->getPerspectiveMatrix();
	world = camera->getWorldMatrix();

	//_device->GetTransform(D3DTS_VIEW, &view);
	//_device->GetTransform(D3DTS_PROJECTION, &project);
	//_device->GetTransform(D3DTS_WORLD, &world);

	_effect->SetMatrix(this->getConstantHandle("World"), &world);
	_effect->SetMatrix(this->getConstantHandle("View"), &view);
	_effect->SetMatrix(this->getConstantHandle("Projection"), &project);

	return numPasses;
}

void Effect::end()
{
	if (_effect == nullptr)
		return;

	_effect->End();
}

void Effect::onBeginPass(const std::shared_ptr<Material>& material)
{
}

bool Effect::initWithFile(const char * effectFile)
{
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	ID3DXBuffer* errorBuffer = 0;
	auto result = D3DXCreateEffectFromFile(
		_device,
		effectFile,
		0,                // no preprocessor definitions
		0,                // no ID3DXInclude interface
		D3DXSHADER_DEBUG, // compile flags
		0,                // don't share parameters
		&_effect,
		&errorBuffer);

	// output any error messages
	if (errorBuffer)
	{
		Log((char*)errorBuffer->GetBufferPointer());
		errorBuffer->Release();
		return false;
	}

	if (FAILED(result))
	{
		Log("D3DXCreateEffectFromFile() - FAILED");
		return false;
	}

	return true;
}

void Effect::useTechnique(const char * name)
{
	if (_effect == nullptr)
	{
		Log("effect is null");
		return;
	}
	
	_currentTechniqueName = name;

	if (_techniques.find(name) != _techniques.end())
	{
		_currentTechnique = _techniques.at(name);
		_effect->SetTechnique(_currentTechnique);
		return;
	}

	auto handle = _effect->GetTechniqueByName(name);
	if (handle != nullptr)
	{
		_techniques[name] = handle;
		_currentTechnique = handle;
		_effect->SetTechnique(_currentTechnique);
		return;
	}

	Log("can't find technique");
}

D3DXHANDLE Effect::getConstantHandle(const char * name)
{
	if (_effect == nullptr)
		return nullptr;

	if (_constantHandles.find(name) != _constantHandles.end())
		return _constantHandles.at(name);

	auto handle = _effect->GetParameterByName(0, name);
	if (handle != nullptr)
	{
		_constantHandles[name] = handle;
		return handle;
	}

	return nullptr;
}

ID3DXEffect* Effect::getD3DEffect()
{
	return _effect;
}

void Effect::setTarget(const std::shared_ptr<Renderer>& target)
{
	_target = target;
}

std::shared_ptr<Renderer> Effect::getTarget()
{
	return _target;
}

