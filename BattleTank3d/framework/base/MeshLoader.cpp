﻿#include "MeshLoader.h"
#include "..\component\Mesh.h"
#include "Material.h"
#include "..\Utils.h"
#include "OBJFileReader.h"
#include "Texture.h"

#include <fstream>

USING_NS_TINY;

D3DVERTEXELEMENT9 MeshLoader::VERTEX_DECL[] =
{
	{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_NORMAL,   0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_TEXCOORD, 0 },
	D3DDECL_END()
};

ID3DXMesh* MeshLoader::loadFromXFile(Mesh* target, const char* filePath, LPDIRECT3DDEVICE9 device)
{
	ID3DXBuffer* adjBuffer = nullptr;
	ID3DXBuffer* mtrlBuffer = nullptr;
	ID3DXMesh* mesh = nullptr;
	DWORD numMtrls = 0;

	auto path = std::string(filePath);
	auto relativePath = path.substr(0, path.find_last_of('\\') + 1);

	HRESULT result = D3DXLoadMeshFromX(
		filePath,
		D3DXMESH_MANAGED,
		device,
		&adjBuffer,
		&mtrlBuffer,
		0,
		&numMtrls,
		&mesh
	);

	if (FAILED(result))
	{
		OutputDebugString("Load mesh from X FAILED!!!");
		return nullptr;
	}

	if (mtrlBuffer != NULL && numMtrls != 0)
	{
		D3DXMATERIAL* mtrls = (D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

		for (size_t i = 0; i < numMtrls; i++)
		{
			// ko có ambient nên phải gán lại
			mtrls[i].MatD3D.Ambient = mtrls[i].MatD3D.Diffuse;

			LPDIRECT3DTEXTURE9 texture = NULL;

			auto material = Material::createWithMaterial(mtrls[i].MatD3D);

			// check coi có file texture ko
			if (mtrls[i].pTextureFilename != 0)
			{
				auto p = relativePath + mtrls[i].pTextureFilename;
				material->textures["diffuse"] = Texture::get(p.c_str());
			}

			// add vào target
			target->addMaterial(material);
		}

		mtrlBuffer->Release();
	}

	if (adjBuffer != NULL)
	{
		result = mesh->OptimizeInplace(
			D3DXMESHOPT_ATTRSORT |
			D3DXMESHOPT_COMPACT |
			D3DXMESHOPT_VERTEXCACHE,
			(DWORD*)adjBuffer->GetBufferPointer(),
			0, 0, 0);

		if (FAILED(result))
		{
			OutputDebugString("OptimizeInplace() FAILED!!!");

			// release
			adjBuffer->Release();
			mesh->Release();

			return nullptr;
		}

		adjBuffer->Release();
	}

	// tính normal vector
	if (!(mesh->GetFVF() & D3DFVF_NORMAL))
	{
		LPD3DXMESH tempMesh;
		result = mesh->CloneMeshFVF(D3DXMESH_MANAGED, mesh->GetFVF() | D3DFVF_NORMAL, device, &tempMesh);

		if (FAILED(result))
		{
			OutputDebugString("CloneMeshFVF() FAILED!!!");
			mesh->Release();

			return nullptr;
		}

		result = D3DXComputeNormals(tempMesh, 0);

		if (FAILED(result))
		{
			OutputDebugString("D3DXComputeNormals() FAILED!!!");

			mesh->Release();
			tempMesh->Release();

			return nullptr;
		}

		// xóa cái cũ gán cái mới tính vào
		mesh->Release();
		mesh = tempMesh;
	}

	return mesh;
}

ID3DXMesh* MeshLoader::loadFromOBJFile(Mesh * target, const char * filePath, LPDIRECT3DDEVICE9 device)
{
	auto reader = new OBJFileReader(filePath);
	reader->readData();

	auto indices = reader->getIndices();
	auto vertices = reader->getVertices();
	auto attributes = reader->getAttributes();

	// Create the encapsulated mesh
	ID3DXMesh* pMesh = nullptr;

	D3DXCreateMesh(indices.size() / 3, vertices.size(),
		D3DXMESH_MANAGED | D3DXMESH_32BIT, VERTEX_DECL,
		device, &pMesh);

	// Copy the vertex data
	Mesh::Vertex* pVertex;
	pMesh->LockVertexBuffer(0, (void**)&pVertex);
	memcpy(pVertex, vertices.data(), vertices.size() * sizeof(Mesh::Vertex));
	pMesh->UnlockVertexBuffer();
	vertices.clear();

	// Copy the index data
	DWORD* pIndex;
	pMesh->LockIndexBuffer(0, (void**)&pIndex);
	memcpy(pIndex, indices.data(), indices.size() * sizeof(DWORD));
	pMesh->UnlockIndexBuffer();
	indices.clear();

	// Copy the attribute data
	DWORD* pSubset;
	pMesh->LockAttributeBuffer(0, &pSubset);
	memcpy(pSubset, attributes.data(), attributes.size() * sizeof(DWORD));
	pMesh->UnlockAttributeBuffer();
	attributes.clear();

	// Optimize mesh
	DWORD* aAdjacency = new DWORD[pMesh->GetNumFaces() * 3];
	if (aAdjacency == NULL)
	{
		return nullptr;
	}

	pMesh->GenerateAdjacency(1e-6f, aAdjacency);
	pMesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, aAdjacency, NULL, NULL, NULL);

	delete[] aAdjacency;

	// tính normal vector
	if (!(pMesh->GetFVF() & D3DFVF_NORMAL))
	{
		LPD3DXMESH tempMesh;
		pMesh->CloneMeshFVF(D3DXMESH_MANAGED, pMesh->GetFVF() | D3DFVF_NORMAL, device, &tempMesh);

		D3DXComputeNormals(tempMesh, 0);

		pMesh->Release();
		pMesh = tempMesh;
	}

	// create textures
	auto materials = reader->getMaterials();
	for (int iMaterial = 0; iMaterial < materials.size(); iMaterial++)
	{
		auto pMaterial = materials.at(iMaterial);
		if (pMaterial->texturePaths["diffuse"][0])
		{
			// Avoid loading the same texture twice
			bool bFound = false;
			for (int x = 0; x < iMaterial; x++)
			{
				auto pCur = materials.at(x);
				if (0 == pCur->texturePaths["diffuse"].compare(pMaterial->texturePaths["diffuse"]))
				{
					bFound = true;
					pMaterial->textures["diffuse"] = pCur->textures["diffuse"];
					break;
				}
			}

			// Not found, load the texture
			if (!bFound)
			{
				auto path = reader->getRelativePath() + pMaterial->texturePaths["diffuse"];
				pMaterial->textures["diffuse"] = Texture::get(path.c_str());
			}
		}
	}

	for (size_t i = 0; i < materials.size(); i++)
	{
		target->addMaterial(materials[i]);
	}

	delete reader;

	return pMesh;
}
