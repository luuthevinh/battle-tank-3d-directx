#pragma once

#include <functional>
#include "EventListener.h"
#include "EventKeyboard.h"

NS_TINY_BEGIN

class EventKeyboardListener : public EventListener
{
public:
	typedef std::shared_ptr<EventKeyboardListener> Ptr;

	static Ptr create()
	{
		auto listenter = std::shared_ptr<EventKeyboardListener>(new EventKeyboardListener());
		return listenter;
	}

	~EventKeyboardListener();

	// Inherited via EventListener
	virtual void onNotify(Event::Ptr event) override;

	std::function<void(EventKeyboard::KeyCode)> onKeyPressed;
	std::function<void(EventKeyboard::KeyCode)> onKeyReleased;

private:
	EventKeyboardListener();
};

NS_TINY_END