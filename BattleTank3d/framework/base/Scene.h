#pragma once

#include "..\TinyDefinitions.h"
#include "Object.h"
#include "..\physics\PhysicsWorld.h"

NS_TINY_BEGIN

class Scene : public Object
{
public:
	typedef std::shared_ptr<Scene> Ptr;

	virtual ~Scene() { }

	virtual bool init() = 0;
	virtual void release() { }

	inline const PhysicsWorld::Ptr& getPhysicsWorld()
	{
		return _physicsWorld;
	}

	inline void setPhysicsWorld(const PhysicsWorld::Ptr& world)
	{
		_physicsWorld = world;
	}

protected:
	Scene() { }

	PhysicsWorld::Ptr _physicsWorld;
};

NS_TINY_END