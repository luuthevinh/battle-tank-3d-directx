#include "Model.h"
#include "..\component\Mesh.h"
#include "MeshFileReader.h"
#include "MeshLoader.h"
#include "..\base\Shader.h"
#include "..\renderer\MeshRenderer.h"
#include "..\SceneManager.h"
#include "OBJFileReader.h"
#include "..\ResourcesManager.h"
#include "Effect.h"
#include "..\component\BoundingBox.h"

USING_NS_TINY;

Model::Model() : Object()
{
}

Model::~Model()
{
}

bool Model::init()
{
	if (!Object::init())
	{
		return false;
	}

	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	// mesh component
	_mesh = Mesh::create();
	this->addComponent(Component::MESH, _mesh);

	// renderer component
	_renderer = MeshRenderer::create();
	_renderer->setMesh(_mesh);
	this->addComponent(Component::RENDERDER, _renderer);

	return true;
}

bool Model::initWithFile(const char * filePath)
{
	if (!Object::init())
	{
		return false;
	}

	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	// renderer component
	_renderer = MeshRenderer::create();
	this->addComponent(Component::RENDERDER, _renderer);

	auto mesh = ResourcesManager::getInstance()->getMesh(filePath);
	if (mesh != nullptr)
	{
		_mesh = mesh;
		_renderer->setMesh(_mesh);
		return true;
	}

	// mesh component
	_mesh = Mesh::create();
	this->addComponent(Component::MESH, _mesh);

	auto reader = new MeshFileReader(filePath);
	if (!reader->readData())
		return false;

	_mesh->indices = reader->getIndices();
	_mesh->vertices = reader->getVertices();
	_mesh->attributes = reader->getAttributes();
	_mesh->materials = reader->getMaterials();
	_mesh->numSubsets = reader->getNumMesh();

	_mesh->buildMesh();

	_renderer->setMesh(_mesh);

	ResourcesManager::getInstance()->addMesh(filePath, _mesh);

	delete reader;

	return true;
}

bool Model::initWithXFile(const char * filePath)
{
	if (!this->init())
	{
		return false;
	}

	// load from x file
	auto d3dMesh = MeshLoader::loadFromXFile((Mesh*)_mesh.get(), filePath, _device);
	if (d3dMesh == nullptr)
	{
		return false;
	}

	_mesh->setD3DMesh(d3dMesh);

	return true;
}

bool Model::initWithOBJFile(const char * filePath)
{
	if (!this->init())
	{
		return false;
	}

	auto reader = new OBJFileReader(filePath);
	reader->readData();

	_mesh->indices = reader->getIndices();
	_mesh->vertices = reader->getVertices();
	_mesh->attributes = reader->getAttributes();
	_mesh->materials = reader->getMaterials();
	_mesh->numSubsets = reader->getNumberOfSubsets();

	_mesh->buildMesh();

	delete reader;

	return true;
}

void Model::draw()
{
	Object::draw();

	_renderer->draw();
}
