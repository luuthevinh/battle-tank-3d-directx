#pragma once

#include <map>
#include <vector>
#include "..\TinyDefinitions.h"
#include "..\ResourcesManager.h"

NS_TINY_BEGIN

class Renderer;
class Material;

class Effect
{
public:
	typedef std::shared_ptr<Effect> Ptr;

	static Ptr createWithFile(const char* effectFile)
	{
		auto effect = Ptr(new Effect());
		if (effect->initWithFile(effectFile))
			return effect;

		return nullptr;
	}

	static Ptr get(const char* name)
	{
		auto effect = ResourcesManager::getInstance()->getEffect(name);
		return effect;
	}

	virtual ~Effect();

	// return number of passes
	virtual unsigned int begin(const char* techniqueName);
	virtual void end();

	virtual void onBeginPass(const std::shared_ptr<Material>& material);

	virtual bool initWithFile(const char* effectFile);

	void useTechnique(const char* name);
	const char* getCurrentTechiqueName() { return _currentTechniqueName.c_str(); }

	D3DXHANDLE getConstantHandle(const char* name);

	ID3DXEffect* getD3DEffect();

	void setTarget(const std::shared_ptr<Renderer>& target);
	std::shared_ptr<Renderer> getTarget();

protected:
	Effect();

	ID3DXEffect* _effect;
	std::map<const char*, D3DXHANDLE> _techniques;

	D3DXHANDLE _currentTechnique;
	std::string _currentTechniqueName;

	LPDIRECT3DDEVICE9 _device;

	std::map<const char*, D3DXHANDLE> _constantHandles;

	std::shared_ptr<Renderer> _target;
};

NS_TINY_END