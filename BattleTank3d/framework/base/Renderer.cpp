#include "Renderer.h"
#include "..\SceneManager.h"
#include "Effect.h"

USING_NS_TINY;

bool Renderer::init()
{
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	_device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

	_fillMode = FillMode::SOLID;

	_shader = nullptr;

	return true;
}

void Renderer::draw()
{
	if (_target != nullptr)
	{
		if (_target->getWorldTransform() != nullptr)
		{
			auto world = _target->getWorldTransform();
			this->updateTransform(world);
		}
	}
}

void Renderer::updateTransform(const Transform::Ptr& transform)
{
	auto mat = transform->getMatrix();

	// set transform
	_device->SetTransform(D3DTS_WORLD, &mat);
}

void Renderer::setFillMode(const FillMode & mode)
{
	_fillMode = mode;
}

const FillMode& Renderer::getFillMode()
{
	return _fillMode;
}

void Renderer::setShader(Shader * shader)
{
	_shader = shader;
}

Shader * Renderer::getShader()
{
	return _shader;
}

void Renderer::setEffect(const std::shared_ptr<Effect>& effect)
{
	_effect = effect;
	_techniqueName = effect->getCurrentTechiqueName();
}

std::shared_ptr<Effect> Renderer::getEffect()
{
	return _effect;
}

