#include "Texture.h"
#include "..\SceneManager.h"

USING_NS_TINY;

Texture::Texture()
{
	_d3dTexture = nullptr;
}

Texture::~Texture()
{
	if(_d3dTexture != nullptr)
		_d3dTexture->Release();
}

bool Texture::initWithFile(const char * path)
{
	auto deivce = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	
	_filePath = path;

	auto result = D3DXCreateTextureFromFile(deivce, path, &_d3dTexture);
	if (FALSE(result))
		return false;

	return true;
}

LPDIRECT3DTEXTURE9 Texture::getD3DTexture()
{
	return _d3dTexture;
}

