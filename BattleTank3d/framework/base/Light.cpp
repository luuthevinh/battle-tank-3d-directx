#include "Light.h"
USING_NS_TINY;

//global variables
int Light::SOURCE_COUNTER = 0;


Light::Light(bool debugFlag)
{
	_reLight = true;
	_debugFlag = debugFlag;
	_lightId = SOURCE_COUNTER;
	SOURCE_COUNTER++;
}


Light::~Light()
{
	this->release();
}

const unsigned int Light::getLightId() {
	return this->_lightId;
}
const D3DLIGHTTYPE Light::getType() {
	return this->_source.Type;
}
const D3DXCOLOR Light::getAmbient() {
	return this->_source.Ambient;
}
const D3DXCOLOR Light::getSpecular() {
	return this->_source.Specular;
}
const D3DXCOLOR Light::getDiffuse() {
	return this->_source.Diffuse;
}
const D3DXVECTOR3 Light::getDirection() {
	return this->_source.Direction;
}
const float Light::getRange() {
	return this->_source.Range;
}

const float Light::getTheta(){
	return this->_source.Theta;
}
const float Light::getPhi() {
	return this->_source.Phi;
}
void Light::setTarget(const Object::Ptr& target) {
	Component::setTarget(target);
	if (this->_target)
	{
		this->_targetTransform = (Transform*)this->_target->getComponent(Component::TRANSFORM).get();
		if (!this->_targetTransform)
		{
			::MessageBox(0, "Light::setTarget(Object* target)-Light require Transform component", "Error", 0);
		}
	}
	
}
void Light::setDiffuse(const D3DXCOLOR& value) {
	this->_source.Diffuse = value;
}
void Light::setSpecular(const D3DXCOLOR& value) {
	this->_source.Specular = value;
}
void Light::setAmbient(const D3DXCOLOR& value) {
	this->_source.Ambient = value;
}
void Light::setRange(float value) {
	this->_source.Range = value;
}
// only for spot light
void  Light::setTheta(float value) {
	this->_source.Theta = value;
}
void Light::setPhi(float value) {
	this->_source.Phi = value;
}
void Light::setLightEnable(bool value) {
	this->_device->LightEnable(_lightId, value);
}
bool Light::init()
{
	return initDirectionalLight(D3DXVECTOR3(0, 0, 1), D3DXCOLOR(1.0F, 1.0F, 1.0F, 1.0F));;
}

bool Light::initDirectionalLight(D3DXVECTOR3 direction, D3DXCOLOR color) {
	::ZeroMemory(&_source, sizeof(_source));
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	if (!this->_device)
		return false;
	//for debug
	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();
	auto listener = EventKeyboardListener::create();
	listener->onKeyPressed = CALLBACK_01(Light::onKeyPressed, this);
	listener->onKeyReleased = CALLBACK_01(Light::onKeyReleased, this);
	dispatcher->addEventListener(listener, this->_target.get());
	//end of debug
	this->_source.Type = D3DLIGHTTYPE::D3DLIGHT_DIRECTIONAL;
	this->_source.Direction = direction;
	this->_source.Diffuse = color;
	
	// specular
	this->_source.Specular.r = 1.0f;
	this->_source.Specular.g = 1.0f;
	this->_source.Specular.b = 1.0f;
	this->_source.Specular.a = 1.0f;

	this->_source.Ambient.r = 0.5f;
	this->_source.Ambient.g = 0.5f;
	this->_source.Ambient.b = 0.5f;
	this->_source.Ambient.a = 1.0f;

	this->_device->LightEnable(_lightId, true);
	this->_device->SetLight(_lightId, &this->_source);
	this->_device->SetRenderState(D3DRS_SPECULARENABLE, true);
	this->_device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	this->_device->SetRenderState(D3DRS_LIGHTING, true);
	return true;
}
bool Light::initPointLight(float range, D3DXCOLOR color) {
	::ZeroMemory(&_source, sizeof(_source));
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	if (!this->_device)
		return false;
	//for debug 
	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();
	auto listener = EventKeyboardListener::create();
	listener->onKeyPressed = CALLBACK_01(Light::onKeyPressed, this);
	listener->onKeyReleased = CALLBACK_01(Light::onKeyReleased, this);
	dispatcher->addEventListener(listener, this->_target.get());
	//end of debug
	this->_source.Type = D3DLIGHTTYPE::D3DLIGHT_POINT;

	this->_source.Diffuse = color;
	this->_source.Range = range;

	// specular
	this->_source.Specular.r = 1.0f;
	this->_source.Specular.g = 1.0f;
	this->_source.Specular.b = 1.0f;
	this->_source.Specular.a = 1.0f;

	this->_source.Ambient.r = 0.5f;
	this->_source.Ambient.g = 0.5f;
	this->_source.Ambient.b = 0.5f;
	this->_source.Ambient.a = 1.0f;

	this->_device->LightEnable(_lightId, true);
	this->_device->SetLight(_lightId, &this->_source);
	this->_device->SetRenderState(D3DRS_SPECULARENABLE, false);
	this->_device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	this->_device->SetRenderState(D3DRS_LIGHTING, true);
	return true;
}
bool Light::initSpotLight(float range, D3DXVECTOR3 direction, D3DXCOLOR color,float theta=0.4f,float phi=0.9f){
	::ZeroMemory(&_source, sizeof(_source));
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	if (!this->_device)
		return false;

	//for debug 
	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();
	auto listener = EventKeyboardListener::create();
	listener->onKeyPressed = CALLBACK_01(Light::onKeyPressed, this);
	listener->onKeyReleased = CALLBACK_01(Light::onKeyReleased, this);
	dispatcher->addEventListener(listener, this->_target.get());
	//end of debug

	this->_source.Type = D3DLIGHTTYPE::D3DLIGHT_SPOT;
	this->_source.Direction = direction;
	this->_source.Specular = color;
	this->_source.Range = range;
	this->_source.Theta = theta;
	this->_source.Phi = phi;
	this->_source.Falloff = 1.0f;
	this->_source.Attenuation0 = 1.0f;


	this->_device->SetRenderState(D3DRS_SPECULARENABLE, TRUE);
	this->_device->LightEnable(_lightId, true);
	this->_device->SetLight(_lightId, &this->_source);
	this->_device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	return true;
}
void Light::update(float dt) 
{
	//their source should be same position with target
	if (_targetTransform //check if available
		&& _targetTransform->getPosition() != this->_source.Position) {
		_source.Position = _targetTransform->getPosition();
		this->_device->SetLight(_lightId, &this->_source);
	}

	if (_reLight && _device) //update
	{
		_device->SetLight(_lightId, &this->_source);
		_reLight = false;
	}
	
}
void  Light::onKeyPressed(EventKeyboard::KeyCode keycode) {
	if (_debugFlag) {
		_reLight = true;
		switch (keycode)
		{
		case EventKeyboard::KeyCode::KEY_F:
			_source.Direction.x += 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_G:
			_source.Direction.x -= 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_H:
			_source.Direction.z += 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_J:
			_source.Direction.z -= 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_K:
			_source.Direction.y += 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_L:
			_source.Direction.y -= 0.1f;
			break;
		case EventKeyboard::KeyCode::KEY_O:
			_source.Range -= 1.0f;
			break;
		case EventKeyboard::KeyCode::KEY_P:
			_source.Range += 1.0f;
			break;
		default:
			break;
		}
	}
	
}
void  Light::onKeyReleased(EventKeyboard::KeyCode keycode) {

}


void Light::release() {
	Light::SOURCE_COUNTER--;
	if (Light::SOURCE_COUNTER < 0)
		Light::SOURCE_COUNTER = 0;
}