#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Mesh;
class Material;

class MeshLoader
{
public:
	static ID3DXMesh* loadFromXFile(Mesh* target, const char* filePath, LPDIRECT3DDEVICE9 device);

	static ID3DXMesh* loadFromOBJFile(Mesh* target, const char* filePath, LPDIRECT3DDEVICE9 device);
	
	static D3DVERTEXELEMENT9 VERTEX_DECL[];

	MeshLoader() { }

private:
	
};

NS_TINY_END