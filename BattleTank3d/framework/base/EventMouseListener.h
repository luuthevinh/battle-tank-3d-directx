#pragma once

#include <functional>
#include "EventListener.h"
#include "EventMouse.h"

NS_TINY_BEGIN


class EventMouseListener : public EventListener
{
public:
	typedef std::shared_ptr<EventMouseListener> Ptr;

	static Ptr create()
	{
		auto listenter = std::shared_ptr<EventMouseListener>(new EventMouseListener());
		return listenter;
	}

	~EventMouseListener();

	// Inherited via EventListener
	virtual void onNotify(Event::Ptr event) override;

	std::function<void(EventMouse* e)> onMouseMoved;
	std::function<void(EventMouse* e)> onMousePressed;

private:
	EventMouseListener();
};

NS_TINY_END
