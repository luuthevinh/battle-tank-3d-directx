#pragma once

#include "Object.h"
#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

using namespace std;

class Mesh;
class MeshRenderer;

class Model : public Object
{
public:
	typedef shared_ptr<Model> Ptr;

	static Ptr createWithFile(const char* filePath)
	{
		auto model = shared_ptr<Model>(new Model());
		if (model->initWithFile(filePath))
			return model;

		return nullptr;
	}

	Model();
	~Model();

	virtual bool init() override;

	// read file with assimp lib
	virtual bool initWithFile(const char* filePath);

	// read .x file with directx
	virtual bool initWithXFile(const char* filePath);

	// use my custom .obj reader
 	virtual bool initWithOBJFile(const char* filePath);

	virtual void draw() override;

	virtual inline const shared_ptr<Mesh>& getMesh()
	{
		return _mesh;
	}

private:
	LPDIRECT3DDEVICE9 _device;

	shared_ptr<Mesh> _mesh;
	shared_ptr<MeshRenderer> _renderer;
};

NS_TINY_END