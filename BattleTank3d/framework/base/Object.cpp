#include "Object.h"
#include "Component.h"
#include "..\component\Transform.h"

USING_NS_TINY;

using namespace std;

Object::Object() : 
	_transform(nullptr),
	_name("GameObject"),
	_parent(nullptr),
	_dirty(true)
{
}

Object::~Object()
{
	_components.clear();
	_children.clear();
}

bool Object::init()
{
	_transform = Transform::create();
	_worldTransform = _transform;
	this->addComponent(Component::TRANSFORM, _transform);

	return true;
}

void Object::update(float dt)
{
	this->updateWorldTransform();

	for (auto it = _components.begin(); it != _components.end(); it++)
	{
		it->second->update(dt);
	}

	for (auto child : _children)
	{
		child.second->update(dt);
	}
}

void Object::updateWorldTransform()
{
	if (_parent != nullptr)
	{
		if (_dirty)
		{
			_dirty = false;
			auto parentWorld = _parent->getWorldTransform();
			_worldTransform = this->getTransform()->combine(parentWorld);
		}
	}
}

void Object::draw()
{
	for (auto child : _children)
	{
		child.second->draw();
	}
}

void Object::addComponent(const char * name, shared_ptr<Component> component)
{
	component->setTarget(shared_from_this());
	_components[name] = component;
}

shared_ptr<Component> Object::getComponent(const char * name)
{
	if (_components.find(name) != _components.end())
		return _components.at(name);

	return nullptr;
}
shared_ptr<Object> Object::getChild(const char* name) {
	if (_children.find(name) != _children.end())
		return _children.at(name);

	return nullptr;
}
std::map<std::string, Object::Ptr> Object::getChilds() {
	
	return _children;
}

shared_ptr<Transform> Object::getTransform()
{
	return _transform;
}

std::shared_ptr<Transform> Object::getWorldTransform()
{
	return _worldTransform;
}

void Object::addChild(Object::Ptr child)
{
	child->setParent(shared_from_this());
	_children[child->getName()] = child;
}

void Object::addChild(const char * name, Object::Ptr child)
{
	child->setParent(shared_from_this());
	_children[name] = child;
}
