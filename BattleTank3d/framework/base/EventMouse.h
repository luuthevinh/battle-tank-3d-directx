#pragma once

#include "Event.h"

NS_TINY_BEGIN

class EventMouse : public Event
{
public:
	typedef std::shared_ptr<EventMouse> Ptr;

	enum Button
	{
		BUTTON_NONE = 0,
		BUTTON_LEFT = 1,
		BUTTON_RIGHT = 2
	};

	enum EventMouseType
	{
		BUTTON_DOWN,
		BUTTON_UP,
		MOUSE_MOVING
	};

	static Ptr create(Button button, const EventMouseType& type)
	{
		auto e = new EventMouse(button);
		e->setEventMouseType(type);

		auto ePtr = std::shared_ptr<EventMouse>(e);
		return ePtr;
	}

	static Ptr create(float x, float y)
	{
		auto e = new EventMouse(x, y);
		e->setEventMouseType(EventMouseType::MOUSE_MOVING);

		auto ePtr = std::shared_ptr<EventMouse>(e);
		return ePtr;
	}

	EventMouse(const Button& button);
	EventMouse(float x, float y);

	~EventMouse();

	const Button& getButton() const;
	void setButton(const Button& button);

	void setPressed(bool pressed);
	bool isPressed();

	void setDeltaChange(float x, float y);
	const Vec2& getDeltaChange() const;

	void setPosition(const Vec2& position);
	const Vec2& getPosition() const;

	inline void setEventMouseType(const EventMouseType& type) { _eventMouseType = type; }
	inline const EventMouseType& getEventMouseType() { return _eventMouseType; }

private:
	Vec2 _deltaChange;
	Vec2 _position;

	Button _button;
	bool _isPressed;

	EventMouseType _eventMouseType;
};

NS_TINY_END