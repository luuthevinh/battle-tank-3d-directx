#include "EventKeyboardListener.h"
#include "EventKeyboard.h"

USING_NS_TINY;

EventKeyboardListener::EventKeyboardListener()
{
	
}

EventKeyboardListener::~EventKeyboardListener()
{
}

void EventKeyboardListener::onNotify(Event::Ptr event)
{
	if (event->getType() != Event::Type::KEYBOARD)
		return;

	auto keyboardEvent = std::dynamic_pointer_cast<EventKeyboard>(event);

	if (keyboardEvent->isPressed())
	{
		if (this->onKeyPressed != nullptr)
		{
			this->onKeyPressed(keyboardEvent->getKeycode());
		}
	}
	else
	{
		if (this->onKeyReleased != nullptr)
		{
			this->onKeyReleased(keyboardEvent->getKeycode());
		}
	}
}
