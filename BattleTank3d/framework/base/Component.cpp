#include "Component.h"
#include "Object.h"

NS_TINY_BEGIN

const char* Component::LIGHT = "light";
const char* Component::TRANSFORM = "transform";
const char* Component::RENDERDER = "renderer";
const char* Component::MESH = "mesh";
const char* Component::BOUNDING_BOX = "bounding_box";
const char* Component::COLLIDER = "collider";
const char* Component::SPHERE_COLLIDER = "sphere_collider";

Component::Component()
{
	_target = nullptr;
}

void Component::setTarget(const std::shared_ptr<Object>& target)
{
	_target = target;
}

const std::shared_ptr<Object>& Component::getTarget()
{
	return _target;
}


NS_TINY_END
