#include "Shader.h"
#include "..\component\Transform.h"
#include "..\SceneManager.h"
#include "..\Camera.h"

USING_NS_TINY;

const char* Shader::CONSTANT_WORLD_MATRIX = "World";
const char* Shader::CONSTANT_VIEW_MATRIX = "View";
const char* Shader::CONSTANT_PROJECTION_MATRIX = "Projection";
const char* Shader::CONSTANT_TIME = "Time";
const char* Shader::CONSTANT_LIGHT_AMBIENT = "LightAmbient";
const char* Shader::CONSTANT_LIGHT_POSITION = "LightPosition";
const char* Shader::CONSTANT_LIGHT_COLOR = "LightColor";
const char* Shader::CONSTANT_VIEW_POSITION = "ViewPositon";

Shader::Shader(const char * vertexPath, const char * pixelPath)
{
	_vsPath = vertexPath;
	_psPath = pixelPath;

	_vertexShader = nullptr;
	_pixelShader = nullptr;
}

Shader::Shader(const char * effect)
{
}

Shader::~Shader()
{
}

bool Shader::compile()
{
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	
	if (strcmp(_vsPath, "") != 0)
	{
		if (!this->compileVertexShader())
			return false;
	}

	if (strcmp(_psPath, "") != 0)
	{
		if (!this->compilePixelShader())
			return false;
	}

	return true;
}

void Shader::begin()
{
	_device->SetVertexShader(_vertexShader);
	_device->SetPixelShader(_pixelShader);

	if (onBeginUse != nullptr)
	{
		this->onBeginUse(this);
	}

	D3DXMATRIX project;
	D3DXMATRIX view;
	D3DXMATRIX world;

	_device->GetTransform(D3DTS_VIEW, &view);
	_device->GetTransform(D3DTS_PROJECTION, &project);
	_device->GetTransform(D3DTS_WORLD, &world);

	this->setMatrixInVertexShader(world, Shader::CONSTANT_WORLD_MATRIX);
	this->setMatrixInVertexShader(view, Shader::CONSTANT_VIEW_MATRIX);
	this->setMatrixInVertexShader(project, Shader::CONSTANT_PROJECTION_MATRIX);

	auto device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	auto table = this->getPixelConstantTable();

	this->setValueInPixelShader(Vec3(10.0f, 10.0f, -10.0f), sizeof(Vec3), Shader::CONSTANT_LIGHT_POSITION);
	this->setValueInPixelShader(Vec4(1.0f, 1.0f, 1.0f, 1.0f), sizeof(Vec4), Shader::CONSTANT_LIGHT_COLOR);
	this->setValueInPixelShader(Vec4(0.2f, 0.2f, 0.2f, 1.0f), sizeof(Vec4), Shader::CONSTANT_LIGHT_AMBIENT);

	auto camera = SceneManager::getInstance()->getMainCamera();
	auto camPos = camera->getTransform()->getPosition();
	this->setValueInPixelShader(camPos, sizeof(Vec3), Shader::CONSTANT_VIEW_POSITION);

}

void Shader::end()
{
	_device->SetVertexShader(NULL);
	_device->SetPixelShader(NULL);
}

void Shader::setMatrixInVertexShader(const D3DXMATRIX & matrix, const char * constant)
{
	if (_constantTableOfVertexShader == nullptr)
		return;

	_constantTableOfVertexShader->SetMatrix(_device, this->getVertexConstant(constant), &matrix);
}

void Shader::setTextureInPixelShader(IDirect3DTexture9* texture, const char * constant)
{
	if (_constantTableOfPixelShader == nullptr)
		return;

	auto handle = _constantTableOfPixelShader->GetConstantByName(0, constant);
	
	D3DXCONSTANT_DESC constDesc;
	UINT count;
	auto desc = _constantTableOfPixelShader->GetConstantDesc(handle, &constDesc, &count);

	_device->SetTexture(constDesc.RegisterIndex, texture);
	_device->SetSamplerState(constDesc.RegisterIndex, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(constDesc.RegisterIndex, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	_device->SetSamplerState(constDesc.RegisterIndex, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
}

void Shader::setValueInPixelShader(void* value, unsigned int size, const char * constant)
{
	if (_constantTableOfPixelShader == nullptr)
		return;

	_constantTableOfPixelShader->SetValue(_device, this->getPixelConstant(constant), value, size);

}

void Shader::release()
{
	_vertexShader->Release();
	_pixelShader->Release();

	delete this;
}

ID3DXConstantTable * Shader::getVertexConstantTable()
{
	return _constantTableOfVertexShader;
}

ID3DXConstantTable * Shader::getPixelConstantTable()
{
	return _constantTableOfPixelShader;
}

D3DXHANDLE Shader::getPixelConstant(const char * name)
{
	if (_constantTableOfPixelShader == nullptr)
		return nullptr;

	if (_pixelConstantHandles.find(name) != _pixelConstantHandles.end())
		return _pixelConstantHandles.at(name);

	auto handle = _constantTableOfPixelShader->GetConstantByName(0, name);
	_pixelConstantHandles[name] = handle;
	
	return handle;
}

D3DXHANDLE Shader::getVertexConstant(const char * name)
{
	if (_constantTableOfVertexShader == nullptr)
		return nullptr;

	if (_vertexConstantHandles.find(name) != _vertexConstantHandles.end())
		return _vertexConstantHandles.at(name);

	auto handle = _constantTableOfVertexShader->GetConstantByName(0, name);
	_vertexConstantHandles[name] = handle;

	return handle;
}

bool Shader::compilePixelShader()
{
	ID3DXBuffer* shader = 0;
	ID3DXBuffer* errorBuffer = 0;

	auto result = D3DXCompileShaderFromFile(
		_psPath,
		0,
		0,
		"Main", 
		"ps_2_0",
		D3DXSHADER_DEBUG,
		&shader,
		&errorBuffer,
		&_constantTableOfPixelShader);

	// output any error messages
	if (errorBuffer)
	{
		OutputDebugStringA((char*)errorBuffer->GetBufferPointer());
		errorBuffer->Release();
		return false;
	}

	if (FAILED(result))
		return false;

	//
	// Create Pixel Shader
	//
	result = _device->CreatePixelShader((DWORD*)shader->GetBufferPointer(), &_pixelShader);

	if (FAILED(result))
		return false;

	shader->Release();

	_constantTableOfPixelShader->SetDefaults(_device);

	return true;
}

bool Shader::compileVertexShader()
{
	// shader
	ID3DXBuffer* shaderBuffer = 0;
	ID3DXBuffer* errorBuffer = 0;

	auto result = D3DXCompileShaderFromFile(
		_vsPath,
		0,
		0,
		"Main",
		"vs_2_0",
		D3DXSHADER_DEBUG,
		&shaderBuffer,
		&errorBuffer,
		&_constantTableOfVertexShader);

	if (errorBuffer)
	{
		OutputDebugStringA((char*)errorBuffer->GetBufferPointer());
		errorBuffer->Release();
		return false;
	}

	if (FAILED(result))
		return false;

	result = _device->CreateVertexShader((DWORD*)shaderBuffer->GetBufferPointer(), &_vertexShader);
	if (FAILED(result))
		return false;

	// release buffer
	shaderBuffer->Release();

	_constantTableOfVertexShader->SetDefaults(_device);

	return true;
}
