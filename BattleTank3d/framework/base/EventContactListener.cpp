#include "EventContactListener.h"

USING_NS_TINY;

EventContactListener::EventContactListener()
{
}

void EventContactListener::onNotify(Event::Ptr event)
{
	auto contactEvent = std::dynamic_pointer_cast<EventContact>(event);

	if (contactEvent == nullptr)
		return;

	auto type = contactEvent->getContactType();
	
	switch (type)
	{
	case EventContact::CONTACT_BEGIN:
		if(onContactBegin != nullptr)
			this->onContactBegin(contactEvent);
		break;
	case EventContact::CONTACT_END:
		if (onContactEnd != nullptr)
			this->onContactEnd(contactEvent);
		break;
	default:
		break;
	}
}

EventContactListener::~EventContactListener()
{
}
