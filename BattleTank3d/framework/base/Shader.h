#pragma once

#include <map>
#include <functional>
#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Shader
{
public:
	static const char* CONSTANT_WORLD_MATRIX;
	static const char* CONSTANT_VIEW_MATRIX;
	static const char* CONSTANT_PROJECTION_MATRIX;
	static const char* CONSTANT_TIME;
	static const char* CONSTANT_LIGHT_AMBIENT;
	static const char* CONSTANT_LIGHT_POSITION;
	static const char* CONSTANT_LIGHT_COLOR;
	static const char* CONSTANT_VIEW_POSITION;

	Shader(const char* vertexPath, const char* pixelPath);
	Shader(const char* effect);
	~Shader();

	virtual bool compile();

	virtual void begin();
	virtual void end();

	virtual void setMatrixInVertexShader(const D3DXMATRIX& matrix, const char* constant);

	virtual void setTextureInPixelShader(IDirect3DTexture9* texture, const char * constant);
	virtual void setValueInPixelShader(void* value, unsigned int size, const char* constant);

	virtual void release();

	std::function<void(Shader*)> onBeginUse;

	ID3DXConstantTable* getVertexConstantTable();
	ID3DXConstantTable* getPixelConstantTable();

	D3DXHANDLE getPixelConstant(const char* name);
	D3DXHANDLE getVertexConstant(const char* name);

protected:
	bool compilePixelShader();
	bool compileVertexShader();

	const char* _vsPath;
	const char* _psPath;

	LPDIRECT3DDEVICE9 _device;

	IDirect3DVertexShader9* _vertexShader;
	IDirect3DPixelShader9* _pixelShader;

	ID3DXConstantTable* _constantTableOfVertexShader;
	ID3DXConstantTable* _constantTableOfPixelShader;

	std::map<const char*, D3DXHANDLE> _vertexConstantHandles;
	std::map<const char*, D3DXHANDLE> _pixelConstantHandles;
};

NS_TINY_END