#include "EventDispatcher.h"
#include "Event.h"

USING_NS_TINY;

EventDispatcher::EventDispatcher()
{
	_numberListeners = 0;
}

EventDispatcher::~EventDispatcher()
{
	for (auto it = _listeners.begin(); it != _listeners.end(); it++)
	{
		it->second.clear();
	}

	_listeners.clear();
}

void EventDispatcher::addEventListener(EventListener::Ptr listener, Object* target)
{
	if (_listeners.find(target) != _listeners.end())
	{
		auto listeners = _listeners.at(target);
		
		auto result = std::find(listeners.begin(), listeners.end(), listener);
		if (result != listeners.end())
		{
			listeners.erase(result);
		}
	}

	_listeners[target].push_back(listener);
}

void EventDispatcher::removeEventListener(EventListener::Ptr listener, Object* target)
{
	if (_listeners.find(target) != _listeners.end())
	{
		auto removeLis = std::find(_listeners.at(target).begin(), _listeners.at(target).end(), listener);
		if (removeLis != _listeners.at(target).end())
		{
			_listeners.at(target).erase(removeLis);
		}

		if (_listeners.at(target).size() == 0)
		{
			_listeners.erase(target);
		}
	}
}

void EventDispatcher::removeAllEventListener(Object * target)
{
	if (_listeners.find(target) != _listeners.end())
	{
		_listeners.at(target).clear();
		_listeners.erase(target);
	}
}

void EventDispatcher::notify(Event::Ptr event)
{
	for (auto it = _listeners.begin(); it != _listeners.end(); it++)
	{
		for (auto itListener = it->second.begin(); itListener != it->second.end(); itListener++)
		{
			(*itListener)->onNotify(event);
		}
	}
}