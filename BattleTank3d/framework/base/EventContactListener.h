#pragma once

#include <functional>
#include "..\TinyDefinitions.h"
#include "EventListener.h"
#include "EventContact.h"

NS_TINY_BEGIN

class EventContactListener : public EventListener
{
public:
	DEF_SHARED_PTR(EventContactListener);

	~EventContactListener();

	static Ptr create()
	{
		auto listenter = Ptr(new EventContactListener());
		return listenter;
	}

	// Inherited via EventListener
	virtual void onNotify(Event::Ptr event) override;


	std::function<void(const EventContact::Ptr&)> onContactBegin;
	std::function<void(const EventContact::Ptr&)> onContactEnd;

private:
	EventContactListener();

	
};

NS_TINY_END
