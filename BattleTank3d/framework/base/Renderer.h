#pragma once

#include <string>
#include "..\TinyDefinitions.h"
#include "Component.h"
#include "..\component\Transform.h"

NS_TINY_BEGIN

class Shader;
class Effect;

class Renderer : public Component
{
public:
	virtual ~Renderer() { }

	// Inherited via Component
	virtual bool init() override;

	virtual void draw();
	virtual void updateTransform(const Transform::Ptr& transform);

	void setFillMode(const FillMode& mode);
	const FillMode& getFillMode();

	void setShader(Shader* shader);
	Shader* getShader();

	void setEffect(const std::shared_ptr<Effect>& effect);
	std::shared_ptr<Effect> getEffect();

	inline void setTechnique(const char* name)
	{
		_techniqueName = name;
	}

protected:
	Renderer() { }

	LPDIRECT3DDEVICE9 _device;

	FillMode _fillMode;
	Shader* _shader;

	std::shared_ptr<Effect> _effect;
	std::string _techniqueName;
};

NS_TINY_END