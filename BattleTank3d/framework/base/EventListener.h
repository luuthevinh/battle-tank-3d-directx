#pragma once

#include "..\TinyDefinitions.h"
#include "Event.h"

NS_TINY_BEGIN

class EventListener
{
public:
	typedef std::shared_ptr<EventListener> Ptr;

	virtual ~EventListener() { }
	virtual void onNotify(Event::Ptr event) = 0;

protected:
	EventListener() { }
	Event::Type _type;
};

NS_TINY_END