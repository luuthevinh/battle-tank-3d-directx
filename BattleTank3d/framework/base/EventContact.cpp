#include "EventContact.h"

USING_NS_TINY;

EventContact::EventContact()
{
	_type = Type::CONTACT;
}

EventContact::EventContact(const ColliderPtr& colliderA, const ColliderPtr& colliderB)
{
	_colliderA = colliderA;
	_colliderB = colliderB;

	_type = Type::CONTACT;
}

EventContact::~EventContact()
{
}
