#pragma once
#include "../component/Transform.h"
#include "../base//Renderer.h"
#include "../SceneManager.h"
#include "../base//EventKeyboard.h"
#include "..//base//EventKeyboardListener.h"
#include "..//base//EventDispatcher.h"
NS_TINY_BEGIN
class Light: public Component
{
public:


	static int SOURCE_COUNTER ;

	typedef std::shared_ptr<Light> Ptr;

	static Ptr createDirectionalLight(D3DXVECTOR3 direction, D3DXCOLOR color,bool debugFlag = false)
	{
		auto light = std::shared_ptr<Light>(new Light(debugFlag));
		if (light->initDirectionalLight(direction,color))
			return light;
		return nullptr;
	}
	static Ptr createPointLight(float range, D3DXCOLOR color, bool debugFlag = false)
	{
		auto light = std::shared_ptr<Light>(new Light(debugFlag));
		if (light->initPointLight(range, color))
			return light;
		return nullptr;
	}
	static Ptr createSpotLight(float range, D3DXVECTOR3 direction, D3DXCOLOR color, float theta, float phi, bool debugFlag = false)
	{
		auto light = std::shared_ptr<Light>(new Light(debugFlag));
		if (light->initSpotLight(range, direction, color, theta,phi))
			return light;
		return nullptr;
	}

	
	~Light();
	// Inherited via Component
	virtual bool init() override;
	// Init a direction light
	virtual bool initDirectionalLight(D3DXVECTOR3 direction, D3DXCOLOR color);
	// Init a point light
	virtual bool initPointLight(float range,  D3DXCOLOR color);
	// Init a spot light
	virtual bool initSpotLight(float range, D3DXVECTOR3 direction, D3DXCOLOR color,float theta,float phi);

	//overide methods
	virtual void update(float dt);
	virtual void setTarget(const Object::Ptr& target) override;
	virtual void release();
	
	//getters
	const D3DLIGHTTYPE getType();
	const D3DXCOLOR getDiffuse();
	const D3DXCOLOR getSpecular();
	const D3DXCOLOR getAmbient();
	const D3DXVECTOR3 getDirection();
	const float getRange();
	const float getTheta();
	const float getPhi();
	const unsigned int getLightId();
	//setters
	void setLightEnable(bool value);
	void setDiffuse(const D3DXCOLOR& value);
	void setSpecular(const D3DXCOLOR& value);
	void setAmbient(const D3DXCOLOR& value);
	void setRange(float value);
	
	void onKeyPressed(EventKeyboard::KeyCode keycode);
	void onKeyReleased(EventKeyboard::KeyCode keycode);

	// only for spot light
	void setTheta(float value);
	void setPhi(float value);
protected:
	Light(bool debugFlag = false);
private:
	bool _reLight;
	bool _debugFlag;
	// source light Identify
	unsigned int _lightId;
	// source information
	D3DLIGHT9 _source;
	// transform component of target are required
	Transform* _targetTransform;
	// direct 3d device pointer
	LPDIRECT3DDEVICE9 _device;

};

NS_TINY_END