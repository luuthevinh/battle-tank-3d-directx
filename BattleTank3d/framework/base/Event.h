#pragma once

#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

class Event
{
public:
	typedef std::shared_ptr<Event> Ptr;

	virtual ~Event() { }

	enum Type
	{
		KEYBOARD,
		MOUSE,
		CONTACT
	};

	virtual void update(float dt) { };

	virtual inline const Type& getType() const 
	{
		return _type;
	}

protected:
	Event() { }

	Type _type;
};

NS_TINY_END