#pragma once

#include <map>
#include <vector>
#include "..\TinyDefinitions.h"
#include "EventListener.h"

NS_TINY_BEGIN

class Event;
class Object;

class EventDispatcher
{
public:
	typedef std::shared_ptr<EventDispatcher> Ptr;

	static Ptr create()
	{
		auto dispatcher = std::shared_ptr<EventDispatcher>(new EventDispatcher());
		return dispatcher;
	}

	EventDispatcher();
	~EventDispatcher();

	void addEventListener(EventListener::Ptr listener, Object* target);
	void removeEventListener(EventListener::Ptr listener, Object* target);
	void removeAllEventListener(Object* target);

	void notify(Event::Ptr event);


private:

	std::map<Object*, std::vector<EventListener::Ptr>> _listeners;

	int _numberListeners;
};

NS_TINY_END