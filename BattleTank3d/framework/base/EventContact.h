#pragma once

#include "..\TinyDefinitions.h"
#include "Event.h"

NS_TINY_BEGIN

class Collider;

class EventContact : public Event
{
public:
	enum EventContactType
	{
		CONTACT_BEGIN,
		CONTACT_END
	};

	DEF_SHARED_PTR(EventContact);

	typedef std::shared_ptr<Collider> ColliderPtr;

	~EventContact();

	static Ptr createBeginEvent(const ColliderPtr& colliderA, const ColliderPtr& colliderB)
	{
		auto contactEvent = new EventContact(colliderA, colliderB);
		contactEvent->setContactType(EventContactType::CONTACT_BEGIN);
		return Ptr(contactEvent);
	}

	static Ptr createEndEvent(const ColliderPtr& colliderA, const ColliderPtr& colliderB)
	{
		auto contactEvent = new EventContact(colliderA, colliderB);
		contactEvent->setContactType(EventContactType::CONTACT_END);
		return Ptr(contactEvent);
	}

	inline void setContactType(const EventContactType& type)
	{
		_contactType = type;
	}

	inline const EventContactType& getContactType()
	{
		return _contactType;
	}

private:
	EventContact();
	EventContact(const ColliderPtr& colliderA, const ColliderPtr& colliderB);

	std::shared_ptr<Collider> _colliderA;
	std::shared_ptr<Collider> _colliderB;

	EventContactType _contactType;
};

NS_TINY_END