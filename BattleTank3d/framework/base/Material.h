#pragma once

#include <map>
#include <string>
#include "..\TinyDefinitions.h"
#include "..\ResourcesManager.h"

NS_TINY_BEGIN

class Texture;

class Material
{
public:
	typedef std::shared_ptr<Material> Ptr;

	static const Material WHITE_MATERIAL;
	static const Material BLACK_MATERIAL;

	static const char* MATERIAL_DIFFUSE;
	static const char* MATERIAL_SPECULAR;

	Material();
	Material(std::string name, const Vec3& amb, const Vec3& dif, const Vec3& spe);
	Material(std::string name, const Vec3& amb, const Vec3& dif, const Vec3& spe, const Vec3&ems);

	static Ptr createWithMaterial(const D3DMATERIAL9& material)
	{
		auto mat = std::shared_ptr<Material>(new Material(material));
		return mat;
	}

	static Ptr create(std::string name, const Vec3& color)
	{
		auto mat = std::shared_ptr<Material>(new Material(name, color, color, color));
		return mat;
	}
	
	static Ptr get(const char* name)
	{
		auto mat = ResourcesManager::getInstance()->getMaterial(name);
		return mat;
	}

	std::string name;

	Vec3 ambient;
	Vec3 diffuse;
	Vec3 specular;
	Vec3 emissive;

	int shininess;
	float alpha;

	bool hasSpecular;

	std::map<std::string, std::string> texturePaths;
	std::map<std::string, std::shared_ptr<Texture>> textures;

	const D3DMATERIAL9& getD3DMaterial(bool update = false);

private:
	Material(const D3DMATERIAL9& material);
	
	D3DMATERIAL9 _d3dMaterial;

	void buildD3DMaterial();
};

NS_TINY_END