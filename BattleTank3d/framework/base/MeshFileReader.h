#pragma once

#include <vector>
#include <map>

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include "..\TinyDefinitions.h"
#include "FileReader.h"
#include "..\component\Mesh.h"

NS_TINY_BEGIN

class Material;

class MeshFileReader : public FileReader
{
public:
	MeshFileReader(std::string filePath);
	~MeshFileReader();

	bool readData();

	const std::vector<std::shared_ptr<Material>>& getMaterials();
	const std::vector<Mesh::Vertex>& getVertices();
	const std::vector<DWORD>& getIndices();
	const std::vector<DWORD>& getAttributes();

	unsigned int getNumMesh();

protected:
	std::vector<std::shared_ptr<Material>> _materials;
	std::vector<Mesh::Vertex> _vertices;
	std::vector<DWORD> _indices;
	std::vector<DWORD> _attributes;

	unsigned int _currentMeshIndex;

	void processNode(aiNode* node, const aiScene* scene);
	void processMesh(aiMesh* mesh, const aiScene* scene);
	std::shared_ptr<Material> processMaterial(aiMaterial* material, aiMesh* mesh);

	std::map<std::string, std::shared_ptr<Material>> _materialsCache;
};

NS_TINY_END