#include "EventMouseListener.h"
#include "EventMouse.h"

USING_NS_TINY;

EventMouseListener::EventMouseListener()
{
}

EventMouseListener::~EventMouseListener()
{
}

void EventMouseListener::onNotify(Event::Ptr event)
{
	if (event->getType() != Event::Type::MOUSE)
	{
		return;
	}

	auto mouseEvent = std::dynamic_pointer_cast<EventMouse>(event);
	if (mouseEvent == nullptr)
	{
		return;
	}

	if (mouseEvent->getEventMouseType() == EventMouse::EventMouseType::MOUSE_MOVING)
	{
		if (this->onMouseMoved != nullptr)
		{
			this->onMouseMoved(mouseEvent.get());
		}
		
	} 
	else
	{
		if (this->onMousePressed != nullptr)
		{
			this->onMousePressed(mouseEvent.get());
		}
	}
}
