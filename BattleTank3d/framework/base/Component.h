#pragma once

#include "../TinyDefinitions.h"

NS_TINY_BEGIN

class Object;

class Component
{
public:
	std::shared_ptr<Component> Ptr;

	virtual ~Component() { }

	static const char* LIGHT;
	static const char* TRANSFORM;
	static const char* RENDERDER;
	static const char* MESH;
	static const char* BOUNDING_BOX;
	static const char* COLLIDER;
	static const char* SPHERE_COLLIDER;

	virtual bool init() = 0;
	virtual void update(float dt) { }

	virtual void setTarget(const std::shared_ptr<Object>& target);
	virtual const std::shared_ptr<Object>& getTarget();

protected:	
	Component();

	std::shared_ptr<Object> _target;
};

NS_TINY_END