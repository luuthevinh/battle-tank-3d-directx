#include "Material.h"

USING_NS_TINY;

const Material Material::WHITE_MATERIAL = Material("white", Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f));
const Material Material::BLACK_MATERIAL = Material("black", Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f));

const char* Material::MATERIAL_DIFFUSE = "diffuse";
const char* Material::MATERIAL_SPECULAR = "specular";

Material::Material()
{
	this->name = "default";
	this->ambient = Vec3(0.2f, 0.2f, 0.2f);
	this->diffuse = Vec3(0.8f, 0.8f, 0.8f);
	this->specular = Vec3(1.0f, 1.0f, 1.0f);
	this->emissive = Vec3(0.0f, 0.0f, 0.0f);
	this->shininess = 0;
	this->alpha = 1.0f;
	this->hasSpecular = false;

	this->buildD3DMaterial();
}

Material::Material(std::string name, const Vec3& amb, const Vec3& dif, const Vec3& spe)
{
	this->name = name;
	this->ambient = amb;
	this->diffuse = dif;
	this->specular = spe;
	this->emissive = Vec3(0.0f, 0.0f, 0.0f);
	this->shininess = 0;
	this->alpha = 1.0f;
	this->hasSpecular = false;

	this->buildD3DMaterial();
}

Material::Material(std::string name, const Vec3& amb, const Vec3& dif, const Vec3& spe,const Vec3&ems)
{
	this->name = name;
	this->ambient = amb;
	this->diffuse = dif;
	this->specular = spe;
	this->emissive = ems;
	this->shininess = 0;
	this->alpha = 1.0f;
	this->hasSpecular = false;

	this->buildD3DMaterial();
}

const D3DMATERIAL9 & Material::getD3DMaterial(bool update /*= false*/)
{
	if (update)
	{
		this->buildD3DMaterial();
	}

	return _d3dMaterial;
}

Material::Material(const D3DMATERIAL9 & material)
{
	_d3dMaterial = material;

	this->name = "default";
	this->ambient = Vec3(material.Ambient.r, material.Ambient.g, material.Ambient.b);
	this->diffuse = Vec3(material.Diffuse.r, material.Diffuse.g, material.Diffuse.b);
	this->specular = Vec3(material.Specular.r, material.Specular.g, material.Specular.b);
	this->emissive = Vec3(material.Emissive.r, material.Emissive.g, material.Emissive.b);
	this->shininess = material.Power;
	this->alpha = material.Diffuse.a;
	this->hasSpecular = false;
}

void Material::buildD3DMaterial()
{
	_d3dMaterial.Ambient = Color4F(this->ambient.x, this->ambient.y, this->ambient.z, this->alpha);
	_d3dMaterial.Diffuse = Color4F(this->diffuse.x, this->diffuse.y, this->diffuse.z, this->alpha);
	_d3dMaterial.Specular = Color4F(this->specular.x, this->specular.y, this->specular.z, this->alpha);
	_d3dMaterial.Emissive = Color4F(this->emissive.x, this->emissive.y, this->emissive.z, this->alpha);
	_d3dMaterial.Power = this->shininess;
}
