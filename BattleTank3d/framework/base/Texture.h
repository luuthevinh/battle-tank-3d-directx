#pragma once

#include <string>
#include "..\TinyDefinitions.h"
#include "..\ResourcesManager.h"

NS_TINY_BEGIN

class Texture
{
public:
	typedef std::shared_ptr<Texture> Ptr;

	static Ptr create(const char* path)
	{
		auto texture = std::shared_ptr<Texture>(new Texture());
		if (texture->initWithFile(path))
			return texture;

		return nullptr;
	}

	static Ptr get(const char* path)
	{
		return ResourcesManager::getInstance()->getTexture(path);
	}

	~Texture();

	bool initWithFile(const char* path);

	LPDIRECT3DTEXTURE9 getD3DTexture();

private:
	Texture();

	LPDIRECT3DTEXTURE9 _d3dTexture;
	std::string _filePath;
};

NS_TINY_END