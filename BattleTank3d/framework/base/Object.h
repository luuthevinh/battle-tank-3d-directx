#pragma once

#include <map>
#include <string>
#include <vector>
#include "..\TinyDefinitions.h"
#include "..\component\Transform.h"

NS_TINY_BEGIN

class Component;
class Transform;

class Object : public std::enable_shared_from_this<Object>
{
protected:
	Object();

public:
	typedef std::shared_ptr<Object> Ptr;

	virtual ~Object();

	virtual bool init();
	virtual void update(float dt);
	virtual void updateWorldTransform();
	virtual void draw();

	virtual void addComponent(const char* name, std::shared_ptr<Component> component);
	virtual std::shared_ptr<Component> getComponent(const char* name);
	virtual std::shared_ptr<Object> getChild(const char* name);
	virtual std::map<std::string, Object::Ptr> getChilds();

	template <typename T>
	inline std::shared_ptr<T> getComponent(const char* name)
	{
		return std::dynamic_pointer_cast<T>(this->getComponent(name));
	}

	template <typename T>
	inline std::shared_ptr<T> getChild(const char* name)
	{
		return std::dynamic_pointer_cast<T>(this->getChild(name));
	}

	std::shared_ptr<Transform> getTransform();
	std::shared_ptr<Transform> getWorldTransform();

	virtual void addChild(Object::Ptr child);
	virtual void addChild(const char* name, Object::Ptr child);

	virtual inline void setName(const char* name)
	{
		_name = name;
	}

	virtual const char* getName()
	{
		return _name.c_str();
	}

	virtual inline Object::Ptr getParent()
	{
		return _parent;
	}

	virtual inline void setParent(const Object::Ptr& parent)
	{
		_parent = parent;
	}

	virtual inline void setDirty(bool value)
	{
		_dirty = value;
		for (auto child : _children)
		{
			child.second->setDirty(value);
		}
	}

	virtual inline bool isDirty()
	{
		return _dirty;
	}

protected:
	std::string _name;

	std::map<std::string, std::shared_ptr<Component>> _components;

	Transform::Ptr _transform;
	Transform::Ptr _worldTransform;

	std::map<std::string, Object::Ptr> _children;
	Object::Ptr _parent;

	bool _dirty;
};

NS_TINY_END