#pragma once

#include <deque>
#include "..\TinyDefinitions.h"
#include "..\ui\Text.h"

NS_TINY_BEGIN

class Sphere;
class MeshRenderer;
class Mesh;

class Debug
{
public:
	Debug();
	~Debug();

	static Debug* getInstance()
	{
		if (_instance == nullptr)
		{
			_instance = new Debug();
			_instance->init();
		}

		return _instance;
	}

	bool init();
	void update(float dt);
	void draw();

	void logStringOnScreen(const char* message);
	void drawSphere(const std::shared_ptr<Sphere>& sphere);

	void addDebugMesh(const char* name, const std::shared_ptr<Mesh>& mesh);
	void removeDebugMeshByName(const char* name);

private:
	static Debug* _instance;

	Text::Ptr _logText;
	std::deque<std::string> _logStrings;
	float _shownTime;
	float _logCounter;

	void updateLogStrings();

	// debug mesh renderer
	std::shared_ptr<MeshRenderer> _debugMeshRenderer;
};

NS_TINY_END