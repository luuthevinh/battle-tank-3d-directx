#include "Debug.h"
#include "..\renderer\MeshRenderer.h"
#include "..\..\Settings.h"
#include <string>

USING_NS_TINY;

Debug* Debug::_instance = nullptr;

Debug::Debug()
{
}

Debug::~Debug()
{
}

bool Debug::init()
{
	_logText = Text::create("Arial", "", Vec2(10.0f, Settings::getInstance()->getWindowSize().height - 10.0f));
	_logText->setColor(Color4F(0.0f, 1.0f, 0.0f, 1.0f));

	_shownTime = 5.0f;

	// debug mesh renderer
	_debugMeshRenderer = MeshRenderer::create();

	return false;
}

void Debug::update(float dt)
{
	if (_logStrings.size() > 0)
	{
		_logCounter -= dt;

		if (_logCounter <= 0)
		{
			_logCounter = _shownTime;
			_logStrings.pop_front();

			this->updateLogStrings();
		}
	}

	_logText->update(dt);
	_debugMeshRenderer->update(dt);
}

void Debug::draw()
{
	_logText->draw();
	_debugMeshRenderer->draw();
}

void Debug::logStringOnScreen(const char * message)
{
	_logStrings.push_back(message);
	
	this->updateLogStrings();
}

void Debug::drawSphere(const std::shared_ptr<Sphere>& sphere)
{
}

void Debug::addDebugMesh(const char* name, const std::shared_ptr<Mesh>& mesh)
{
	_debugMeshRenderer->setMesh(name, mesh);
}

void Debug::removeDebugMeshByName(const char* name)
{
	_debugMeshRenderer->removeMeshByName(name);
}

void Debug::updateLogStrings()
{
	std::string str = "";

	for (size_t i = 0; i < _logStrings.size(); i++)
	{
		str += _logStrings[i] + '\n';
	}
	_logText->setText(str);
}
