#include "SphereCollider.h"
#include "..\SceneManager.h"
#include "..\base\Object.h"
#include "..\component\Mesh.h"
#include "..\base\Material.h"

USING_NS_TINY;

SphereCollider::SphereCollider()
{
}

SphereCollider::~SphereCollider()
{

}

bool SphereCollider::checkCollisionWith(const Collider::Ptr & other)
{
	return false;
}

bool SphereCollider::init()
{
	auto device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	
	ID3DXMesh* mesh;
	D3DXCreateSphere(device, this->radius, 20, 20, &mesh, nullptr);

	_debugMesh = Mesh::create();
	_debugMesh->setD3DMesh(mesh);
	_debugMesh->materials.push_back(Material::get("green"));
	_debugMesh->setFillMode(FillMode::WIREFRAME);

	return true;
}

void SphereCollider::update(float dt)
{
}

float SphereCollider::getRadiusInWorld()
{
	if (_target == nullptr)
	{
		return this->radius;
	}

	auto world = _target->getWorldTransform();
	return world->getScale().x * this->radius;
}

Vec3 SphereCollider::getCenterInWorld()
{
	if (_target == nullptr)
		return this->center;

	auto world = _target->getWorldTransform();
	auto wCenter = world->transformPosition(this->center);

	return wCenter;
}

