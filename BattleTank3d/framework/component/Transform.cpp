#include "Transform.h"
#include "..\base\Object.h"

USING_NS_TINY;

Transform::Transform() :
	position(0.0f, 0.0f, 0.0f),
	rotate(0.0f, 0.0f, 0.0f),
	scale(1.0f, 1.0f, 1.0f),
	normal(0.0,1.0f,0.0f),
	localRotation(0.0f, 0.0f, 0.0f)
{
	right = Vec3(1, 0, 0);
	up = Vec3(0, 1, 0);
	forward = Vec3(0, 0, 1);
}

Transform::Transform(const D3DXMATRIX & mat)
{
	D3DXQUATERNION rotation;
	D3DXMatrixDecompose(&scale, &rotation, &position, &mat);

	//D3DXMATRIX rotateMat;
	//D3DXMatrixRotationQuaternion(&rotateMat, &rotation);
	
	this->rotate = this->getRotationFromMatrix(mat);

	right = Vec3(1, 0, 0);
	up = Vec3(0, 1, 0);
	forward = Vec3(0, 0, 1);
}

D3DXMATRIX Transform::getWorldRotationMatrix()
{
	D3DXMATRIX rotateMatX;
	D3DXMatrixIdentity(&rotateMatX);
	D3DXMATRIX rotateMatY;
	D3DXMatrixIdentity(&rotateMatY);
	D3DXMATRIX rotateMatZ;
	D3DXMatrixIdentity(&rotateMatZ);

	D3DXMatrixRotationX(&rotateMatX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&rotateMatY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&rotateMatZ, D3DXToRadian(rotate.z));

	return rotateMatX * rotateMatY * rotateMatZ;
}

D3DXMATRIX Transform::getLocalRotationMatrix()
{
	// local rotate
	D3DXMATRIX localrotateMatX;
	D3DXMatrixIdentity(&localrotateMatX);
	D3DXMATRIX localrotateMatY;
	D3DXMatrixIdentity(&localrotateMatY);
	D3DXMATRIX localrotateMatZ;
	D3DXMatrixIdentity(&localrotateMatZ);

	D3DXMatrixRotationX(&localrotateMatX, D3DXToRadian(localRotation.x));
	D3DXMatrixRotationY(&localrotateMatY, D3DXToRadian(localRotation.y));
	D3DXMatrixRotationZ(&localrotateMatZ, D3DXToRadian(localRotation.z));

	return localrotateMatX * localrotateMatY * localrotateMatZ;
}

bool Transform::init()
{
	return true;
}

const Vec3 & Transform::getPosition() const
{
	return position;
}

void Transform::setPosition(const Vec3 & pos)
{
	position = pos;

	if(_target != nullptr)
		_target->setDirty(true);
}

void Transform::setPosition(float x, float y, float z)
{
	position.x = x;
	position.y = y;
	position.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setPositionX(float x)
{
	position.x = x;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setPositionY(float y)
{
	position.y = y;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setPositionZ(float z)
{
	position.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

const Vec3 & Transform::getScale() const
{
	return scale;
}

void Transform::setScale(const Vec3 & value)
{
	scale = value;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setScale(float x, float y, float z)
{
	scale.x = x;
	scale.y = y;
	scale.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setScale(float value)
{
	this->setScale(value, value, value);
}

void Transform::setScaleX(float x)
{
	scale.x = x;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setScaleY(float y)
{
	scale.y = y;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setScaleZ(float z)
{
	scale.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

const Vec3 & Transform::getRotate() const
{
	return rotate;
}

void Transform::setRotate(const Vec3 & r)
{
	rotate = r;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setRotate(float x, float y, float z)
{
	rotate.x = x;
	rotate.y = y;
	rotate.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setRotateX(float x)
{
	rotate.x = x;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setRotateY(float y)
{
	rotate.y = y;

	if (_target != nullptr)
		_target->setDirty(true);
}

void Transform::setRotateZ(float z)
{
	rotate.z = z;

	if (_target != nullptr)
		_target->setDirty(true);
}

Transform::Ptr Transform::combine(const Transform::Ptr& other) 
{
	auto otherMat = other->getMatrix();
	auto thisMat = this->getMatrix();
	
	D3DXMATRIX result = thisMat * otherMat;

	return Transform::Ptr(new Transform(result));
}

const Vec3& Transform::getNormal() const
{
	return this->normal;
}
void Transform::setNormal(const Vec3& nor) {
	this->normal = nor;
	if (_target != nullptr)
		_target->setDirty(true);
}
D3DXMATRIX Transform::getMatrix() 
{
	D3DXMATRIX matrix;

	// scale
	D3DXMATRIX scaleMat;
	D3DXMatrixScaling(&scaleMat, scale.x, scale.y, scale.z);

	// world rotate
	auto worldRotation = this->getWorldRotationMatrix();
	auto localRotation = this->getLocalRotationMatrix();

	D3DXVECTOR4 x, y, z;
	D3DXVec3Transform(&z, &forward, &worldRotation);
	forward = Vec3(z.x, z.y, z.z);
	D3DXVec3Normalize(&forward, &forward);

	D3DXVec3Transform(&x, &right, &worldRotation);
	right = Vec3(x.x, x.y, x.z);
	D3DXVec3Normalize(&right, &right);

	D3DXVec3Transform(&y, &up, &worldRotation);
	up = Vec3(y.x, y.y, y.z);
	D3DXVec3Normalize(&up, &up);

	// translate
	D3DXMATRIX translateMat;
	D3DXMatrixTranslation(&translateMat, position.x, position.y, position.z);

	matrix = localRotation * worldRotation * scaleMat * translateMat;

	return matrix;
}

Vec3 Transform::transformPosition(const Vec3 & position)
{
	Vec3 result = Vec3(0.0f, 0.0f, 0.0f);
	
	D3DXMATRIX translateMat;
	D3DXMatrixTranslation(&translateMat, position.x, position.y, position.z);

	auto matResult = translateMat * this->getMatrix();

	result.x = matResult._41;
	result.y = matResult._42;
	result.z = matResult._43;

	return result;
}

void Transform::rotateAround(const Vec3 & axis, float angle)
{
	D3DXMATRIX rotateMat;
	D3DXMatrixRotationAxis(&rotateMat, &axis, D3DXToRadian(angle));

	this->localRotation = this->getRotationFromMatrix(rotateMat);
}

Vec3 Transform::getRotationFromMatrix(const D3DXMATRIX & matrix)
{
	// decompose rotate matrix
	// http://nghiaho.com/?page_id=846
	auto rotX = atan2(matrix._32, matrix._33);
	auto rotY = atan2(-matrix._31, sqrt(matrix._32 * matrix._32 + matrix._33 * matrix._33));
	auto rotZ = atan2(matrix._21, matrix._11);

	return Vec3(D3DXToDegree(rotX), D3DXToDegree(rotY), D3DXToDegree(rotZ));
}
