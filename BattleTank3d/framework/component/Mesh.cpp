﻿#include "Mesh.h"
#include "..\component\Transform.h"
#include "..\base\Material.h"
#include "..\renderer\MeshRenderer.h"
#include "..\SceneManager.h"

#include "..\base\MeshLoader.h"
#include "..\base\OBJFileReader.h"
#include "..\base\MeshFileReader.h"
#include "..\base\Shader.h"
#include "..\base\Texture.h"

USING_NS_TINY;

Mesh::Mesh() : Component()
{
	_fillMode = FillMode::SOLID;
}

Mesh::~Mesh()
{
	if (_d3dMesh != nullptr)
		_d3dMesh->Release();
}

bool Mesh::init()
{
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	return true;
}

bool Mesh::initBox(const Vec3 & minPosition, const Vec3 & maxPosition)
{
	_device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	Mesh::Vertex vertex[] =
	{
		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, maxPosition.z)), // side 1
		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, maxPosition.z)),

		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, minPosition.z)), // side 2
		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, minPosition.z)),

		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, minPosition.z)), // side 3
		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, maxPosition.z)),

		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, minPosition.z)), // side 4
		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, maxPosition.z)),

		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, minPosition.z)), // side 5
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, minPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(maxPosition.x, maxPosition.y, maxPosition.z)),

		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, minPosition.z)), // side 6
		Mesh::Vertex(Vec3(minPosition.x, minPosition.y, maxPosition.z)),
		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, minPosition.z)),
		Mesh::Vertex(Vec3(minPosition.x, maxPosition.y, maxPosition.z)),
	};

	for (size_t i = 0; i < 24; i++)
	{
		this->vertices.push_back(vertex[i]);
	}

	short index[] =
	{
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 5, 6,    // side 2
		6, 5, 7,
		8, 9, 10,    // side 3
		10, 9, 11,
		12, 13, 14,    // side 4
		14, 13, 15,
		16, 17, 18,    // side 5
		18, 17, 19,
		20, 21, 22,    // side 6
		22, 21, 23,
	};

	for (size_t i = 0; i < 36; i++)
	{
		this->indices.push_back(index[i]);
	}

	return true;
}

bool Mesh::initWithData(const std::vector<Vertex>& vertices, const std::vector<unsigned long>& indices, const std::vector<Material::Ptr>& materials)
{
	if (!this->init())
	{
		return false;
	}

	this->vertices = vertices;
	this->indices = indices;
	this->materials = materials;

	return this->buildMesh();
}

void Mesh::addMaterial(const std::shared_ptr<Material>& material)
{
	materials.push_back(material);
}

void Mesh::copyVerticesData(ID3DXMesh* mesh, const std::vector<Vertex>& vertices)
{
	// Copy the vertex data
	Mesh::Vertex* pVertex;
	mesh->LockVertexBuffer(0, (void**)&pVertex);
	memcpy(pVertex, vertices.data(), vertices.size() * sizeof(Mesh::Vertex));
	mesh->UnlockVertexBuffer();
}

void Mesh::copyIndicesData(ID3DXMesh* mesh, const std::vector<DWORD>& indices)
{
	// Copy the index data
	DWORD* pIndex;
	mesh->LockIndexBuffer(0, (void**)&pIndex);
	memcpy(pIndex, indices.data(), indices.size() * sizeof(unsigned long));
	mesh->UnlockIndexBuffer();
}

void Mesh::copyAttributesData(ID3DXMesh* mesh, const std::vector<DWORD>& attributes)
{
	// Copy the attribute data
	DWORD* pSubset;
	mesh->LockAttributeBuffer(0, &pSubset);
	memcpy(pSubset, attributes.data(), attributes.size() * sizeof(DWORD));
	mesh->UnlockAttributeBuffer();
}

void Mesh::optimizeMesh(ID3DXMesh* mesh)
{
	// Optimize mesh
	DWORD* aAdjacency = new DWORD[mesh->GetNumFaces() * 3];
	if (aAdjacency == NULL)
	{
		return;
	}

	mesh->GenerateAdjacency(1e-6f, aAdjacency);
	mesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, aAdjacency, NULL, NULL, NULL);

	delete[] aAdjacency;
}

void Mesh::calculateNormal(ID3DXMesh& mesh)
{
	if (!(mesh.GetFVF() & D3DFVF_NORMAL))
	{
		LPD3DXMESH tempMesh;
		mesh.CloneMeshFVF(D3DXMESH_MANAGED, mesh.GetFVF() | D3DFVF_NORMAL, _device, &tempMesh);

		D3DXComputeNormals(tempMesh, 0);

		//mesh.Release();
		mesh = *tempMesh;
	}
}

void Mesh::loadMaterialTextures(const std::vector<Material::Ptr>& materials)
{
	for (int iMaterial = 0; iMaterial < materials.size(); iMaterial++)
	{
		auto pMaterial = materials.at(iMaterial);
		if (pMaterial->texturePaths.find("diffuse") != pMaterial->texturePaths.end())
		{
			// Avoid loading the same texture twice
			//bool bFound = false;
			//for (int x = 0; x < iMaterial; x++)
			//{
			//	Material* pCur = materials.at(x);
			//	if (0 == pCur->texturePaths["diffuse"].compare(pMaterial->texturePaths["diffuse"]))
			//	{
			//		bFound = true;
			//		pMaterial->textures["diffuse"] = pCur->textures["diffuse"];
			//		break;
			//	}
			//}

			// get texture from resource manager
			pMaterial->textures["diffuse"] = Texture::get(pMaterial->texturePaths["diffuse"].c_str());
		}

		if (pMaterial->texturePaths.find("specular") != pMaterial->texturePaths.end())
		{
			pMaterial->textures["specular"] = Texture::get(pMaterial->texturePaths["specular"].c_str());
		}

		// add to renderer
		// this->addMaterial(materials[iMaterial]);
	}
}

void Mesh::calculateMinMaxBounds(ID3DXMesh * mesh)
{
	Mesh::Vertex* vertices;

	auto result = _d3dMesh->LockVertexBuffer(D3DLOCK_READONLY, (LPVOID*)&vertices);
	
	if (FAILED(result))
		return;

	D3DXComputeBoundingBox(
		(Vec3*)vertices, _d3dMesh->GetNumVertices(), D3DXGetFVFVertexSize(_d3dMesh->GetFVF()), 
		&_minBoundPosition, &_maxBoundPosition);

	_d3dMesh->UnlockVertexBuffer();
}

bool Mesh::buildMesh()
{
	// Create mesh
	_d3dMesh = NULL;

	HRESULT result = D3DXCreateMesh(indices.size() / 3, vertices.size(),
		D3DXMESH_MANAGED | D3DXMESH_32BIT, MeshLoader::VERTEX_DECL,
		_device, &_d3dMesh);

	if (FAILED(result))
	{
		auto code = HRESULT_CODE(result);
		return false;
	}

	this->copyVerticesData(_d3dMesh, vertices);
	this->copyIndicesData(_d3dMesh, indices);
	this->copyAttributesData(_d3dMesh, attributes);
	this->optimizeMesh(_d3dMesh);
	this->calculateNormal(*_d3dMesh);
	this->loadMaterialTextures(materials);
	this->calculateMinMaxBounds(_d3dMesh);

	return true;
}

void Mesh::setD3DMesh(ID3DXMesh * mesh)
{
	_d3dMesh = mesh;
}

ID3DXMesh* Mesh::getD3DMesh()
{
	return _d3dMesh;
}