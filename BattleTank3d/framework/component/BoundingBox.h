#pragma once

#include "Mesh.h"
#include "Collider.h"
#include "..\geometry\Box.h"

NS_TINY_BEGIN

class Model;

class BoundingBox : public Collider, public Box
{
public:
	typedef std::shared_ptr<BoundingBox> Ptr;
	~BoundingBox();

	static Ptr create(const Vec3& minPos, const Vec3& maxPos)
	{
		auto boundingBox = Ptr(new BoundingBox(minPos, maxPos));
		if (boundingBox->init())
		{
			return boundingBox;
		}

		return nullptr;
	}

	static Ptr createWithModel(const std::shared_ptr<Model>& model, const Direction& offset = Direction())
	{
		auto boundingBox = Ptr(new BoundingBox());
		boundingBox->setOffset(offset);
		if (boundingBox->initWithModel(model))
		{
			return boundingBox;
		}

		return nullptr;
	}

	virtual bool init() override;
	virtual bool initWithModel(const std::shared_ptr<Model>& model);

	virtual bool checkCollisionWith(const Collider::Ptr& other) override;

	bool intersectAABB(const BoundingBox::Ptr& other);
	bool intersectSAT(const BoundingBox::Ptr& other);

	void updateWorldPosition();

	inline void setOffset(const Direction& offset)
	{
		_offset = offset;
	}

	inline const Direction& getOffset()
	{
		return _offset;
	}

	inline const Vec3& getMinPositionInWorld() { return _minWorldPosition; }
	const Vec3& getMaxPositionInWorld() { return _maxWorldPosition; }



private:
	BoundingBox();
	BoundingBox(const Vec3& minPos, const Vec3& maxPos);

	Vec3 _minWorldPosition;	// world
	Vec3 _maxWorldPosition;

	Vec3 _minPositionMesh;
	Vec3 _maxPositionMesh;

	Direction _offset;

	std::shared_ptr<Mesh> _targetMesh;

	void createDebugMesh();

	std::vector<Vec3> _points;
};

NS_TINY_END