#pragma once

#include "..\TinyDefinitions.h"
#include "..\base\Component.h"

NS_TINY_BEGIN

class Transform : public Component
{
public:
	typedef std::shared_ptr<Transform> Ptr;

	static Ptr create()
	{
		auto transform = std::shared_ptr<Transform>(new Transform());
		if (transform->init())
			return transform;

		return nullptr;
	}

	Vec3 position;
	Vec3 scale;

	// degree
	Vec3 rotate;
	Vec3 localRotation;

	Vec3 forward;
	Vec3 right;
	Vec3 up;

	Vec3 normal;

	// Inherited via Component
	virtual bool init() override;
	virtual const Vec3& getNormal() const;
	virtual void setNormal(const Vec3& nor);

	virtual const Vec3& getPosition() const;
	virtual void setPosition(const Vec3& position);
	virtual void setPosition(float x, float y, float z);
	virtual void setPositionX(float x);
	virtual void setPositionY(float y);
	virtual void setPositionZ(float z);

	virtual const Vec3& getScale() const;
	virtual void setScale(const Vec3& value);
	virtual void setScale(float x, float y, float z);
	virtual void setScale(float value);
	virtual void setScaleX(float x);
	virtual void setScaleY(float y);
	virtual void setScaleZ(float z);

	virtual const Vec3& getRotate() const;
	virtual void setRotate(const Vec3& rotate);
	virtual void setRotate(float x, float y, float z);
	virtual void setRotateX(float x);
	virtual void setRotateY(float y);
	virtual void setRotateZ(float z);

	virtual Transform::Ptr combine(const Transform::Ptr& parent) ;
	virtual D3DXMATRIX getMatrix();

	virtual Vec3 transformPosition(const Vec3& position);

	virtual void rotateAround(const Vec3& axis, float angle);
	
	Vec3 getRotationFromMatrix(const D3DXMATRIX& matrix);

private:
	Transform();
	Transform(const D3DXMATRIX& mat);

	D3DXMATRIX getWorldRotationMatrix();
	D3DXMATRIX getLocalRotationMatrix();
};

NS_TINY_END