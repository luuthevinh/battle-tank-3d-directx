#pragma once

#include "..\TinyDefinitions.h"
#include "..\base\Component.h"

NS_TINY_BEGIN

class Mesh;

class Collider : public Component
{
public:
	DEF_SHARED_PTR(Collider);

	~Collider();

	// Inherited via Component
	virtual bool init();

	virtual bool checkCollisionWith(const Collider::Ptr& other) = 0;

	inline std::shared_ptr<Mesh> getDebugMesh() { return _debugMesh; }
protected:
	Collider();

	std::shared_ptr<Mesh> _debugMesh;
};

NS_TINY_END