#pragma once

#include "..\TinyDefinitions.h"
#include "Collider.h"
#include "..\geometry\Sphere.h"

NS_TINY_BEGIN


class SphereCollider : public Collider, public Sphere
{
public:
	DEF_SHARED_PTR(SphereCollider);

	static Ptr create(const Vec3& position, float radius)
	{
		auto sphere = Ptr(new SphereCollider());

		sphere->center = position;
		sphere->radius = radius;

		if(sphere->init())
			return sphere;

		return nullptr;
	}

	~SphereCollider();

	// Inherited via Collider
	virtual bool checkCollisionWith(const Collider::Ptr & other) override;

	virtual bool init() override;
	virtual void update(float dt) override;

	// get radius in world
	float getRadiusInWorld();

	Vec3 getCenterInWorld();

private:
	SphereCollider();
};

NS_TINY_END