#include "BoundingBox.h"
#include "Mesh.h"
#include "..\base\Model.h"
#include "..\base\Material.h"
#include "..\SceneManager.h"
#include "..\component\Transform.h"

USING_NS_TINY;

BoundingBox::BoundingBox()
{
}

BoundingBox::BoundingBox(const Vec3& minPos, const Vec3& maxPos) 
{
	minPosition = minPos;
	maxPosition = maxPos;
}

BoundingBox::~BoundingBox()
{
}

bool BoundingBox::init()
{
	this->createDebugMesh();

	return true;
}

bool BoundingBox::initWithModel(const std::shared_ptr<Model>& model)
{
	auto device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();
	_targetMesh = model->getMesh();

	_minPositionMesh = _targetMesh->getMinBoundingPosition();
	_maxPositionMesh = _targetMesh->getMaxBoundingPosition();

	this->minPosition = _minPositionMesh - Vec3(_offset.left, _offset.down, _offset.front);
	this->maxPosition = _maxPositionMesh - Vec3(_offset.right, _offset.up, _offset.behind);

	this->createDebugMesh();

	auto parentTransform = model->getTransform();

	this->minPosition = parentTransform->transformPosition(minPosition);
	this->maxPosition = parentTransform->transformPosition(maxPosition);

	// 8 points
	_points.push_back(Vec3(minPosition.x, minPosition.y, minPosition.z));
	_points.push_back(Vec3(minPosition.x, minPosition.y, maxPosition.z));
	_points.push_back(Vec3(maxPosition.x, minPosition.y, maxPosition.z));
	_points.push_back(Vec3(maxPosition.x, minPosition.y, minPosition.z));
	_points.push_back(Vec3(minPosition.x, maxPosition.y, minPosition.z));
	_points.push_back(Vec3(minPosition.x, maxPosition.y, maxPosition.z));
	_points.push_back(Vec3(maxPosition.x, maxPosition.y, maxPosition.z));
	_points.push_back(Vec3(maxPosition.x, maxPosition.y, minPosition.z));

	return true;
}

bool BoundingBox::checkCollisionWith(const Collider::Ptr & other)
{
	auto box = dynamic_pointer_cast<BoundingBox>(other);
	if (box == nullptr)
		return false;

	return intersectAABB(box);
}

bool BoundingBox::intersectAABB(const BoundingBox::Ptr & other)
{
	this->updateWorldPosition();
	auto thisMin = _minWorldPosition;
	auto thisMax = _maxWorldPosition;
	
	other->updateWorldPosition();
	auto otherMin = other->_minWorldPosition;
	auto otherMax = other->_maxWorldPosition;

	//char str[100];
	//sprintf(str, "min (%.2f, %.2f, %.2f)\n", thisMin.x, thisMin.y, thisMin.z);
	//Log(str);
	//sprintf(str, "max (%.2f, %.2f, %.2f)\n", thisMax.x, thisMax.y, thisMax.z);
	//Log(str);

	return ((thisMin.x >= otherMin.x && thisMin.x <= otherMax.x) || (otherMin.x >= thisMin.x && otherMin.x <= thisMax.x)) &&
		((thisMin.y >= otherMin.y && thisMin.y <= otherMax.y) || (otherMin.y >= thisMin.y && otherMin.y <= thisMax.y)) &&
		((thisMin.z >= otherMin.z && thisMin.z <= otherMax.z) || (otherMin.z >= thisMin.z && otherMin.z <= thisMax.z));
}

bool BoundingBox::intersectSAT(const BoundingBox::Ptr & other)
{
	return false;
}

void BoundingBox::createDebugMesh()
{
	_debugMesh = Mesh::createBox(minPosition, maxPosition);
	
	_debugMesh->materials.push_back(Material::get("green"));
	_debugMesh->setFillMode(FillMode::WIREFRAME);

	_debugMesh->buildMesh();
}

void BoundingBox::updateWorldPosition()
{
	Transform::Ptr thisWorld = Transform::create();

	if (_target != nullptr)
	{
		thisWorld = _target->getWorldTransform();
	}

	_minWorldPosition = thisWorld->transformPosition(_points[0]);
	_maxWorldPosition = thisWorld->transformPosition(_points[6]);;

	for (size_t i = 0; i < 8; i++)
	{
		auto p = thisWorld->transformPosition(_points[i]);

		if (D3DXVec3Length(&p) > D3DXVec3Length(&_maxWorldPosition) )
		{
			_maxWorldPosition = p;
			continue;
		}
		
		if (D3DXVec3Length(&p) < D3DXVec3Length(&_minWorldPosition))
		{
			_minWorldPosition = p;
		}
	}

}