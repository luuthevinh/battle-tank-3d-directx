#pragma once

#include <vector>
#include "..\TinyDefinitions.h"
#include "..\base\Component.h"

NS_TINY_BEGIN

class MeshRenderer;
class Material;
class MeshFileReader;
class Shader;

using namespace std;

class Mesh : public Component
{
public:
	struct Vertex
	{
		Vec3 position;
		Vec3 normal;
		Vec2 texcoord;

		Vertex() {}

		Vertex(Vec3 pos, Vec3 nor, Vec2 uv)
		{
			position = pos;
			normal = nor;
			texcoord = uv;
		}

		Vertex(Vec3 pos, Vec2 uv)
		{
			position = pos;
			normal = Vec3(0.0F,0.0F,0.0F);
			texcoord = uv;
		}

		Vertex(Vec3 pos)
		{
			position = pos;
			normal = Vec3(0.0f, 0.0f, 0.0f);
			texcoord = Vec2(0.0f, 0.0f);
		}

		bool operator==(const Vertex& other) {
			if (this->position != other.position)
				return false;

			if (this->normal != other.normal)
				return false;

			if (this->texcoord != other.texcoord)
				return false;

			return true;
		}
	};
	
	typedef shared_ptr<Mesh> Ptr;

	std::vector<Vertex> vertices;
	std::vector<DWORD> indices;
	std::vector<DWORD> attributes;
	std::vector<std::shared_ptr<Material>> materials;

	int numSubsets;

	~Mesh();

	/*
	* after creating object and fill data into vertices, indices...
	you must call buildMesh() to create mesh with that data.
	*/
	static Ptr create()
	{
		auto mesh = shared_ptr<Mesh>(new Mesh());
		if (mesh->init())
			return mesh;

		return nullptr;
	}

	static Ptr createBox(const Vec3& minPosition, const Vec3& maxPosition)
	{
		auto mesh = shared_ptr<Mesh>(new Mesh());
		if (mesh->initBox(minPosition, maxPosition))
			return mesh;

		return nullptr;
	}

	virtual bool init() override;
	virtual bool initBox(const Vec3& minPosition, const Vec3& maxPosition);

	virtual bool buildMesh();

	virtual bool initWithData(
		const std::vector<Vertex>& vertices, 
		const std::vector<DWORD>& indices, 
		const std::vector<std::shared_ptr<Material>>& materials);

	virtual void addMaterial(const std::shared_ptr<Material>& material);

	virtual void setD3DMesh(ID3DXMesh* mesh);
	virtual ID3DXMesh* getD3DMesh();

	void calculateNormal(ID3DXMesh& mesh);

	virtual inline const Vec3& getMinBoundingPosition()
	{
		return _minBoundPosition;
	}

	virtual inline const Vec3& getMaxBoundingPosition()
	{
		return _maxBoundPosition;
	}

	virtual inline void setFillMode(const FillMode& mode)
	{
		_fillMode = mode;
	}

	virtual inline const FillMode& getFillMode()
	{
		return _fillMode;
	}

protected:
	Mesh();

	LPDIRECT3DDEVICE9 _device;
	ID3DXMesh* _d3dMesh;

	void copyVerticesData(ID3DXMesh* mesh, const std::vector<Vertex>& vertices);
	void copyIndicesData(ID3DXMesh* mesh, const std::vector<DWORD>& indices);
	void copyAttributesData(ID3DXMesh* mesh, const std::vector<DWORD>& attributes);

	void optimizeMesh(ID3DXMesh* mesh);

	void loadMaterialTextures(const std::vector<std::shared_ptr<Material>>& materials);

	Vec3 _minBoundPosition;
	Vec3 _maxBoundPosition;
	void calculateMinMaxBounds(ID3DXMesh* mesh);

	FillMode _fillMode;
};

NS_TINY_END