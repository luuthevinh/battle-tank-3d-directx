#pragma once

#include <string>
#include "..\TinyDefinitions.h"

NS_TINY_BEGIN

using namespace std;

class Text
{
public:
	DEF_SHARED_PTR(Text);

	static Ptr create(const char* font, const char* text, const Vec2& position)
	{
		auto t = Ptr(new Text(font, text, position.x, position.y, 16));
		if (t->init())
		{
			return t;
		}

		return nullptr;
	}

	~Text();

	virtual bool init();
	virtual void update(float dt);
	virtual void draw();

	void setText(string text);
	string getText();

	void setFontHeight(int fontheight);
	int getFontHeight();

	void setFontWeight(UINT fontW);
	UINT getFontWeight();

	void setItalic(bool i);
	bool isItalic();

	void setTextAlign(DWORD align);
	DWORD getTextAlign();

	void setColor(D3DCOLOR color);
	D3DCOLOR getColor();

private:
	Text(string fontName, string text, float x = 0, float y = 0, int fontSize = 21);

	ID3DXFont* _font;
	string _fontName;
	string _text;

	int _fontHeight;
	UINT _fontWeight;
	bool _italic;
	DWORD _textAlign;					//DT_LEFT | DT_WORDBREAK | DT_CENTER | DT_RIGHT ...
	D3DCOLOR _color;

	RECT _textRect;
	Vec2 _position;

	void updateFont();
};

NS_TINY_END