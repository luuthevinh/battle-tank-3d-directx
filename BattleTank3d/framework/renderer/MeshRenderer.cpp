#include "MeshRenderer.h"
#include "..\base\Object.h"
#include "..\base\Material.h"
#include "..\base\Texture.h"

#include "..\Utils.h"
#include "..\base\Shader.h"
#include "..\base\Effect.h"
#include "..\component\Mesh.h"

USING_NS_TINY;

MeshRenderer::MeshRenderer()
{
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::draw()
{
	Renderer::draw();

	auto numMeshes = _meshes.size();
	for (auto it = _meshes.begin(); it != _meshes.end(); it++)
	{
		auto meshName = it->first;
		auto mesh = it->second;

		_device->SetRenderState(D3DRS_FILLMODE, mesh->getFillMode());

		if (_shader != nullptr)
		{
			this->drawMeshWithShader(meshName, mesh);
		}
		else
		{
			if (_effect != nullptr)
			{
				this->drawMeshWithEffect(meshName, mesh);
			}
			else
			{
				this->drawMesh(meshName, mesh);
			}
		}

	}
}

std::string MeshRenderer::setMesh(const shared_ptr<Mesh>& mesh)
{
	char defaultName[100];
	sprintf(defaultName, "mesh%d", _meshes.size());

	_meshes[defaultName] = mesh;

	this->updateMateialsByMaterialsOfMesh(defaultName, mesh->materials);

	return defaultName;
}

void MeshRenderer::setMesh(const char * name, const shared_ptr<Mesh>& mesh)
{
	_meshes[name] = mesh;

	this->updateMateialsByMaterialsOfMesh(name, mesh->materials);
}

const std::vector<std::shared_ptr<Material>>& MeshRenderer::getMaterials(int index /*= 0*/)
{
	if (index >= _materials.size())
		return _materials.begin()->second;

	auto it = _materials.begin();
	for (auto i = 0; i < index; i++)
	{
		it++;
	}

	return _materials.at(it->first);
}

void MeshRenderer::updateMaterial(const std::string& meshName, int materialIndex, const std::shared_ptr<Material>& material)
{
	_materials.at(meshName).at(materialIndex) = material;
}

void MeshRenderer::removeMeshByName(const std::string & name)
{
	if (_meshes.find(name) == _meshes.end())
		return;

	// mesh
	_meshes.erase(name);

	// materials
	_materials.at(name).clear();
	_materials.erase(name);
}

void MeshRenderer::drawMesh(const std::string& meshName, const shared_ptr<Mesh>& mesh)
{
	auto materials = _materials.at(meshName);

	for (size_t i = 0; i < materials.size(); i++)
	{
		_device->SetMaterial(&materials[i]->getD3DMaterial());

		if (materials[i]->textures[Material::MATERIAL_DIFFUSE] != nullptr)
		{
			_device->SetTexture(0, materials[i]->textures[Material::MATERIAL_DIFFUSE]->getD3DTexture());

			// fix me: use specular light with texture not working
			_device->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
		}
		else
		{
			_device->SetTexture(0, nullptr);
		}

		mesh->getD3DMesh()->DrawSubset(i);
	}
}

void MeshRenderer::drawMeshWithEffect(const std::string& meshName, const shared_ptr<Mesh>& mesh)
{
	auto materials = _materials.at(meshName);

	auto passes = _effect->begin(_techniqueName.c_str());
	auto effect = _effect->getD3DEffect();

	for (size_t i = 0; i < passes; i++)
	{
		effect->BeginPass(i);

		for (size_t iMaterial = 0; iMaterial < materials.size(); iMaterial++)
		{
			_effect->onBeginPass(materials[iMaterial]);
			// commit changes
			effect->CommitChanges();

			_device->SetMaterial(&materials[iMaterial]->getD3DMaterial());
			

			if (materials[iMaterial]->textures.find(Material::MATERIAL_DIFFUSE) != materials[iMaterial]->textures.end())
			{
				_device->SetTexture(0, materials[iMaterial]->textures[Material::MATERIAL_DIFFUSE]->getD3DTexture());
			}
			else
			{
				_device->SetTexture(0, nullptr);
			}

			if (materials[iMaterial]->textures.find(Material::MATERIAL_SPECULAR) != materials[iMaterial]->textures.end())
			{
				_device->SetTexture(1, materials[iMaterial]->textures[Material::MATERIAL_SPECULAR]->getD3DTexture());
			}
			else
			{
				_device->SetTexture(1, nullptr);
			}

			mesh->getD3DMesh()->DrawSubset(iMaterial);
		}

		effect->EndPass();
	}
	_effect->end();
}

void MeshRenderer::drawMeshWithShader(const std::string& meshName, const shared_ptr<Mesh>& mesh)
{
	auto materials = _materials.at(meshName);

	_shader->begin();

	for (size_t i = 0; i < materials.size(); i++)
	{
		auto diffuse = materials[i]->textures.find(Material::MATERIAL_DIFFUSE);
		auto specular = materials[i]->textures.find(Material::MATERIAL_SPECULAR);

		if (diffuse != materials[i]->textures.end())
		{
			_shader->setTextureInPixelShader((*diffuse).second->getD3DTexture(), "Diffuse");
		}
		else
		{
			_shader->setTextureInPixelShader(nullptr, "Diffuse");
		}

		if (specular != materials[i]->textures.end())
		{
			_shader->setTextureInPixelShader((*specular).second->getD3DTexture(), "Specular");
		}
		else
		{
			_shader->setTextureInPixelShader(nullptr, "Specular");
		}


		mesh->getD3DMesh()->DrawSubset(i);
	}

	_shader->end();
}

void MeshRenderer::drawBoundingBox()
{

}

void MeshRenderer::updateMateialsByMaterialsOfMesh(const std::string& meshName, const MaterialVector& materials)
{
	for (size_t i = 0; i < materials.size(); i++)
	{
		_materials[meshName].push_back(materials[i]);
	}
}


