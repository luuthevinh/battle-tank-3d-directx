#pragma once

#include <map>
#include <vector>
#include "..\TinyDefinitions.h"
#include "..\base\Renderer.h"

NS_TINY_BEGIN

using namespace std;

class Material;
class Mesh;

class MeshRenderer : public Renderer
{
public:
	typedef shared_ptr<MeshRenderer> Ptr;

	static Ptr create() 
	{
		auto renderer = shared_ptr<MeshRenderer>(new MeshRenderer());
		
		if(renderer->init())
			return renderer;

		return nullptr;
	}

	MeshRenderer();
	~MeshRenderer();

	virtual void draw() override;

	// must be called after create all of mesh data.
	std::string setMesh(const shared_ptr<Mesh>& mesh);

	void setMesh(const char* name, const shared_ptr<Mesh>& mesh);

	const std::vector<std::shared_ptr<Material>>& getMaterials(int meshIndex = 0);
	
	void updateMaterial(const std::string& meshName, int materialIndex, const std::shared_ptr<Material>& material);
	void removeMeshByName(const std::string& name);

private:
	typedef std::vector<std::shared_ptr<Material>> MaterialVector;

	std::map<std::string, shared_ptr<Mesh>> _meshes;
	std::map<std::string, MaterialVector> _materials;

	void drawMesh(const std::string& meshName, const shared_ptr<Mesh>& mesh);
	void drawMeshWithEffect(const std::string& meshName, const shared_ptr<Mesh>& mesh);
	void drawMeshWithShader(const std::string& meshName, const shared_ptr<Mesh>& mesh);
	void drawBoundingBox();

	void updateMateialsByMaterialsOfMesh(const std::string& meshName, const MaterialVector& materials);
};

NS_TINY_END