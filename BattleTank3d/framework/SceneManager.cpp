#include "SceneManager.h"
#include "Camera.h"

USING_NS_TINY;

SceneManager* SceneManager::_instance = nullptr;

SceneManager::~SceneManager()
{
}

SceneManager::SceneManager()
{
}

SceneManager * SceneManager::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new SceneManager();
	}

	return _instance;
}

void SceneManager::update(float dt)
{
	if (!_scenes.empty())
	{
		_scenes.back()->update(dt);

		// update main camera transform
		this->mainCameraUpdate();
	}

}

void SceneManager::draw()
{
	if (!_scenes.empty())
		_scenes.back()->draw();
}

void SceneManager::release()
{
	this->clearScenes();

	delete this;
}

void SceneManager::addScene(const Scene::Ptr& scene)
{
	_scenes.push_back(scene);
	_scenes.back()->init();
}

void SceneManager::removeScene()
{
	if (!_scenes.empty())
	{
		_scenes.back()->release();
		_scenes.pop_back();
	}
}

void SceneManager::replaceScene(const Scene::Ptr& scene)
{
	this->removeScene();
	this->addScene(scene);
}

void SceneManager::clearScenes()
{
	while (!_scenes.empty())
	{
		_scenes.back()->release();
		_scenes.pop_back();
	}
}

Scene::Ptr SceneManager::getCurrentScene()
{
	if (!_scenes.empty())
		return _scenes.back();

	return nullptr;
}

void SceneManager::setDevice(Direct3DDevice * device)
{
	_device = device;
}

Direct3DDevice * SceneManager::getDevice()
{
	return _device;
}

void SceneManager::setDispatcher(std::shared_ptr<EventDispatcher> dispatcher)
{
	_dispatcher = dispatcher;
}

std::shared_ptr<EventDispatcher> SceneManager::getEventDispatcher()
{
	return _dispatcher;
}

std::shared_ptr<Camera> SceneManager::getMainCamera()
{
	return _mainCamera;
}

void SceneManager::setMainCamera(std::shared_ptr<Camera> camera)
{
	if (_mainCamera != nullptr)
		_mainCamera->setMain(false);

	_mainCamera = camera;
	_mainCamera->setMain(true);
}

void SceneManager::mainCameraUpdate()
{
	if (_mainCamera == nullptr)
	{
		Log("Not found main camera! Scene should have one camera at least.\n");
		this->createDefaultCamera();
		return;
	}

	D3DXMATRIX viewMat;
	viewMat = _mainCamera->getViewMatrix();
	_device->getDirec3DDevice()->SetTransform(D3DTS_VIEW, &viewMat);

	D3DXMATRIX matProjection;
	matProjection = _mainCamera->getPerspectiveMatrix();
	_device->getDirec3DDevice()->SetTransform(D3DTS_PROJECTION, &matProjection);
}

void SceneManager::createDefaultCamera()
{
	_mainCamera = Camera::create(Vec3(63.0f, 193.0f, -291.0f));
	_scenes.back()->addChild("mainCamera", _mainCamera);

	Log("Default Camera was created.\n");
}

