#include "ResourcesManager.h"
#include "base\Texture.h"
#include "effect\PhongEffect.h"
#include "base\Material.h"
#include "Utils.h"

USING_NS_TINY;

ResourcesManager* ResourcesManager::_instance = nullptr;

ResourcesManager::ResourcesManager()
{
}

ResourcesManager::~ResourcesManager()
{
	_meshes.clear();
	_textures.clear();
	_materials.clear();
	_effects.clear();
}

bool ResourcesManager::init()
{
	// load base effects
	this->addEffect("phong", PhongEffect::create());

	// load base effects
	this->addMaterial("yellow", Material::create("yellow", Vec3(1.0f, 1.0f, 0.0f)));
	this->addMaterial("white", Material::create("white", Vec3(1.0f, 1.0f, 1.0f)));
	this->addMaterial("green", Material::create("green", Utils::rgbToVec3(66, 244, 89)));

	return true;
}

void ResourcesManager::release()
{
	delete this;
}

void ResourcesManager::addMaterial(const char * name, const std::shared_ptr<Material>& material)
{
	_materials[name] = material;
}

std::shared_ptr<Material> ResourcesManager::getMaterial(const char * name)
{
	if (_materials.find(name) == _materials.end())
		return nullptr;

	return _materials.at(name);
}

std::shared_ptr<Texture> ResourcesManager::loadTexture(const char * name)
{
	if (_textures.find(name) == _textures.end())
	{
		auto texture = Texture::create(name);
		if (texture != nullptr)
		{
			_textures[name] = texture;
			return texture;
		}

		return nullptr;
	}

	return _textures.at(name);
}

std::shared_ptr<Texture> ResourcesManager::getTexture(const char * name)
{
	return loadTexture(name);
}

void ResourcesManager::addMesh(const char * name, const std::shared_ptr<Mesh>& mesh)
{
	_meshes[name] = mesh;
}

std::shared_ptr<Mesh> ResourcesManager::getMesh(const char * name)
{
	if (_meshes.find(name) == _meshes.end())
	{
		return nullptr;
	}

	return _meshes.at(name);
}

void ResourcesManager::addEffect(const char * name, const std::shared_ptr<Effect>& effect)
{
	_effects[name] = effect;
}

const std::shared_ptr<Effect>& ResourcesManager::getEffect(const char * name)
{
	if (_effects.find(name) == _effects.end())
	{
		return nullptr;
	}

	return _effects.at(name);
}
