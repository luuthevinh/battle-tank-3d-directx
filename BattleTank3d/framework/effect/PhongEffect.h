#pragma once

#include "..\TinyDefinitions.h"
#include "..\base\Effect.h"

NS_TINY_BEGIN

class PhongEffect : public Effect
{
public:
	typedef std::shared_ptr<PhongEffect> Ptr;

	static Ptr create()
	{
		auto effect = Ptr(new PhongEffect());
		if (effect->initWithFile("resources\\shaders\\phong.effect"))
		{
			effect->useTechnique("PhongEffect");
			return effect;
		}
			
		return nullptr;
	}

	~PhongEffect();

	virtual void onBeginPass(const std::shared_ptr<Material>& material) override;
	
private:
	PhongEffect();
};

NS_TINY_END