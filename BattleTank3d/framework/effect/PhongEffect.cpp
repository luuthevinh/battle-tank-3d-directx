#include "PhongEffect.h"
#include "..\base\Material.h"
#include "..\base\Texture.h"
#include "..\SceneManager.h"
#include "..\Camera.h"
#include "..\component\Transform.h"

USING_NS_TINY;

PhongEffect::PhongEffect() : Effect()
{
}

PhongEffect::~PhongEffect()
{
}

void PhongEffect::onBeginPass(const std::shared_ptr<Material>& material)
{
	if (material->textures.find(Material::MATERIAL_DIFFUSE) != material->textures.end())
	{
		auto diffuse = material->textures.at(Material::MATERIAL_DIFFUSE);
		_effect->SetTexture(this->getConstantHandle("Diffuse"), diffuse->getD3DTexture());
	}
		
	if (material->textures.find(Material::MATERIAL_SPECULAR) != material->textures.end())
	{
		auto specular = material->textures.at(Material::MATERIAL_SPECULAR);
		_effect->SetTexture(this->getConstantHandle("Specular"), specular->getD3DTexture());
	}

	_effect->SetVector(this->getConstantHandle("material.ambient"), &Vec4(material->diffuse, material->alpha));
	_effect->SetVector(this->getConstantHandle("material.specular"), &Vec4(material->specular, material->alpha));
	_effect->SetVector(this->getConstantHandle("material.diffuse"), &Vec4(material->diffuse, material->alpha));
	_effect->SetVector(this->getConstantHandle("material.emissive"), &Vec4(material->emissive, material->alpha));
	_effect->SetFloat(this->getConstantHandle("material.shininess"), material->shininess);


	D3DLIGHT9 light;
	_device->GetLight(0, &light);

	if (light.Type == D3DLIGHTTYPE::D3DLIGHT_DIRECTIONAL)
	{
		_effect->SetValue(this->getConstantHandle("directionLight.direction"), &light.Direction, sizeof(Vec3));
		_effect->SetVector(this->getConstantHandle("directionLight.diffuse"), &Vec4(light.Diffuse.r, light.Diffuse.g, light.Diffuse.b, light.Diffuse.a));
		_effect->SetVector(this->getConstantHandle("directionLight.specular"), &Vec4(light.Specular.r, light.Specular.g, light.Specular.b, light.Specular.a));
		_effect->SetVector(this->getConstantHandle("directionLight.ambient"), &Vec4(light.Ambient.r, light.Ambient.g, light.Ambient.b, light.Ambient.a));
	}
	
	D3DLIGHT9 light1;
	_device->GetLight(1, &light1);
	if (light1.Type == D3DLIGHTTYPE::D3DLIGHT_POINT)
	{
		_effect->SetValue(this->getConstantHandle("pointLight.position"), &light1.Position, sizeof(Vec3));
		_effect->SetVector(this->getConstantHandle("pointLight.diffuse"), &Vec4(light1.Diffuse.r, light1.Diffuse.g, light1.Diffuse.b, 1.0f));
		_effect->SetVector(this->getConstantHandle("pointLight.specular"), &Vec4(light1.Specular.r, light1.Specular.g, light1.Specular.b, 1.0f));
		_effect->SetVector(this->getConstantHandle("pointLight.ambient"), &Vec4(light1.Ambient.r, light1.Ambient.g, light1.Ambient.b, 1.0f));
		_effect->SetFloat(this->getConstantHandle("pointLight.constantValue"), 1.0f);
		_effect->SetFloat(this->getConstantHandle("pointLight.linearValue"), 0.045f);
		_effect->SetFloat(this->getConstantHandle("pointLight.quadraticValue"), 0.0075f);
	}

	auto camera = SceneManager::getInstance()->getMainCamera();
	auto camPos = camera->getTransform()->getPosition();
	_effect->SetValue(this->getConstantHandle("ViewPositon"), camPos, sizeof(Vec3));

	auto world = camera->getViewMatrix() * camera->getWorldMatrix();
	D3DXMATRIX inverseMat;
	D3DXMatrixInverse(&inverseMat, NULL, &world);
	_effect->SetMatrix(this->getConstantHandle("InverseWVP"), &inverseMat);
}

