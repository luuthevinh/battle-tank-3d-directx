﻿#include "Camera.h"
#include "..\Settings.h"
#include "SceneManager.h"
#include "base\EventKeyboardListener.h"
#include "base\EventDispatcher.h"
#include "base\EventMouseListener.h"
#include "base\EventMouse.h"
#include "..\Definitions.h"
#include "component\Transform.h"

USING_NS_TINY;

Camera::Camera(const Vec3 & position, const Vec3 & eye, const Vec3 & up)
{
	//_doFollowTerrain = false;

	_position = position;
	_lookDirection = eye - position;
	_up = up;

	_fovY = 45.0f;
	_aspect = (float)Settings::getInstance()->getWindowSize().width / Settings::getInstance()->getWindowSize().height;
	_nearViewPlane = 1.0f;
	_farViewPlane = 1000.0f;

	_sensitive = 0.001f;
	_enableMouse = false;

	_walkSpeed = 0.0f;
	_strafeSpeed = 0.0f;
	_rollSpeed = 0.0f;

	_isMain = false;
}

Camera::~Camera()
{
}

bool Camera::init()
{
	Object::init();

	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();

	auto listener = EventKeyboardListener::create();
	listener->onKeyPressed = CALLBACK_01(Camera::onKeyPressed, this);
	listener->onKeyReleased = CALLBACK_01(Camera::onKeyReleased, this);

	dispatcher->addEventListener(listener, this);

	auto mouse = EventMouseListener::create();
	mouse->onMouseMoved = CALLBACK_01(Camera::onMouseMoved, this);

	dispatcher->addEventListener(mouse, this);

	this->getTransform()->setPosition(_position);

	return true;
}

void Camera::update(float dt)
{
	this->walk(_walkSpeed * dt);
	this->strafe(_strafeSpeed * dt);
	this->roll(_rollSpeed * dt);

	// used for character cam to move on terrain 
	// turn off if unnecessary
	//if (_doFollowTerrain)
	//{
	//	_terrain->walkOnTerrain(_transform, FIXED_ABSOLUTE_CAM_HEIGHT);
	//	/*float height =	_terrain->getHeight(_position.x, _position.z);
	//	_position.y = height + FIXED_ABSOLUTE_CAM_HEIGHT;
	//	_transform->setPosition(_position);*/
	//}

}

void Camera::strafe(float delta)
{
	_position += _right * delta;
	this->getTransform()->setPosition(_position);
}

void Camera::fly(float delta)
{
	_position += _up * delta;
	this->getTransform()->setPosition(_position);
}

void Camera::walk(float delta)
{
	_position += _lookDirection * delta;
	this->getTransform()->setPosition(_position);
}

void Camera::pitch(float angle)
{
	D3DXMATRIX mat;
	D3DXMatrixRotationAxis(&mat, &_right, angle);

	// rotate _up and _lookDirection around _right vector
	D3DXVec3TransformCoord(&_up, &_up, &mat);
	D3DXVec3TransformCoord(&_lookDirection, &_lookDirection, &mat);
}

void Camera::yaw(float angle)
{
	D3DXMATRIX mat;
	D3DXMatrixRotationAxis(&mat, &_up, angle);

	// rotate _right and _lookDirection around _up or y-axis
	D3DXVec3TransformCoord(&_right, &_right, &mat);
	D3DXVec3TransformCoord(&_lookDirection, &_lookDirection, &mat);
}

void Camera::roll(float angle)
{
	D3DXMATRIX mat;
	D3DXMatrixRotationAxis(&mat, &_lookDirection, angle);

	// rotate _up and _right around _lookDirection vector
	D3DXVec3TransformCoord(&_right, &_right, &mat);
	D3DXVec3TransformCoord(&_up, &_up, &mat);
}

void Camera::setPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;

	this->getTransform()->setPosition(_position);
}

const D3DXMATRIX& Camera::getViewMatrix()
{
	// chuẩn hóa vector hướng của camera
	D3DXVec3Normalize(&_lookDirection, &_lookDirection);

	// vector right là pháp tuyến của look và up
	D3DXVec3Cross(&_right, &_up, &_lookDirection);
	D3DXVec3Normalize(&_right, &_right);

	D3DXVec3Cross(&_up, &_lookDirection, &_right);
	D3DXVec3Normalize(&_up, &_up);

	float x = -D3DXVec3Dot(&_right, &_position);
	float y = -D3DXVec3Dot(&_up, &_position);
	float z = -D3DXVec3Dot(&_lookDirection, &_position);

	(_viewMatrix)(0, 0) = _right.x;
	(_viewMatrix)(0, 1) = _up.x;
	(_viewMatrix)(0, 2) = _lookDirection.x;
	(_viewMatrix)(0, 3) = 0.0f;

	(_viewMatrix)(1, 0) = _right.y;
	(_viewMatrix)(1, 1) = _up.y;
	(_viewMatrix)(1, 2) = _lookDirection.y;
	(_viewMatrix)(1, 3) = 0.0f;

	(_viewMatrix)(2, 0) = _right.z;
	(_viewMatrix)(2, 1) = _up.z;
	(_viewMatrix)(2, 2) = _lookDirection.z;
	(_viewMatrix)(2, 3) = 0.0f;

	(_viewMatrix)(3, 0) = x;
	(_viewMatrix)(3, 1) = y;
	(_viewMatrix)(3, 2) = z;
	(_viewMatrix)(3, 3) = 1.0f;

	return _viewMatrix;
}

const D3DXMATRIX& Camera::getPerspectiveMatrix()
{
	D3DXMatrixPerspectiveFovLH(
		&_projectionMatrix,
		D3DXToRadian(_fovY),
		_aspect,
		_nearViewPlane,
		_farViewPlane);

	return _projectionMatrix;
}

const D3DXMATRIX& Camera::getWorldMatrix()
{
	SceneManager::getInstance()->getDevice()->getDirec3DDevice()->GetTransform(D3DTS_WORLD, &_worldMatrix);
	return _worldMatrix;
}

void Camera::setFOV(float degree)
{
	_fovY = degree;
}

float Camera::getFOV()
{
	return _fovY;
}

void Camera::setAspect(float value)
{
	_aspect = value;
}

float Camera::getAspect()
{
	return _aspect;
}

void Camera::setNearViewPlane(float value)
{
	_nearViewPlane = value;
}

float Camera::getNearViewPlane()
{
	return _nearViewPlane;
}

void Camera::setFarViewPlane(float value)
{
	_farViewPlane = value;
}

float Camera::getFarViewPlane()
{
	return _farViewPlane;
}

void Camera::onKeyPressed(EventKeyboard::KeyCode keycode)
{
	if (!SceneManager::getInstance()->isDebug())
		return;

	switch (keycode)
	{
	case EventKeyboard::KeyCode::KEY_W:
	{
		_walkSpeed = 50.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_S:
	{
		_walkSpeed = -20.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_A:
	{
		_strafeSpeed = -20.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_D:
	{
		_strafeSpeed = 20.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_Q:
	{
		_rollSpeed = 2.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_E:
	{
		_rollSpeed = -2.0f;
		break;
	}
	default:
		break;
	}
}

void Camera::onKeyReleased(EventKeyboard::KeyCode keycode)
{
	if (!SceneManager::getInstance()->isDebug())
		return;

	switch (keycode)
	{
	case EventKeyboard::KeyCode::KEY_W:
	case EventKeyboard::KeyCode::KEY_S:
	{
		_walkSpeed = 0.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_A:
	case EventKeyboard::KeyCode::KEY_D:
	{
		_strafeSpeed = 0.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_Q:
	case EventKeyboard::KeyCode::KEY_E:
	{
		
		_rollSpeed = 0.0f;
		break;
	}
	default:
		break;
	}
}

void Camera::onMouseMoved(EventMouse * e)
{
	//char str[128];
	//sprintf(str, "mouse: (%.2f, %.2f), delta: (%.2f, %.2f)\n", e->getPosition().x, e->getPosition().y, e->getDeltaChange().x, e->getDeltaChange().y);
	//OutputDebugString(str);

	if (!_isMain)
		return;

	if (e->getButton() == EventMouse::Button::BUTTON_LEFT)
	{
		_enableMouse = true;
		Settings::getInstance()->showCursor(false);
	}
	else
	{
		_enableMouse = false;
		Settings::getInstance()->showCursor(true);
	}

	if (_enableMouse)
	{
		this->yaw(e->getDeltaChange().x * _sensitive);
		this->pitch(e->getDeltaChange().y * _sensitive);
	}
}

void Camera::onMousePressed(EventMouse * e)
{
	
}

//
//void Camera::followByTerrain(std::shared_ptr<Terrain> terrain) {
//
//	_terrain = terrain;
//	_doFollowTerrain = true;
//}
//
//void Camera::unfollowByTerrain(bool doClean) {
//	_doFollowTerrain = false;
//	if (doClean)
//	{
//		_terrain._Decref();
//	}
//}
Ray::Ptr Camera::calculatePickingRay(const Vec2& screenPosition)
{
	float px = 0.0f;
	float py = 0.0f;

	auto device = SceneManager::getInstance()->getDevice()->getDirec3DDevice();

	D3DVIEWPORT9 vp;
	device->GetViewport(&vp);

	D3DXMATRIX proj;
	device->GetTransform(D3DTS_PROJECTION, &proj);

	px = (((2.0f * screenPosition.x) / vp.Width) - 1.0f) / proj(0, 0);
	py = (((-2.0f * (vp.Height - screenPosition.y)) / vp.Height) + 1.0f) / proj(1, 1);

	auto ray = Ray::create(Vec3(0.0f, 0.0f, 0.0f), Vec3(px, py, 1.0f));

	// transform to view space

	D3DXMATRIX view;// = this->getViewMatrix();
	device->GetTransform(D3DTS_VIEW, &view);

	D3DXMATRIX viewInverse;
	D3DXMatrixInverse(&viewInverse, 0, &view);

	// transform the ray's origin, w = 1.
	D3DXVec3TransformCoord(&ray->origin, &ray->origin, &viewInverse);

	// transform the ray's direction, w = 0.
	D3DXVec3TransformNormal(&ray->direction, &ray->direction, &viewInverse);

	// normalize the direction
	D3DXVec3Normalize(&ray->direction, &ray->direction);

	return ray;
}

void Camera::setMain(bool value)
{
	_isMain = value;
}
