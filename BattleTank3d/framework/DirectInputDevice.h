#pragma once

#include "TinyDefinitions.h"
#include "base\EventMouse.h"

NS_TINY_BEGIN

class WindowsHelper;
class EventDispatcher;

#define VIRTUAL_KEY_NUMBER 256

class DirectInputDevice
{
public:
	static DirectInputDevice* create(WindowsHelper* window);

	DirectInputDevice();
	~DirectInputDevice();

	bool init(WindowsHelper* window);
	void update();
	void release();
private:
	WindowsHelper* _window;
	std::shared_ptr<EventDispatcher> _dispatcher;
	LPDIRECTINPUT8 _input;

	LPDIRECTINPUTDEVICE8 _keyboard;
	DIDEVICEOBJECTDATA _keyData[VIRTUAL_KEY_NUMBER];

	LPDIRECTINPUTDEVICE8 _mouse;
	DIMOUSESTATE _mouseState;
	DIMOUSESTATE _mouseOldState;

	int _screenWidth, _screenHeight;
	int _mouseX, _mouseY;

	Vec2 getCursorPosition();
	void updateMouseEvent();

	void notifyMouseEvent(const EventMouse::Button& button, const EventMouse::EventMouseType& type);
};

NS_TINY_END