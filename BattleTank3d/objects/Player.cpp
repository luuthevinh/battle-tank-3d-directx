#include "Player.h"
#include "..\Definitions.h"
#include "..\framework\base\Model.h"

USING_NS_TINY;

Player::Player() : Tank()
{
}

Player::~Player()
{
}

bool Player::init()
{
	Tank::init();

	_canMove = true;

	auto model = this->getChild<Model>("model");
	auto renderer = model->getComponent<MeshRenderer>(Component::RENDERDER);
	auto color = renderer->getMaterials().at(0);

	// update other material
	//auto red = Material::createWithMaterial(color->getD3DMaterial());
	//red->diffuse.x = 1.0f;

	//renderer->updateMaterial("mesh0", 0, red);
	//renderer->updateMaterial("mesh0", 2, red);
	//renderer->updateMaterial("mesh0", 4, red);
	//renderer->updateMaterial("mesh0", 6, red);

	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();

	auto keyListener = EventKeyboardListener::create();
	keyListener->onKeyPressed = CALLBACK_01(Player::onKeyPressed, this);
	keyListener->onKeyReleased = CALLBACK_01(Player::onKeyReleased, this);

	dispatcher->addEventListener(keyListener, this);

	auto contactListener = EventContactListener::create();
	contactListener->onContactBegin = CALLBACK_01(Player::onContactBegin, this);

	dispatcher->addEventListener(contactListener, this);


	return true;
}

void Player::update(float dt)
{
	Tank::update(dt);
}

void Player::onKeyPressed(EventKeyboard::KeyCode keycode)
{
	if (SceneManager::getInstance()->isDebug() || !_canMove)
		return;

	switch (keycode)
	{
	case EventKeyboard::KeyCode::KEY_W:
	{
		_velocity = MAX_SPEED;

		break;
	}
	case EventKeyboard::KeyCode::KEY_S:
	{
		_velocity = -MAX_SPEED;

		break;
	}
	case EventKeyboard::KeyCode::KEY_A:
	{
		_rotateSpeed = -MAX_SPEED;

		break;
	}
	case EventKeyboard::KeyCode::KEY_D:
	{
		_rotateSpeed = MAX_SPEED;

		break;
	}
	default:
		break;
	}
}

void Player::onKeyReleased(EventKeyboard::KeyCode keycode)
{
	if (SceneManager::getInstance()->isDebug())
		return;

	switch (keycode)
	{
	case EventKeyboard::KeyCode::KEY_W:
	case EventKeyboard::KeyCode::KEY_S:
	{
		_velocity = 0.0f;
		break;
	}
	case EventKeyboard::KeyCode::KEY_A:
	case EventKeyboard::KeyCode::KEY_D:
	{
		_rotateSpeed = 0.0f;
		break;
	}
	default:
		break;
	}
}

void Player::onContactBegin(const EventContact::Ptr & contact)
{
	Log("onContactBegin\n");
	//_canMove = false;
	_velocity = 0.0f;
}

void Player::onContactEnd(const EventContact::Ptr & contact)
{
	//_canMove = true;
}
