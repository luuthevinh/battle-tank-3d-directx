#pragma once

#include "..\Tiny3D.h"

USING_NS_TINY;

class Tank : public Object
{
public:
	typedef std::shared_ptr<Tank> Ptr;

	static Ptr create()
	{
		auto tank = Ptr(new Tank());
		if (tank->init())
		{
			return tank;
		}

		return nullptr;
	}

	~Tank();

	virtual bool init() override;
	virtual void update(float dt) override;

	virtual void setTerrain(Terrain::Ptr terrain);

protected:
	Tank();

	Terrain::Ptr _terrain;

	float _velocity;
	float _rotateSpeed;

	Vec3 _direction;
	Vec3 _up;
	Vec3 _for;
};
