#pragma once

#include "..\Tiny3D.h"
#include "Tank.h"

class Player : public Tank
{
public:
	typedef std::shared_ptr<Player> Ptr;

	static Ptr create()
	{
		auto player = Ptr(new Player());
		if (player->init())
		{
			return player;
		}

		return nullptr;
	}

	~Player();

	virtual bool init() override;
	virtual void update(float dt);

	void onKeyPressed(tiny::EventKeyboard::KeyCode keycode);
	void onKeyReleased(tiny::EventKeyboard::KeyCode keycode);
	
	void onContactBegin(const EventContact::Ptr& contact);
	void onContactEnd(const EventContact::Ptr& contact);

private:
	Player();

	bool _canMove;
};