#include "FollowCamera.h"
#include "..\Settings.h"

USING_NS_TINY;

FollowCamera::FollowCamera() : Camera(Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f))
{
}

FollowCamera::~FollowCamera()
{
}

bool FollowCamera::initWithTarget(const Object::Ptr& target, float distance, float angle)
{
	Object::init();

	_target = target;
	_distance = distance;
	_distanceFromTarget = Vec3(0.0f, distance, 0.0f);
	_angleRoundTarget = angle;
	_targetOffset = Vec3(0.0f, 0.0f, 0.0f);
	_enableMouse = true;

	this->calculateCameraPosition(1.0f);
	
	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();
	
	auto mouseListener = EventMouseListener::create();
	mouseListener->onMouseMoved = CALLBACK_01(FollowCamera::onMouseMoved, this);

	dispatcher->addEventListener(mouseListener, this);
	
	return true;
}

void FollowCamera::update(float dt)
{
	if (!_isMain)
		return;

	Settings::getInstance()->showCursor(false);

	if (_enableMouse)
	{
		_distanceFromTarget.y += _ySpeed * dt;

		// should negative as UX. Try it if it is confused! Just set positive again
		_angleRoundTarget += -_rotateSpeed * dt;

		_enableMouse = false;
	}

	this->calculateCameraPosition(dt);
}

void FollowCamera::onKeyPressed(EventKeyboard::KeyCode keycode)
{
}

void FollowCamera::onKeyReleased(EventKeyboard::KeyCode keycode)
{
}

void FollowCamera::onMouseMoved(EventMouse * e)
{
	//if (e->getButton() == EventMouse::Button::BUTTON_LEFT)
	//{
	//	_enableMouse = true;
	//}
	//else
	//{
	//	_enableMouse = false;
	//}

	if (!_isMain)
		return;

	_enableMouse = true;
	
	if (_enableMouse)
	{
		_ySpeed = e->getDeltaChange().y * 2.0f;

		_rotateSpeed = e->getDeltaChange().x * 2.0f;
	}
	else
	{
		_ySpeed = 0.0f;
		_rotateSpeed = 0.0f;
	}
}

void FollowCamera::calculateCameraPosition(float dt)
{
	_up = Vec3(0.0f, 1.0f, 0.0f);

	auto targetPosition = _target->getTransform()->getPosition();

	_distanceFromTarget.x = _distance * cos(D3DXToRadian(_angleRoundTarget));
	_distanceFromTarget.z = _distance * sin(D3DXToRadian(_angleRoundTarget));

	_position = targetPosition + _distanceFromTarget;

	_lookDirection = (targetPosition + _targetOffset) - _position;
	D3DXVec3Normalize(&_lookDirection, &_lookDirection);

	D3DXVec3Cross(&_right, &_up, &_lookDirection);
	D3DXVec3Normalize(&_right, &_right);

	D3DXVec3Cross(&_up, &_lookDirection, &_right);
	D3DXVec3Normalize(&_up, &_up);

	this->getTransform()->setPosition(_position);
}

