#pragma once

#include "..\Tiny3D.h"

USING_NS_TINY;

class TankBullet : public Object
{
public:
	DEF_SHARED_PTR(TankBullet);

	~TankBullet();

	virtual bool init() override;

private:
	TankBullet();
};
