#include "Tank.h"

Tank::Tank() : Object()
{
}

Tank::~Tank()
{
}

bool Tank::init()
{
	Object::init();

	//this->getTransform()->setScale(0.02f);

	_for = Vec3(1, 0, 0);
	_up = Vec3(0, 1, 0);

	//auto model = Model::createWithFile("resources\\models\\Tank.fbx"); 
	auto model = Model::createWithFile("resources\\models\\tank\\tank_01.obj");
	model->getTransform()->setPosition(0.0f, 0.0f, 0.0f);
	model->getTransform()->setRotateY(0.0f);
	this->addChild("model", model);

	auto renderer = model->getComponent<MeshRenderer>(Component::RENDERDER);
	renderer->setEffect(Effect::get("phong"));
	//renderer->setTechnique("PhongEffect_NoTexture");

	// bounding box
	Direction offset;
	offset.up = 0.0f;
	auto boundingBox = BoundingBox::createWithModel(model, offset);
	this->addComponent(Component::COLLIDER, boundingBox);

	renderer->setMesh(boundingBox->getDebugMesh());

	auto sphere = SphereCollider::create(Vec3(0.0f, 0.0f, 0.0f), 3.0f);
	this->addComponent(Component::SPHERE_COLLIDER, sphere);

	//renderer->setMesh(sphere->getDebugMesh());
	
	return true;
}

void Tank::update(float dt)
{
	Object::update(dt);

	Vec3 position = this->getTransform()->getPosition();
	Vec3 rotation = this->getTransform()->localRotation;

	//this->getTransform()->rotate.x = 30.0f;
	
	rotation.y += _rotateSpeed * dt;
	
	_direction.z = sin(D3DXToRadian(-rotation.y));
	_direction.x = cos(D3DXToRadian(-rotation.y));
	_direction.y = 0;

	position += _direction * _velocity * dt;

	//if (_terrain != nullptr)
	//{
	//	// adjust translation
	//	float height = _terrain->getHeight(position.x, position.z);

	//	position.y = height + 1.0f;

	//	// adjust rotation
	//	Vec3 normalSurface = _terrain->getNormalSurface(position.x, position.z);
	//	float  alpha = 0.0f;
	//	if (_up != normalSurface) {
	//		alpha = acos(D3DXVec3Dot(&normalSurface, &_up) / (D3DXVec3Length(&normalSurface) * D3DXVec3Length(&_up)));
	//	}
	//	if (alpha > D3DX_PI / 12) // 15 degree
	//	{
	//		alpha /= 2;
	//	}
	//	rotation.z = alpha != 0.0f ? D3DXToDegree(alpha) : rotation.z;

	//	_up = normalSurface;
	//}

	this->getTransform()->setPosition(position);
	this->getTransform()->localRotation = rotation;

}

void Tank::setTerrain(Terrain::Ptr terrain) 
{
	_terrain = terrain;
}

