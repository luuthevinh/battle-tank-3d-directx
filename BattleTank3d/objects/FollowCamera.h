#pragma once

#include "..\Tiny3D.h"

USING_NS_TINY;

class FollowCamera : public tiny::Camera 
{
public:
	typedef std::shared_ptr<FollowCamera> Ptr;

	static Ptr createWithTarget(const Object::Ptr& target, float distance, float angleAround)
	{
		auto camera = Ptr(new FollowCamera());
		if (camera->initWithTarget(target, distance, angleAround))
		{
			return camera;
		}

		return nullptr;
	}

	~FollowCamera();


	virtual bool initWithTarget(const Object::Ptr& target, float distance, float angle);
	virtual void update(float dt) override;

	void onKeyPressed(EventKeyboard::KeyCode keycode);
	void onKeyReleased(EventKeyboard::KeyCode keycode);

	void onMouseMoved(EventMouse* e);

	virtual inline void setTargetOffset(const Vec3& offset)
	{
		_targetOffset = offset;
	}

	virtual inline const Vec3& getTargetOffset()
	{
		return _targetOffset;
	}

private:
	FollowCamera();

	Object::Ptr _target;
	Vec3 _targetOffset;

	Vec3 _distanceFromTarget;
	
	float _angleRoundTarget;
	float _distance;

	float _ySpeed;
	float _rotateSpeed;

	void calculateCameraPosition(float dt);
};
