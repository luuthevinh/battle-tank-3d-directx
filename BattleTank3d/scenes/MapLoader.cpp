#include "MapLoader.h"
#include <rapidjson\rapidjson.h>
#include <rapidjson\filereadstream.h>
#include <cstdio>

const int MapLoader::WALL = 0;
const int MapLoader::FARM_HOUSE = 1;
const int MapLoader::WOOD = 2;
const int MapLoader::TREE_01 = 3;
const int MapLoader::TANK_01 = 4;

MapLoader::MapLoader()
{
}

MapLoader::~MapLoader()
{
}

bool MapLoader::initWithFile(const char * filePath)
{
	FILE* fp = fopen(filePath, "rb");

	char readBuffer[65536];

	FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	_document.ParseStream(is);

	fclose(fp);

	if (_document.HasParseError())
		return false;

	return true;
}

std::vector<Object::Ptr> MapLoader::buildMap()
{
	std::vector<Object::Ptr> result;

	auto objects = _document["objects"].GetArray();

	for (auto it = objects.begin(); it != objects.end(); it++)
	{
		auto object = this->createObject(*it);
		if (object != nullptr)
			result.push_back(object);
	}

	return result;
}

Vec3 MapLoader::getVector3(const rapidjson::Value & data, const char * key)
{
	if(!data.HasMember(key))
		return Vec3(0.0f, 0.0f, 0.0f);

	auto values = data[key].GetArray();

	return Vec3(values[0].GetFloat(), values[1].GetFloat(), values[2].GetFloat());
}

Object::Ptr MapLoader::createObject(const rapidjson::Value & data)
{
	int type = data.HasMember("type") ? data["type"].GetInt() : -1;
 
	switch (type)
	{
	case WALL:
	{
		return this->createModel(data, "resources\\models\\wall.obj");
	}
	case FARM_HOUSE:
	{
		return this->createModel(data, "resources\\models\\farmhouse\\farmhouse.obj");
	}
	case WOOD:
	{
		return this->createModel(data, "resources\\models\\wood\\Barrel_Boxes_Grain_Sack.obj");
	}
	case TREE_01:
	{
		return this->createModel(data, "resources\\models\\tree\\tree_01.obj");
	}
	case TANK_01:
	{
		return this->createModel(data, "resources\\models\\tank\\tank_01.obj");
	}
	default:
		break;
	}

	return nullptr;
}

Model::Ptr MapLoader::createModel(const rapidjson::Value & data, const char* filePath)
{
	auto wall = Model::createWithFile(filePath);

	auto position = this->getVector3(data, "position");
	auto rotation = this->getVector3(data, "rotation");
	auto name = this->getString(data, "name");

	wall->getTransform()->setPosition(position);
	wall->getTransform()->setRotate(rotation);
	wall->setName(name.c_str());

	auto renderer = wall->getComponent<MeshRenderer>(Component::RENDERDER);
	renderer->setEffect(Effect::get("phong"));
	renderer->setTechnique("PhongEffect");

	return wall;
}
