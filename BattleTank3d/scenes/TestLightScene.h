#pragma once
#include "..\Tiny3D.h"
#include "..\framework\\geometry\Sun.h"
USING_NS_TINY;

class TestLightScene: public Scene
{
public:
	TestLightScene();
	~TestLightScene();

	// Inherited via Scene
	virtual bool init() override;
	virtual void update(float dt) override;
	virtual void draw() override;
	virtual void release() override;

private:
	Mesh* _mesh;
	Sun::Ptr _sun;
	Cube::Ptr _testCube;
	Cube::Ptr _testCube2;
	Camera::Ptr _camera;
};

