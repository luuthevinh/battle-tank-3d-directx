#pragma once

#include "..\Tiny3D.h"

USING_NS_TINY;

class TestScene : public Scene
{
public:
	TestScene();
	~TestScene();

	virtual bool init() override;
	virtual void update(float dt) override;

	void beginUseShader(Shader* sender);

	void onKeyPressed(EventKeyboard::KeyCode keycode);
	void onMousePressed(EventMouse* e);
private:
	Cube::Ptr _testCube;
	Camera::Ptr _camera;
	Model::Ptr _model;
	Plane::Ptr _plane;
	Terrain::Ptr _terrain;

	Sphere::Ptr _playerSphere;

	bool _debug;
	
};
