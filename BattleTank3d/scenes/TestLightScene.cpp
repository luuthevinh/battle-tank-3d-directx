#include "TestLightScene.h"



TestLightScene::TestLightScene()
{
}


TestLightScene::~TestLightScene()
{
}

bool TestLightScene::init()
{
	//init camera
	_camera = Camera::create(Vec3(20.0f, 50.0f, -45.0f));

	//light can look like component, for case normal object which has point light
	//test point light with cube
	_testCube = Cube::create();

	auto pointLight = Light::createPointLight(1.0f, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	_testCube->addComponent(Component::LIGHT, pointLight);

	//test cube
	_testCube2 = Cube::create();
	_testCube2->getTransform()->setPositionX(5.0f);

	//test direction light with sun
	_sun = Sun::create();
	_sun->init();
	_sun->getTransform()->setPositionX(-5.5f);
	
	

	


	return true;
}

void TestLightScene::update(float dt) {

	//press F,G,H,J,K,L to rotate direction vector follow by x,z,y
	_sun->update(dt);

	//press O,P to increase/decrease range of point light
	_testCube->update(dt);


	_camera->update(dt);
}
void TestLightScene::draw() {
	_testCube->draw();
	_testCube2->draw();
	_sun->draw();
	
}
void TestLightScene::release() 
{
	//_sun->release();
}
