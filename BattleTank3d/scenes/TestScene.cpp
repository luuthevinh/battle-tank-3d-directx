#include "TestScene.h"
#include "MapLoader.h"
#include "..\objects\Tank.h"
#include "..\objects\FollowCamera.h"
#include "..\objects\Player.h"

USING_NS_TINY;

TestScene::TestScene()
{
}

TestScene::~TestScene()
{
	
}

bool TestScene::init()
{
	Object::init();

	auto physicsWorld = PhysicsWorld::create();
	this->setPhysicsWorld(physicsWorld);

	//auto scene = MapLoader::create("resources/data/map_01.json");
	//auto objects = scene->buildMap();

	

	// test direction light with sun
	auto sun = Sun::create(true);
	this->addChild("sun", sun);

	//_testCube = Cube::create();
	//_testCube->getTransform()->setScale(10.0f);
	//_testCube->getTransform()->setPositionY(100.0f);
	//this->addChild("testcube", _testCube);

	//63 ,193, -291 
	_camera = Camera::create(Vec3(63.0f, 193.0f, -291.0f));
	//_camera->followByTerrain(_terrain);
	//SceneManager::getInstance()->setMainCamera(_camera);
	this->addChild("debugCamera", _camera);

	//_terrain = Terrain::create("resources\\coastMountain64.raw", 64, 64, 10, 0.5f);
	_terrain = Terrain::create("resources\\heightmap_64.raw", 64, 64, 10, 0.5f);
	Vec3 sunDir = sun->getLight()->getDirection();
	_terrain->loadTexture("resources\\grass_2.png", &sunDir);
	//_terrain->loadTexture("resources\\texture2.png", &sunDir);
	//_terrain->getTransform()->setPosition(0.0f, 0.0f, 0.0f);
	this->addChild("terrain",_terrain);

	_plane = Plane::create(100, 100);
	this->addChild("plane", _plane);
	
	auto pointLight = PointLightCube::create(Vec3(0.0f, 100.0f, -5.0f));
	this->addChild("pointLight", pointLight);

	auto tank = Tank::create();
	tank->getTransform()->setPosition(Vec3(10.0f, 0.0f, 10.0f));
	//tank->setTerrain(_terrain);
	this->addChild("tank", tank);

	auto player = Player::create();
	this->addChild("player", player);
	player->getTransform()->setPosition(0.0f, 0.0f, 0.0f);
	//player->setTerrain(_terrain);

	auto followCamera = FollowCamera::createWithTarget(player, 8.0f, -60.0f);
	followCamera->setTargetOffset(Vec3(0.0f, 3.0f, 0.0f));
	this->addChild("mainCamera", followCamera);
	SceneManager::getInstance()->setMainCamera(followCamera);

	//auto pump = Model::createWithFile("resources\\models\\PumpJack.fbx");
	//pump->getTransform()->setScale(0.02f);
	//this->addChild("pump", pump);

	//for (size_t i = 0; i < 10; i++)
	//{
	//	auto wall = Model::createWithFile("resources\\models\\wall.obj");
	//	wall->getTransform()->setPosition(10.0f * i, 20.0f, 20.0f);

	//	char name[100];
	//	sprintf(name, "wall%d", i);

	//	this->addChild(name, wall);
	//}
	
	//auto renderer = pump->getComponent<MeshRenderer>(Component::RENDERDER);
	//renderer->setEffect(Effect::get("phong"));
	//renderer->setTechnique("PhongEffect_NoTexture");

	auto dispatcher = SceneManager::getInstance()->getEventDispatcher();

	auto keyListener = EventKeyboardListener::create();
	keyListener->onKeyPressed = CALLBACK_01(TestScene::onKeyPressed, this);

	dispatcher->addEventListener(keyListener, this);

	// add like physics world
	_terrain->addTransformToTerrain(player);
	//_terrain->addTransformToTerrain(tank);

	//for (auto obj : objects)
	//{
	//	_terrain->addTransformToTerrain(obj);
	//	this->addChild(obj);
	//}

	// add to physics world
	//_physicsWorld->addObjectToWorld(tank);
	_physicsWorld->addObjectToWorld(player);

	auto mouseListener = EventMouseListener::create();
	mouseListener->onMousePressed = CALLBACK_01(TestScene::onMousePressed, this);
	
	dispatcher->addEventListener(mouseListener, this);

	_playerSphere = Sphere::create(Vec3(0.0f, 0.0f, 0.0f), 10.0f);

	return true;
}

void TestScene::update(float dt)
{
	Scene::update(dt);

	// fix me: physics should update with fixed time
	if (_physicsWorld != nullptr)
	{
		_physicsWorld->update(dt);
	}
}

void TestScene::beginUseShader(Shader* sender)
{
}

void TestScene::onKeyPressed(EventKeyboard::KeyCode keycode)
{
	switch (keycode)
	{
	case EventKeyboard::KeyCode::KEY_F5:
	{
		if (!SceneManager::getInstance()->isDebug())
		{
			SceneManager::getInstance()->setDebug(true);
			SceneManager::getInstance()->setMainCamera(_camera);
			
		}
		else
		{
			auto playerCam = dynamic_pointer_cast<Camera>(this->getChild("mainCamera"));
			SceneManager::getInstance()->setMainCamera(playerCam);
			SceneManager::getInstance()->setDebug(false);
		}

		break;
	}
	default:
		break;
	}
}

void TestScene::onMousePressed(EventMouse* e)
{
	if (e->getButton() == EventMouse::Button::BUTTON_LEFT)
	{
		auto mainCam = SceneManager::getInstance()->getMainCamera();
		auto ray = mainCam->calculatePickingRay(e->getPosition());

		auto player = this->getChild("player");
		auto collider = player->getComponent<SphereCollider>(Component::SPHERE_COLLIDER);

		if (ray->hitTestWithSphereCollider(collider))
		{
			Debug::getInstance()->logStringOnScreen("Hit player!");
		}
	}
}
