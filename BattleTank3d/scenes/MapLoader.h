#pragma once
#include "..\Tiny3D.h"
#include <rapidjson\document.h>

USING_NS_TINY;
using namespace rapidjson;

class MapLoader
{
public:
	static const int WALL;
	static const int FARM_HOUSE;
	static const int WOOD;
	static const int TREE_01;
	static const int TANK_01;

	DEF_SHARED_PTR(MapLoader);

	static Ptr create(const char* filePath)
	{
		auto loader = Ptr(new MapLoader());
		if (loader->initWithFile(filePath))
		{
			return loader;
		}

		return nullptr;
	}

	~MapLoader();

	bool initWithFile(const char* filePath);
	
	std::vector<Object::Ptr> buildMap();

	inline static std::string getString(const rapidjson::Value &data, const char* key, const std::string& defaultValue = "")
	{
		return data.HasMember(key) ? data[key].GetString() : defaultValue;
	}

	inline static float getFloat(const rapidjson::Value &data, const char* key, float defaultValue = 0.0f)
	{
		return data.HasMember(key) ? data[key].GetFloat() : defaultValue;
	}

	inline static int getInt(const rapidjson::Value &data, const char* key, int defaultValue = 0)
	{
		return data.HasMember(key) ? data[key].GetInt() : defaultValue;
	}

	inline static bool getBool(const rapidjson::Value &data, const char* key, bool defaultValue = false)
	{
		return data.HasMember(key) ? data[key].GetBool() : defaultValue;;
	}

	Vec3 getVector3(const rapidjson::Value& data, const char * key);

private:
	MapLoader();

	Document _document;
	Object::Ptr createObject(const rapidjson::Value& data);

	Model::Ptr createModel(const rapidjson::Value& data, const char* filePath);
};
